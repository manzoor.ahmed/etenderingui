import { Route } from '@angular/router';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { NoAuthGuard } from 'app/core/auth/guards/noAuth.guard';
import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';

// @formatter:off
// tslint:disable:max-line-length
export const appRoutes: Route[] = [

    {
        path       : '',
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        data: {
            layout: 'modern'
        },
        children   : [
            {path: 'dashboard', loadChildren: () => import('app/modules/pages/dashboard/dashboard.module').then(m => m.DashboardModule)},
            {path: 'colteams', loadChildren: () => import('app/modules/pages/collaboration-teams/collaboration-teams.module').then(m => m.CollaborationTeamsModule)},
            {path: 'colteams-detail', loadChildren: () => import('app/modules/pages/collaboration-teams/list-detail/list-detail.module').then(m => m.ListDetailModule)},
            {path: 'rfx', loadChildren: () => import('app/modules/pages/rfx/rfx.module').then(m => m.RfxModule)},
            {path: 'rfx-new', loadChildren: () => import('app/modules/pages/rfx/rfx-new/rfx-new.module').then(m => m.RfxNewModule)},
            {path: 'rfx-new-from-template', loadChildren: () => import('app/modules/pages/rfx/rfx-new-from-template/rfx-new-from-template.module').then(m => m.RfxNewFromTemplateModule)},
            {path: 'rfx-new-from-existing', loadChildren: () => import('app/modules/pages/rfx/rfx-new-from-existing/rfx-new-from-existing.module').then(m => m.RfxNewFromExistingModule)},
            {path: 'attribute-items', loadChildren: () => import('app/modules/pages/attributes/attribute-items.module').then(m => m.AttributeItemsModule)},
            {path: 'attribute-list', loadChildren: () => import('app/modules/pages/attributes-list/attribute-list.module').then(m => m.AttributeListModule)},
            {path: 'reattlist', loadChildren: () => import('app/modules/pages/attributes-list/attribute-list.module').then(m => m.AttributeListModule)},
            {path: 'attribute/attribute-list/detail', loadChildren: () => import('app/modules/pages/attributes-list/list-detail/list-detail.module').then(m => m.ListDetailModule)},

            {path: 'supplier-invitation-list', loadChildren: () => import('app/modules/pages/supplier-invitation-list/supplier-invitation-list.module').then(m => m.SupplierInvitationListModule)},
            {path: 'supplier-invitation-list-detail', loadChildren: () => import('app/modules/pages/supplier-invitation-list/sup-list-detail/sup-list-detail.module').then(m => m.SupListDetailModule)},

            {path: 'cost-factors', loadChildren: () => import('app/modules/pages/cost-factors/cost-factors.module').then(m => m.CostFactorsModule)},
            {path: 'cost-factors-list', loadChildren: () => import('app/modules/pages/cost-factors-list/cost-factors-list.module').then(m => m.CostFactorsListModule)},
            {path: 'cost-factors-list-detail', loadChildren: () => import('app/modules/pages/cost-factors-list/list-detail/list-detail.module').then(m => m.ListDetailModule)},
            {path: 'negotiation-styles', loadChildren: () => import('app/modules/pages/negotiation-styles/negotiation-styles.module').then(m => m.NegotiationStylesModule)},
            {path: 'currency', loadChildren: () => import('app/modules/pages/currency/currency.module').then(m => m.CurrencyModule)},
            {path: 'numbering-sequence', loadChildren: () => import('app/modules/pages/numbering-sequence/numbering-sequence.module').then(m => m.NumberingSequenceModule)},
            {path: 'terms', loadChildren: () => import('app/modules/pages/terms/terms.module').then(m => m.TermsModule)},
            {path: 'rfq', loadChildren: () => import('app/modules/pages/rfq/rfq.module').then(m => m.RfqModule)},
            {path: 'rfaq', loadChildren: () => import('app/modules/pages/rfaq/rfaq.module').then(m => m.RfaqModule)},
            {path: 'form', loadChildren: () => import('app/modules/pages/form-builder/form-landing.module').then(m => m.FormLandingModule)},
            {path: 'form-new', loadChildren: () => import('app/modules/pages/form-builder/form-new/form-new.module').then(m => m.FormNewModule)},
            {path: 'suppliers', loadChildren: () => import('app/modules/pages/suppliers/suppliers.module').then(m => m.SuppliersModule)},

        ]
    },
];
