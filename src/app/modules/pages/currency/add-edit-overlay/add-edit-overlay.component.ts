import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator,PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {CurrencyService} from 'app/services/currency.service';
import {CurrencySearchModel} from 'app/models/currency-search-model';
import { Sort } from '@angular/material/sort';



@Component({
    selector: 'att-add-edit-overlay',
    templateUrl: './add-edit-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddEditOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumns: string[] = ['id', 'currency', 'country'];
    currencySearchModel:CurrencySearchModel=new CurrencySearchModel();
    pageEvent: PageEvent;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    
    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    addTeam = new FormGroup({
        teamName: new FormControl('Team Name One'),
        teamDescription: new FormControl('Team Description One'),
    });


    constructor(public dialogRef: MatDialogRef<AddEditOverlayComponent>,
                public dialog: MatDialog,private currencyService :CurrencyService) {
                    this.currencySearchModel.pageSize = 10;
                    this.currencySearchModel.page = 1;
    }
    FetchBasicData() {
        debugger;
        
        this.currencyService.getCurrencyList(this.currencySearchModel).subscribe(result => {
            console.log(result);
            debugger;
            this.currencySearchModel=result;
        });
    }
    ngOnInit() {
        this.FetchBasicData();

    }
    UpdateCurrencies() {
        this.currencyService.UpdateCurrencies(this.currencySearchModel.currencyModels).subscribe(result => {
            console.log(result);
            
        });
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    
    OnPaginateChange(event: PageEvent) {
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.currencySearchModel.pageSize = event.pageSize;
        this.currencySearchModel.page = page;
        this.FetchBasicData();
        //  this.dataSource=   this.CreatePaginationData(page,size);

    }
    sortData(sort: Sort) {
        debugger;
        this.currencySearchModel.direction = sort.direction;
        this.currencySearchModel.column = sort.active;
        this.FetchBasicData();
    }


}

