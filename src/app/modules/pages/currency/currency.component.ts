import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AddEditOverlayComponent} from './add-edit-overlay/add-edit-overlay.component';
import {CurrencyService} from 'app/services/currency.service';
import {CurrencyRateSearchModel} from 'app/models/currency-rate-search-model';



@Component({
    selector     : 'attribute-items',
    templateUrl  : './currency.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CurrencyComponent
{
    displayedColumns: string[] = ['id', 'currency', 'rate', 'validDate', 'conversion', 'rfq', 'rfi', 'rfaq'];
    currencyRateSearchModel:CurrencyRateSearchModel=new CurrencyRateSearchModel();
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    panelOpenState = false;
    defaultGuid:string="00000000-0000-0000-0000-000000000000";
    /**
     * Constructor
     */
    constructor(public dialog: MatDialog,private currencyService :CurrencyService)
    {
        this.currencyRateSearchModel.baseCurrency="00000000-0000-0000-0000-000000000000";   
    }
    FetchBasicData() {
        debugger;
        
        this.currencyService.getCurrencyRateList().subscribe(result => {
            console.log(result);
            debugger;
            this.currencyRateSearchModel=result;
           

        });
    }
    ngOnInit() {
        this.FetchBasicData();

    }
    SaveDefaults() {
        this.currencyService.SaveCurrencyRates(this.currencyRateSearchModel).subscribe(result => {
            console.log(result);
            debugger;
            this.currencyRateSearchModel=result;
           
        });

    }
    
    openDialog() {
        const dialogRef = this.dialog.open(AddEditOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
}

