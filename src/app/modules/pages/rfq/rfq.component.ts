import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AddCurrencyOverlayComponent} from './add-currency-overlay/add-currency-overlay.component';
import {AddNewAttributeListOverlayComponent} from './add-new-attributelist-overlay/add-new-attribute-list-overlay.component';
import {AddReusableAttributeOverlayComponent} from './add-reusable-attribute-overlay/add-reusable-attribute-overlay.component';
import {AddAttributeItemOverlayComponent} from './add-attribute-item-overlay/add-attribute-item-overlay.component';
import {AddNewAttributeOverlayComponent} from './add-new-attribute-overlay/add-new-attribute-overlay.component';
import {AddReusableCostfactorOverlayComponent} from './add-reusable-costfactor-overlay/add-reusable-costfactor-overlay.component';
import {AddNewCostfactorlistOverlayComponent} from './add-new-costfactorlist-overlay/add-new-costfactorlist-overlay.component';
import {AddNewCostfactorOverlayComponent} from './add-new-costfactor-overlay/add-new-costfactor-overlay.component';
import {AddCostfactorItemOverlayComponent} from './add-costfactor-item-overlay/add-costfactor-item-overlay.component';
import {AddNewPaymentschedulesOverlayComponent} from './add-new-paymentschedules-overlay/add-new-paymentschedules-overlay.component';
import {AddTermsOverlayComponent} from './add-terms-overlay/add-terms-overlay.component';
import {AddNewAttachmentOverlayComponent} from './add-new-attachment-overlay/add-new-attachment-overlay.component';
import {AddNewDocumenttextOverlayComponent} from './add-new-documenttext-overlay/add-new-documenttext-overlay.component';
import {AddNewNoteOverlayComponent} from './add-new-note-overlay/add-new-note-overlay.component';
import {AddNewDeliverableOverlayComponent} from './add-new-deliverable-overlay/add-new-deliverable-overlay.component';
import {AddSurveyOverlayComponent} from './add-survey-overlay/add-survey-overlay.component';
import {AddSurveyQuestionOverlayComponent} from './add-survey-question-overlay/add-survey-question-overlay.component';
import {CreateSurveyQuestionOverlayComponent} from './create-survey-question-overlay/create-survey-question-overlay.component';
import {AddSupplierOverlayComponent} from './add-supplier-overlay/add-supplier-overlay.component';
import {AddTeamOverlayComponent} from './add-team-overlay/add-team-overlay.component';
import {AddEditShippingOverlayComponent} from './add-edit-shipping-overlay/add-edit-shipping-overlay.component';
import {CopyToLinesOverlayComponent} from './copy-to-lines-overlay/copy-to-lines-overlay.component';
import {ApexAxisChartSeries, ApexChart, ApexDataLabels, ApexPlotOptions, ApexYAxis, ApexLegend, ApexStroke, ApexXAxis, ApexFill, ApexTooltip, ApexGrid, ApexStates,} from 'ng-apexcharts';
import {AwardSelectedOverlayComponent} from './award-selected-overlay/award-selected-overlay.component';
import {AwardRecommendOverlayComponent} from './award-recommend-overlay/award-recommend-overlay.component';


export type ChartOptions = {
    series: ApexAxisChartSeries;
    chart: ApexChart;
    dataLabels: ApexDataLabels;
    plotOptions: ApexPlotOptions;
    yaxis: ApexYAxis;
    xaxis: ApexXAxis;
    fill: ApexFill;
    tooltip: ApexTooltip;
    stroke: ApexStroke;
    legend: ApexLegend;
    grid: ApexGrid;
    colors: string[];
    states: ApexStates;
};
export type ChartOptions2 = {
    series: ApexAxisChartSeries;
    chart: ApexChart;
    dataLabels: ApexDataLabels;
    plotOptions: ApexPlotOptions;
    yaxis: ApexYAxis;
    xaxis: ApexXAxis;
    fill: ApexFill;
    tooltip: ApexTooltip;
    stroke: ApexStroke;
    legend: ApexLegend;
    grid: ApexGrid;
    colors: string[];
    states: ApexStates;
};

export interface RowData {
    stepNumber: string;
    id: string;
    name: string;
    address: string;
    status: string;
    rules: string;
    template: string;

}
export interface RowData2 {
    pefno: string;
    revno: string;
    type: string;
    name2: string;
    by: string;
    action: string;
    status2: string;
    timestamp: string;
    justification: string;
}

export interface RowData22 {
    supId: string;
    supName: string;
    supEmail: string;
    supResponse: string;
    supTechEval: string;
    supComEval: string;
    supRank: string;
    supContact: string;
}

export interface RowData4 {
    payno: string;
    description2: string;
    work: string;
    milestone: string;
    payment: string;
    retention: string;
    release: string;
    releasevalue: string;
}

export interface RowData432 {
    tbepayno: string;
    tbedescription2: string;
    tbework: string;
    tbemilestone: string;
    tbepayment: string;
    tberetention: string;
    tberelease: string;
    tbereleasevalue: string;
    tbecomments432: string;
    tbeweight432: string;
}

export interface RowData5 {
    no: string;
    termsname: string;
    inputtype: string;
    preview: string;
    editable: string;
    beforequote: string;
    endquote: string;
}

export interface RowData6 {
    srno: string;
    title: string;
    filename: string;
    documentclass: string;
    reference: string;
    atttype: string;
}

export interface RowData622 {
    srno622: string;
    title622: string;
    filename622: string;
    comment622: string;
    atttype622: string;
    tbeweight622: string;
}

export interface RowData7 {
    docsrno: string;
    outputtype: string;
    documentext: string;
}

export interface RowData722 {
    docsrno722: string;
    outputtype722: string;
    documentext722: string;
    documentext2722: string;
    tbeweight722: string;
    comment722: string;
}

export interface RowData8 {
    notesrno: string;
    notes: string;
}

export interface RowData822 {
    notes2822: string;
    notes822: string;
    comment822: string;
    tbeweight822: string;
}

export interface RowData9 {
    milestonenumber: string;
    milestonename: string;
    deliverabledescription: string;
    prevmilestonenumber: string;
    progresspercentage: string;
    stagepercentage: string;
    begindate: string;
}

export interface RowData932 {
    milestonenumber932: string;
    milestonename932: string;
    deliverabledescription932: string;
    prevmilestonenumber932: string;
    progresspercentage932: string;
    stagepercentage932: string;
    begindate932: string;
    tbeweight932: string;
    comment932: string;
}

export interface RowData10 {
    templatename: string;
    questionname: string;
    surveydescription: string;
    questiondetails: string;
}

export interface RowData11 {
    linesprno: string;
    lineno: string;
    partid: string;
    partdescription: string;
    uom: string;
    needbydate: string;
    startprice: string;
    targetprice: string;
}

export interface RowData12 {
    supplierId: string;
    supplierName: string;
    invitedDateTime: string;
    supplierEmail: string;
    altEmail: string;
    currencies: string;
    acknowledged: string;
    supplierStatus: string;
    responses: string;
}

export interface RowData13 {
    teamUserName: string;
    rfxRole: string;
    substituteName: string;
    accessLevel: string;
    pageAccess: string;
    teamName: string;
    teamDescription: string;
}
export interface RowData14 {
    headerRules: string;
}
export interface RowData15 {
    group: string;
}
export interface RowData16 {
    resRules: string;
}
export interface RowData17 {
    costfactName: string;
    costfactType: string;
    costfactDesc: string;
    costfactExpectedValue: string;
    costfactValue: string;
    costfactComments: string;
}
export interface RowData172 {
    tbecostfactName: string;
    tbecostfactType: string;
    tbecostfactDesc: string;
    tbecostfactExpectedValue: string;
    tbecostfactValue: string;
    tbecostfactComments: string;
    tbeweight2: string;
}
export interface RowData182 {
    tbecostfactName: string;
    tbecostfactType: string;
    tbecostfactDesc: string;
    tbecostfactValue: string;
    tbecostfactComments: string;
    tbeweight2: string;
}
export interface RowData19 {
    attributeItem: string;
    description: string;
    expectedValue: string;
    value: string;
    assocCosts: string;
    cost: string;
    comments: string;
}
export interface RowData20 {
    tbeattributeItem: string;
    tbedescription: string;
    tbeexpectedValue: string;
    tbevalue: string;
    tbeassocCosts: string;
    tbecost: string;
    tbecomments: string;
    tbecomments2: string;
    tbeweight: string;
}

/** Constants used to fill up our data base. */
const PAYNO: string[] = [
    '10101', '10102'
];
const DESCRIPTION2: string[] = [
    'Freight Cost for single shipment'
];
const WORK: string[] = [
    '01%'
];
const MILESTONE: string[] = [
    '01', '02'
];
const PAYMENT: string[] = [
    '20%', '30%'
];
const RETENTION: string[] = [
    '25%', '34%'
];
const RELEASE: string[] = [
    '20%', '30%'
];
const RELEASEVALUE: string[] = [
    '87234 USD'
];


/** Constants used to fill up our data base. */
const NUMBER: string[] = [
    '10', '20', '30'
];
const ID: string[] = [
    'User 01','User 02',
];
const NAME: string[] = [
    'Manager 01', 'Manager 02',
];
const ADDRESS: string[] = [
    'manager1@gmail.com',    'manager2@gmail.com',
];
const STATUS: string[] = [
    'Approved', 'Pending Approval',
];
const RULES: string[] = [
    'R1', 'R2','R3'
];
const TEMPLATE: string[] = [
    'R1', 'R2','R3'
];

/** Constants used to fill up our data base. */
const SUPID: string[] = [
    '10101', '10102', '10103'
];
const SUPNAME: string[] = [
    'Sup A','Sup B',
];
const SUPRESPONSE: string[] = [
    '01', '02',
];
const SUPEMAIL: string[] = [
    'manager1@gmail.com',    'manager2@gmail.com',
];
const SUPTECHEVAL: string[] = [
    'Accepted with reservations', 'Accepted',
];
const SUPCOMEVAL: string[] = [
    'Accepted'
];
const SUPRANK: string[] = [
    '2', '1','4'
];
const SUPCONTACT: string[] = [
    'Contact A', 'Contact B','Contact C'
];


const PEFNO: string[] = [
    '10101', '10102', '10104'
];
const REVNO: string[] = [
    '01', '02', '03'
];
const TYPE: string[] = [
    'RFQ'
];
const NAME2: string[] = [
    'ABCD ', 'ABCA', 'ABCO'
];
const BY: string[] = [
    'Buyer 01', 'Buyer 02', 'Buyer 03'
];
const ACTION: string[] = [
    'New Round RFQ', 'RFQ Published'
];
const STATUS2: string[] = [
    'Planned'
];
const TIMESTAMP: string[] = [
    '01-07-2021  3:499:40 PM', '01-07-2021  3:499:40 PM'
];
const JUSTIFICATION: string[] = [
    '10', '20', '30'
];

/** Constants used to fill up our data base. */
const TERMSNAME: string[] = [
    'Gegeral RFQ', 'Legal Terms'
];
const INPUTTYPE: string[] = [
    'Wordpad', 'Attachment'
];
const PREVIEW: string[] = [
    ' XXXXXXXXXXX XXXXXXXXX'
];
const EDITABLE: string[] = [
    'No', 'Yes'
];
const BEFOREQUOTE: string[] = [
    'Mandatory', 'Optional'
];
const ENDQUOTE: string[] = [
    'Mandatory', 'Optional'
];


/** Constants used to fill up our data base. */
const SRNO: string[] = [
    '10101', '10102'
];
const TITLE: string[] = [
    'xxxxxxxxxxxx'
];
const FILENAME: string[] = [
    'abcd.xml', 'pqrs.xml'
];
const DOCUMENTCLASS: string[] = [
    'XXX', 'YYY'
];
const REFERENCE: string[] = [
    'PR 001', 'PR 002'
];
const ATTTYPE: string[] = [
    'Technical', 'Non Technical'
];


/** Constants used to fill up our data base. */
const DOCSRNO: string[] = [
    '10101', '10102'
];
const OUTPUTTYPE: string[] = [
    'XXX', 'YYY'
];
const DOCUMENTTEXT: string[] = [
    'xxxxx xxxxxx xxxxx'
];


/** Constants used to fill up our data base. */
const NOTESRNO: string[] = [
    '10101', '10102'
];
const NOTES: string[] = [
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', 'Ipsum is simply dummy text and typesetting industry. '
];

/** Constants used to fill up our data base. */
const MILESTONENUMBER: string[] = [
    '01', '02'
];
const MILESTONENAME: string[] = [
    'Design Approval', 'Design Approval'
];
const DELIVERABLEDESCRIPTION: string[] = [
    'lorem ipsum text'
];
const PREVMILESTONENUMBER: string[] = [
    'PR 001', 'PR 002'
];
const PROGRESSPERCENTAGE: string[] = [
    '25%', '75%'
];
const STAGEPERCENTAGE: string[] = [
    '15%', '35%'
];
const BEGINDATE: string[] = [
    '02 -12-2021', '22 -12-2021'
];


/** Constants used to fill up our data base. */
const TEMPLATENAME: string[] = [
    '01', '02'
];
const QUESTIONNAME: string[] = [
    'Clarification Response Time', 'Payment'
];
const SURVEYDESCRIPTION: string[] = [
    'Rate our response time for clarifications', 'Payment were made on the due date without delays?'
];
const QUESTIONDETAILS: string[] = [
    'Public', 'Public'
];


/** Constants used to fill up our data base. */
const LINESPRNO: string[] = [
    '10101', '10101'
];
const LINENO: string[] = [
    '01', '02'
];
const PARTID: string[] = [
    '1121247', '3221247'
];
const PARTDESCRIPTION: string[] = [
    'Ball Bearings - SKF 6206'
];
const UOM: string[] = [
    '75', '50'
];
const NEEDBYDATE: string[] = [
    '19/12/2021', '24/12/2021'
];
const STARTPRICE: string[] = [
    '$75', '$35'
];
const TARGETPRICE: string[] = [
    '$55', '$25'
];


/** Constants used to fill up our data base. */
const SUPPLIERID: string[] = [
    '10101', '10101'
];
const SUPPLIERNAME: string[] = [
    'Sup A', 'Sup B'
];
const INVITEDDATETIME: string[] = [
    '01-02-2021  02:00:13 PM', '21-02-2021  02:00:13 PM'
];
const SUPPLIEREMAIL: string[] = [
    'A@gmail.com', 'B@gmail.com'
];
const ALTEMAIL: string[] = [
    'ABC@gmail.com', 'BCD@gmail.com'
];
const CURRENCIES: string[] = [
    'Dirham', 'USD'
];
const ACKNOWLEDGED: string[] = [
    'Yes', 'No'
];
const SUPPLIERSTATUS: string[] = [
    'Intend to participate', 'Not Interested to participate','Response Submitted', 'Yet to acknowledge'
];
const RESPONSES: string[] = [
    '$55', '$25'
];

/** Constants used to fill up our data base. */
const TEAMUSERNAME: string[] = [
    'Danny Boyle', 'Richard Barn'
];
const RFXROLE: string[] = [
    'Buyer', 'Technical Bid Evaluator', 'Commercial Bid Evaluator'
];
const SUBSTITUTENAME: string[] = [
    'Chris Torris', 'M Nauman'
];
const ACCESSLEVEL: string[] = [
    'Full', 'Edit Tabs'
];
const PAGEACCESS: string[] = [
    'Full RFQ', 'RFQ - Evaluations'
];
const TEAMNAME: string[] = [
    '10101', '10101'
];
const TEAMDESCRIPTION: string[] = [
    'Quality Team 01', 'Quality Team 02'
];

/** Constants used to fill up our data base. */
const HEADERRULES: string[] = [
    'Attribute Items', 'Cost Factors', 'Payment Schedules'
];

/** Constants used to fill up our data base. */
const GROUP: string[] = [
    'Attribute Items', 'Cost Factors', 'Payment Schedules'
];

/** Constants used to fill up our data base. */
const RESRULES: string[] = [
    'Allow suppliers to select lines on which to respond', 'Display best price to suppliers', 'Allow multiple response'
];

/** Constants used to fill up our data base. */
const COSTFACTNAME: string[] = [
    'Line Price', 'Cost of Start-Up & C/Ammissioning Spares'
];
const COSTFACTTYPE: string[] = [
    'Total', 'Fixed'
];
const COSTFACTDESC: string[] = [
    'Charges applicable for R&', 'cost of administration '
];
const COSTFACTEXPECTEDVALUE: string[] = [
    '$25,000.00', '$35,000.00'
];
const COSTFACTVALUE: string[] = [
    '$5,000.00', '$5,000.00'
];
const COSTFACCOMMENTS: string[] = [
    'comment'
];


/** Constants used to fill up our data base. */
const ATTRIBUTEITEM: string[] = [
    'Minimum Order', 'Packaging'
];
const RESDESCRIPTION: string[] = [
    'Min order quantity', 'When can you ship?'
];
const EXPECTEDVALUE: string[] = [
    '100', 'Purposes/Overlays/Structures'
];
const RESVALUE: string[] = [
    'Number', 'Text'
];
const ASSOCCOSTS: string[] = [
    'Yes', 'No'
];
const COST: string[] = [
    '236.00 USD', '456.00 USD'
];
const COMMENT: string[] = [
    ''
];

/** Constants used to fill up our data base. */
const TBEATTRIBUTEITEM: string[] = [
    'Minimum Order', 'Packaging'
];
const TBERESDESCRIPTION: string[] = [
    'Min order quantity', 'When can you ship?'
];
const TBEEXPECTEDVALUE: string[] = [
    '100', 'Purposes/Overlays/Structures'
];
const TBERESVALUE: string[] = [
    'Number', 'Text'
];
const TBEASSOCCOSTS: string[] = [
    'Yes', 'No'
];
const TBECOST: string[] = [
    '236.00 USD', '456.00 USD'
];
const TBECOMMENT: string[] = [
    'comment description'
];
const TBEWEIGHT: string[] = [
    '05', '02'
];

@Component({
    selector     : 'attribute-items',
    templateUrl  : './rfq.component.html',
    encapsulation: ViewEncapsulation.None,
    styles         : [
        /* language=SCSS */
        `
            .attribute-items-parent-grid {
                grid-template-columns: 40px 20% 20% auto 96px;

                @screen md {
                    grid-template-columns: 40px 10% 10% auto 96px;
                }
            }
            .attribute-items-parent-grid2 {
                grid-template-columns: 20% 20% auto 96px;

                @screen md {
                    grid-template-columns: 10% 10% auto 96px;
                }
            }
            .attribute-items-inner-grid {
                grid-template-columns: 40px 10% 12% auto 10% 10% 10% 10%;
                min-width: 1200px;

                @screen md {
                    grid-template-columns: 40px 10% 12% auto 10% 10% 10% 10%;
                }
            }
            .noneditable-attribute-items-inner-grid {
                grid-template-columns: 35% auto 30%;
                min-width: 500px;

                @screen md {
                    grid-template-columns: 35% auto 30%;
                }
            }
            .noneditable-attribute-items-inner-grid2 {
                grid-template-columns: 25% 25% 20% auto;
                min-width: 500px;

                @screen md {
                    grid-template-columns: 25% 25% 20% auto;
                }
            }
            .costfactor-items-parent-grid {
                grid-template-columns: 40px 20% auto 96px;

                @screen md {
                    grid-template-columns: 40px 10% auto 96px;
                }
            }
            .noneditable-costfactor-items-parent-grid {
                grid-template-columns: 20% auto 96px;

                @screen md {
                    grid-template-columns: 10% auto 96px;
                }
            }
            .costfactor-items-inner-grid {
                grid-template-columns: 40px 10% 12% 12% 15% 10% 10% 10% 14%;
                min-width: 1200px;

                @screen md {
                    grid-template-columns: 40px 10% 12% 12% 15% 10% 10% 10% 14%;
                }
            }
            .survey-parent-grid {
                grid-template-columns: 40px 20% auto 96px;

                @screen md {
                    grid-template-columns: 40px 10% auto 96px;
                }
            }
            .scoring-grid {
                grid-template-columns: auto 260px 260px 96px;
                min-width: 800px;

                @screen md {
                    grid-template-columns: auto 260px 260px 96px;
                    min-width: 800px;
                }
            }
            .scoring-inner-grid {
                grid-template-columns: auto 160px 160px;

                @screen md {
                    grid-template-columns: auto 160px 160px;
                }
            }
            .survey-inner-grid {
                grid-template-columns: 40px 15% auto 14% 12%;
                min-width: 900px;

                @screen md {
                    grid-template-columns: 40px 15% auto 14% 12%;
                }
            }
            .line-items-parent-grid {
                grid-template-columns: 40px 60px 60px 90px auto 80px 80px 110px 80px 100px 100px 96px;

                @screen md {
                    grid-template-columns: 40px 60px 60px 90px auto 80px 80px 110px 80px 100px 100px 96px;
                }
            }
            .noneditable-line-items-parent-grid {
                grid-template-columns: 60px 60px 90px auto 80px 80px 110px 80px 100px 100px 96px;

                @screen md {
                    grid-template-columns: 60px 60px 90px auto 80px 80px 110px 80px 100px 100px 96px;
                }
            }
            .noneditable-line-items-parent-grid2 {
                grid-template-columns: 60px 90px auto 80px 80px 110px 100px 100px 96px;

                @screen md {
                    grid-template-columns: 60px 90px auto 80px 80px 110px 100px 100px 96px;
                }
            }
            .scoring-line-items-grid {
                grid-template-columns: 60px 140px auto 260px 260px;

                @screen md {
                    grid-template-columns: 60px 140px auto 260px 260px;
                }
            }
            .scoring-line-items-criteria-grid {
                grid-template-columns: auto 120px 120px 135px;

                @screen md {
                    grid-template-columns: auto 120px 120px 135px;
                }
            }
            .awardline-grid {
                grid-template-columns: 90px 80px 120px 110px 180px 180px 120px;

                @screen md {
                    grid-template-columns: 90px 80px 120px 110px 180px 180px 120px;
                }
            }
            .deviation-grid {
                grid-template-columns: 40px 150px 200px 150px 150px 150px;

                @screen md {
                    grid-template-columns: 40px 150px 200px 150px 150px 150px;
                }
            }
            .noneditable-deviation-grid {
                grid-template-columns: 150px 250px 150px 150px 150px;

                @screen md {
                    grid-template-columns: 150px 250px 150px 150px 150px;
                }
            }
            .overall-supplier-grid {
                grid-template-columns: 140px 80px 160px 60px 60px 60px 180px;

                @screen md {
                    grid-template-columns: 140px 80px 160px 60px 60px 60px 180px;
                }
            }
            .shipment-grid {
                grid-template-columns: 40px 100px 100px 100px 180px 150px 150px 150px;

                @screen md {
                    grid-template-columns: 40px 100px 100px 100px 180px 150px 150px 150px;
                }
            }
            .noneditable-shipment-grid {
                grid-template-columns: 100px 100px 100px 180px 150px 150px 150px;

                @screen md {
                    grid-template-columns: 100px 100px 100px 180px 150px 150px 150px;
                }
            }
            .tbe-noneditable-line-items-parent-grid {
                grid-template-columns: 80px 80px 100px 200px 80px 80px 130px 100px 120px 120px 100px 100px 140px 180px 96px;

                @screen md {
                    grid-template-columns: 80px 80px 100px 200px 80px 80px 130px 100px 120px 120px 100px 100px 140px 180px 96px;
                }
            }
        `
    ],
})

export class RfqComponent
{
    displayedColumn: string[] = ['stepNumber', 'id', 'name', 'address', 'status', 'rules', 'template'];
    displayedColumn2: string[] = ['pefno', 'revno', 'type', 'name2', 'by','action', 'status2', 'timestamp', 'justification'];
    displayedColumn22: string[] = ['supId', 'supName', 'supContact', 'supEmail', 'supResponse','supTechEval', 'supComEval', 'supRank'];
    displayedColumn4: string[] = ['id', 'payno', 'description2', 'type2', 'work', 'milestone', 'payment','retention', 'release','releasevalue', 'visibility'];
    displayedColumn42: string[] = ['payno', 'description2', 'work', 'milestone', 'payment','retention', 'release','releasevalue'];
    displayedColumn43: string[] = ['payno', 'description2', 'work', 'milestone', 'payment','retention', 'release','releasevalue', 'comment'];
    displayedColumn432: string[] = ['tbepayno', 'tbedescription2', 'tbework', 'tbemilestone', 'tbepayment','tberetention', 'tberelease','tbereleasevalue', 'tbecomments432', 'tbeweight432', 'tbescore', 'tbeevaluation', 'tbecomments2432'];
    displayedColumn5: string[] = ['id', 'no', 'termsname', 'inputtype', 'preview', 'type3', 'editable','default', 'beforequote','endquote'];
    displayedColumn6: string[] = ['id', 'srno', 'title', 'filename','attachment', 'documentclass', 'reference','internalrfq','atttype'];
    displayedColumn62: string[] = ['srno', 'title', 'filename','attachment','atttype', 'attachment2', 'comment'];
    displayedColumn622: string[] = ['srno622', 'title622', 'filename622','attachment622','atttype622', 'attachment2622', 'comment622', 'tbeweight622', 'tbescore622', 'tbeevaluation622', 'tbecomments622'];
    displayedColumn7: string[] = ['id', 'docsrno','outputtype','documentext', 'type3', 'visibility3'];
    displayedColumn72: string[] = ['documentext', 'documentext2', 'comment'];
    displayedColumn722: string[] = ['documentext722', 'documentext2722', 'comment722', 'tbeweight722', 'tbescore722', 'tbeevaluation722', 'tbecomments722'];
    displayedColumn8: string[] = ['id', 'notesrno', 'notes', 'type4', 'visibility4'];
    displayedColumn82: string[] = ['notes', 'notes2', 'comment'];
    displayedColumn822: string[] = ['notes822', 'notes2822', 'comment822', 'tbeweight822', 'tbescore822', 'tbeevaluation822', 'tbecomments822'];
    displayedColumn9: string[] = ['id', 'milestonenumber', 'milestonename', 'deliverabledescription','type5','prevmilestonenumber', 'progresspercentage', 'stagepercentage','begindate','visibility5'];
    displayedColumn92: string[] = ['milestonenumber', 'milestonename', 'deliverabledescription','prevmilestonenumber', 'progresspercentage', 'stagepercentage','begindate'];
    displayedColumn93: string[] = ['milestonenumber', 'milestonename', 'deliverabledescription','prevmilestonenumber', 'progresspercentage', 'stagepercentage','begindate','comment'];
    displayedColumn932: string[] = ['milestonenumber932', 'milestonename932', 'deliverabledescription932','prevmilestonenumber932', 'progresspercentage932', 'stagepercentage932','begindate932','comment932', 'tbeweight932', 'tbescore932', 'tbeevaluation932', 'tbecomments932'];
    displayedColumn11: string[] = ['id', 'linesprno', 'lineno', 'partid','partdescription', 'uom', 'needbydate','startprice', 'targetprice','showprices'];
    displayedColumn12: string[] = ['id', 'supplierId', 'supplierName', 'invitedDateTime','supplierContact', 'supplierEmail', 'altEmail', 'currencies','acknowledged', 'supplierStatus','responses'];
    displayedColumn13: string[] = ['id', 'teamUserName', 'rfxRole', 'substituteName','accessLevel', 'pageAccess', 'teamName','teamDescription'];
    displayedColumn14: string[] = ['id', 'headerRules', 'headerTechnical', 'headerCommercial'];
    displayedColumn15: string[] = ['id', 'group', 'headerTechnical2', 'headerCommercial2'];
    displayedColumn16: string[] = ['id', 'resRules', 'selection'];
    displayedColumn17: string[] = ['costfactName', 'costfactType', 'costfactDesc', 'costfactExpectedValue', 'costfactValue', 'costfactComments'];
    displayedColumn172: string[] = ['tbecostfactName', 'tbecostfactType', 'tbecostfactDesc', 'tbecostfactExpectedValue', 'tbecostfactValue', 'tbecostfactComments', 'tbeweight2', 'tbescore', 'tbeevaluation', 'tbecomments2'];
    displayedColumn18: string[] = ['costfactName', 'costfactType', 'costfactDesc', 'costfactValue', 'costfactComments'];
    displayedColumn182: string[] = ['tbecostfactName', 'tbecostfactType', 'tbecostfactDesc', 'tbecostfactValue', 'tbecostfactComments', 'tbeweight2', 'tbescore', 'tbeevaluation', 'tbecomments2'];
    displayedColumn19: string[] = ['attributeItem', 'description', 'expectedValue', 'value', 'assocCosts', 'cost', 'comments'];
    displayedColumn20: string[] = ['tbeattributeItem', 'tbedescription', 'tbeexpectedValue', 'tbevalue', 'tbeassocCosts', 'tbecost', 'tbecomments', 'tbeweight', 'tbescore', 'tbeevaluation', 'tbecomments2'];


    dataSource: MatTableDataSource<RowData>;
    dataSource2: MatTableDataSource<RowData2>;
    dataSource22: MatTableDataSource<RowData22>;
    dataSource4: MatTableDataSource<RowData4>;
    dataSource432: MatTableDataSource<RowData432>;
    dataSource5: MatTableDataSource<RowData5>;
    dataSource6: MatTableDataSource<RowData6>;
    dataSource622: MatTableDataSource<RowData622>;
    dataSource7: MatTableDataSource<RowData7>;
    dataSource722: MatTableDataSource<RowData722>;
    dataSource8: MatTableDataSource<RowData8>;
    dataSource822: MatTableDataSource<RowData822>;
    dataSource9: MatTableDataSource<RowData9>;
    dataSource932: MatTableDataSource<RowData932>;
    dataSource10: MatTableDataSource<RowData10>;
    dataSource11: MatTableDataSource<RowData11>;
    dataSource12: MatTableDataSource<RowData12>;
    dataSource13: MatTableDataSource<RowData13>;
    dataSource14: MatTableDataSource<RowData14>;
    dataSource15: MatTableDataSource<RowData15>;
    dataSource16: MatTableDataSource<RowData16>;
    dataSource17: MatTableDataSource<RowData17>;
    dataSource172: MatTableDataSource<RowData172>;
    dataSource182: MatTableDataSource<RowData182>;
    dataSource19: MatTableDataSource<RowData19>;
    dataSource20: MatTableDataSource<RowData20>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;

    public chartOptions: Partial<ChartOptions>;
    public chartOptions2: Partial<ChartOptions2>;


    /**
     * Constructor
     */
    constructor(public dialog: MatDialog)
    {
        this.chartOptions = {
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            series: [
                {
                    name: 'Rank',
                    data: [21, 22, 10]
                },
            ],
            chart: {
                height: 300,
                type: 'bar',
                toolbar: {
                    show: false
                },
                animations:{
                    enabled: false
                }
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#0FA60C',
                    '#438AFE',
                    '#958F02',
                ],
            },

            plotOptions: {
                bar: {
                    columnWidth: '40%',
                    distributed: true,
                     dataLabels: {
                        position: 'top' // top, center, bottom
                    },
                }
            },
            dataLabels: {
                enabled: true,
                formatter: function(val) {
                    return val + '';
                },
                offsetY: -20,
                offsetX: 0,
                style: {
                    fontSize: '12px',
                    colors: ['#464A53']
                }
            },
            legend: {
                show: false
            },
            grid: {
                show: false
            },
            yaxis: {
                title: {
                    text: 'Rank'
                }
            },
            xaxis: {
                categories: [
                    ['Supplier A'],
                    ['Supplier B'],
                    ['Supplier C'],

                ],
                labels: {
                    style: {
                        colors: [
                            '#464A53',
                        ],
                        fontSize: '12px'
                    }
                },
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };

        this.chartOptions2 = {
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            series: [
                {
                    name: 'Rank',
                    data: [21, 22, 10]
                },
            ],
            chart: {
                height: 300,
                type: 'bar',
                toolbar: {
                    show: false
                },
                animations:{
                    enabled: false
                }
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#0FA60C',
                    '#438AFE',
                    '#958F02',
                ],
            },

            plotOptions: {
                bar: {
                    columnWidth: '20%',
                    barHeight: '50%',
                    distributed: true,
                     dataLabels: {
                        position: 'top' // top, center, bottom
                    },
                    horizontal: true,
                }
            },
            dataLabels: {
                enabled: true,
                formatter: function(val) {
                    return val + '';
                },
                offsetY: -0,
                offsetX: 20,
                style: {
                    fontSize: '12px',
                    colors: ['#464A53']
                }
            },
            legend: {
                show: false
            },
            grid: {
                show: false
            },
            yaxis: {
                labels:{
                    show: true
                },
                axisBorder: {
                    show: false
                }
            },
            xaxis: {
                categories: [
                    ['Supplier A'],
                    ['Supplier B'],
                    ['Supplier C'],

                ],
                title: {
                    text: 'Rank',
                },
                crosshairs: {
                    show: false
                },
                labels: {
                    style: {
                        colors: [
                            '#464A53',
                        ],
                        fontSize: '12px'
                    }
                },
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };


        const users = Array.from({length: 5}, (_, k) => createNewRow(k + 1));
        const users2 = Array.from({length: 5}, (_, k) => createNewRow2(k + 1));
        const users22 = Array.from({length: 5}, (_, k) => createNewRow22(k + 1));
        const users4 = Array.from({length: 6}, (_, k) => createNewRow4(k + 1));
        const users432 = Array.from({length: 6}, (_, k) => createNewRow432(k + 1));
        const users5 = Array.from({length:3}, (_, k) => createNewRow5(k + 1));
        const users6 = Array.from({length:3}, (_, k) => createNewRow6(k + 1));
        const users622 = Array.from({length:3}, (_, k) => createNewRow622(k + 1));
        const users7 = Array.from({length:3}, (_, k) => createNewRow7(k + 1));
        const users722 = Array.from({length:3}, (_, k) => createNewRow722(k + 1));
        const users8 = Array.from({length:3}, (_, k) => createNewRow8(k + 1));
        const users822 = Array.from({length:3}, (_, k) => createNewRow822(k + 1));
        const users9 = Array.from({length:3}, (_, k) => createNewRow9(k + 1));
        const users932 = Array.from({length:3}, (_, k) => createNewRow932(k + 1));
        const users10 = Array.from({length:3}, (_, k) => createNewRow10(k + 1));
        const users11 = Array.from({length:3}, (_, k) => createNewRow11(k + 1));
        const users12 = Array.from({length:3}, (_, k) => createNewRow12(k + 1));
        const users13 = Array.from({length:3}, (_, k) => createNewRow13(k + 1));
        const users14 = Array.from({length:3}, (_, k) => createNewRow14(k + 1));
        const users15 = Array.from({length:3}, (_, k) => createNewRow15(k + 1));
        const users16 = Array.from({length:3}, (_, k) => createNewRow16(k + 1));
        const users17 = Array.from({length:3}, (_, k) => createNewRow17());
        const users172 = Array.from({length:3}, (_, k) => createNewRow172());
        const users182 = Array.from({length:3}, (_, k) => createNewRow182());
        const users19 = Array.from({length:3}, (_, k) => createNewRow19());
        const users20 = Array.from({length:3}, (_, k) => createNewRow20());

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
        this.dataSource2 = new MatTableDataSource(users2);
        this.dataSource22 = new MatTableDataSource(users22);
        this.dataSource4 = new MatTableDataSource(users4);
        this.dataSource432 = new MatTableDataSource(users432);
        this.dataSource5 = new MatTableDataSource(users5);
        this.dataSource6 = new MatTableDataSource(users6);
        this.dataSource622 = new MatTableDataSource(users622);
        this.dataSource7 = new MatTableDataSource(users7);
        this.dataSource722 = new MatTableDataSource(users722);
        this.dataSource8 = new MatTableDataSource(users8);
        this.dataSource822 = new MatTableDataSource(users822);
        this.dataSource9 = new MatTableDataSource(users9);
        this.dataSource932 = new MatTableDataSource(users932);
        this.dataSource10 = new MatTableDataSource(users10);
        this.dataSource11 = new MatTableDataSource(users11);
        this.dataSource12 = new MatTableDataSource(users12);
        this.dataSource13 = new MatTableDataSource(users13);
        this.dataSource14 = new MatTableDataSource(users14);
        this.dataSource15 = new MatTableDataSource(users15);
        this.dataSource16 = new MatTableDataSource(users16);
        this.dataSource17 = new MatTableDataSource(users17);
        this.dataSource172 = new MatTableDataSource(users172);
        this.dataSource182 = new MatTableDataSource(users182);
        this.dataSource19 = new MatTableDataSource(users19);
        this.dataSource20 = new MatTableDataSource(users20);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource2.paginator = this.paginator;
        this.dataSource2.sort = this.sort;
        this.dataSource4.paginator = this.paginator;
        this.dataSource4.sort = this.sort;
        this.dataSource5.paginator = this.paginator;
        this.dataSource5.sort = this.sort;
        this.dataSource6.paginator = this.paginator;
        this.dataSource6.sort = this.sort;
        this.dataSource7.paginator = this.paginator;
        this.dataSource7.sort = this.sort;
        this.dataSource8.paginator = this.paginator;
        this.dataSource8.sort = this.sort;
        this.dataSource9.paginator = this.paginator;
        this.dataSource9.sort = this.sort;
        this.dataSource10.paginator = this.paginator;
        this.dataSource10.sort = this.sort;
        this.dataSource11.paginator = this.paginator;
        this.dataSource11.sort = this.sort;
        this.dataSource12.paginator = this.paginator;
        this.dataSource12.sort = this.sort;
        this.dataSource13.paginator = this.paginator;
        this.dataSource13.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
        this.dataSource2.filter = filterValue.trim().toLowerCase();
        this.dataSource4.filter = filterValue.trim().toLowerCase();
        this.dataSource5.filter = filterValue.trim().toLowerCase();
        this.dataSource6.filter = filterValue.trim().toLowerCase();
        this.dataSource7.filter = filterValue.trim().toLowerCase();
        this.dataSource8.filter = filterValue.trim().toLowerCase();
        this.dataSource9.filter = filterValue.trim().toLowerCase();
        this.dataSource10.filter = filterValue.trim().toLowerCase();
        this.dataSource11.filter = filterValue.trim().toLowerCase();
        this.dataSource12.filter = filterValue.trim().toLowerCase();
        this.dataSource13.filter = filterValue.trim().toLowerCase();
    }
    openAddCurrency() {
        const dialogRef = this.dialog.open(AddCurrencyOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    reusableAttributeList() {
        const dialogRef = this.dialog.open(AddReusableAttributeOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addAttributeList() {
        const dialogRef = this.dialog.open(AddNewAttributeListOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addAttributeItem() {
        const dialogRef = this.dialog.open(AddAttributeItemOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addNewAttributeItem() {
        const dialogRef = this.dialog.open(AddNewAttributeOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }

    reusableCostFactorList() {
        const dialogRef = this.dialog.open(AddReusableCostfactorOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addCostFactorList() {
        const dialogRef = this.dialog.open(AddNewCostfactorlistOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addNewCostFactorItem() {
        const dialogRef = this.dialog.open(AddNewCostfactorOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addCostFactorItem() {
        const dialogRef = this.dialog.open(AddCostfactorItemOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addPaymentSchedule() {
        const dialogRef = this.dialog.open(AddNewPaymentschedulesOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addTerms() {
        const dialogRef = this.dialog.open(AddTermsOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addAttachment() {
        const dialogRef = this.dialog.open(AddNewAttachmentOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addDocumentText() {
        const dialogRef = this.dialog.open(AddNewDocumenttextOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addNote() {
        const dialogRef = this.dialog.open(AddNewNoteOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addDeliverable() {
        const dialogRef = this.dialog.open(AddNewDeliverableOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addSurveyTemplate() {
        const dialogRef = this.dialog.open(AddSurveyOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addSurveyQuestion() {
        const dialogRef = this.dialog.open(AddSurveyQuestionOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    createSurveyQuestion() {
        const dialogRef = this.dialog.open(CreateSurveyQuestionOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addSupplier() {
        const dialogRef = this.dialog.open(AddSupplierOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addTeamMember() {
        const dialogRef = this.dialog.open(AddTeamOverlayComponent);
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    openShipment() {
        const dialogRef = this.dialog.open(AddEditShippingOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    openCopytoLines() {
        const dialogRef = this.dialog.open(CopyToLinesOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    openAwardSelectedLine() {
        const dialogRef = this.dialog.open(AwardSelectedOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    openAwardRecommendLine() {
        const dialogRef = this.dialog.open(AwardRecommendOverlayComponent);
        dialogRef.addPanelClass('inline-sm-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }


    isShow = true;
    isShow2 = true;

    toggleDisplay() {
        this.isShow = !this.isShow;
    }
    toggleDisplay2() {
        this.isShow2 = !this.isShow2;
    }

}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        stepNumber: NUMBER[Math.round(Math.random() * (NUMBER.length - 1))],
        id: ID[Math.round(Math.random() * (ID.length - 1))],
        name: NAME[Math.round(Math.random() * (NAME.length - 1))],
        address: ADDRESS[Math.round(Math.random() * (ADDRESS.length - 1))],
        status: STATUS[Math.round(Math.random() * (STATUS.length - 1))],
        rules: RULES[Math.round(Math.random() * (RULES.length - 1))],
        template: TEMPLATE[Math.round(Math.random() * (TEMPLATE.length - 1))],

    };
}
function createNewRow2(id: number): RowData2 {

    return {
        pefno: PEFNO[Math.round(Math.random() * (PEFNO.length - 1))],
        revno: REVNO[Math.round(Math.random() * (REVNO.length - 1))],
        type: TYPE[Math.round(Math.random() * (TYPE.length - 1))],
        name2: NAME2[Math.round(Math.random() * (NAME2.length - 1))],
        by: BY[Math.round(Math.random() * (BY.length - 1))],
        action: ACTION[Math.round(Math.random() * (ACTION.length - 1))],
        status2: STATUS2[Math.round(Math.random() * (STATUS2.length - 1))],
        timestamp: TIMESTAMP[Math.round(Math.random() * (TIMESTAMP.length - 1))],
        justification: JUSTIFICATION[Math.round(Math.random() * (JUSTIFICATION.length - 1))],
    };
}
function createNewRow22(id: number): RowData22 {

    return {
        supId: SUPID[Math.round(Math.random() * (SUPID.length - 1))],
        supName: SUPNAME[Math.round(Math.random() * (SUPNAME.length - 1))],
        supEmail: SUPEMAIL[Math.round(Math.random() * (SUPEMAIL.length - 1))],
        supResponse: SUPRESPONSE[Math.round(Math.random() * (SUPRESPONSE.length - 1))],
        supTechEval: SUPTECHEVAL[Math.round(Math.random() * (SUPTECHEVAL.length - 1))],
        supComEval: SUPCOMEVAL[Math.round(Math.random() * (SUPCOMEVAL.length - 1))],
        supRank: SUPRANK[Math.round(Math.random() * (SUPRANK.length - 1))],
        supContact: SUPCONTACT[Math.round(Math.random() * (SUPCONTACT.length - 1))],
    };
}

function createNewRow4(id: number): RowData4 {

    return {
        payno: PAYNO[Math.round(Math.random() * (PAYNO.length - 1))],
        description2: DESCRIPTION2[Math.round(Math.random() * (DESCRIPTION2.length - 1))],
        work: WORK[Math.round(Math.random() * (WORK.length - 1))],
        milestone: MILESTONE[Math.round(Math.random() * (MILESTONE.length - 1))],
        payment: PAYMENT[Math.round(Math.random() * (PAYMENT.length - 1))],
        retention: RETENTION[Math.round(Math.random() * (RETENTION.length - 1))],
        release: RELEASE[Math.round(Math.random() * (RELEASE.length - 1))],
        releasevalue: RELEASEVALUE[Math.round(Math.random() * (RELEASEVALUE.length - 1))],
    };
}

function createNewRow432(id: number): RowData432 {

    return {
        tbepayno: PAYNO[Math.round(Math.random() * (PAYNO.length - 1))],
        tbedescription2: DESCRIPTION2[Math.round(Math.random() * (DESCRIPTION2.length - 1))],
        tbework: WORK[Math.round(Math.random() * (WORK.length - 1))],
        tbemilestone: MILESTONE[Math.round(Math.random() * (MILESTONE.length - 1))],
        tbepayment: PAYMENT[Math.round(Math.random() * (PAYMENT.length - 1))],
        tberetention: RETENTION[Math.round(Math.random() * (RETENTION.length - 1))],
        tberelease: RELEASE[Math.round(Math.random() * (RELEASE.length - 1))],
        tbereleasevalue: RELEASEVALUE[Math.round(Math.random() * (RELEASEVALUE.length - 1))],
        tbecomments432: TBECOMMENT[Math.round(Math.random() * (TBECOMMENT.length - 1))],
        tbeweight432: TBEWEIGHT[Math.round(Math.random() * (TBEWEIGHT.length - 1))],
    };
}

function createNewRow5(no: number): RowData5 {

    return {
        no: no.toString(),
        termsname: TERMSNAME[Math.round(Math.random() * (TERMSNAME.length - 1))],
        inputtype: INPUTTYPE[Math.round(Math.random() * (INPUTTYPE.length - 1))],
        preview: PREVIEW[Math.round(Math.random() * (PREVIEW.length - 1))],
        editable: EDITABLE[Math.round(Math.random() * (EDITABLE.length - 1))],
        beforequote: BEFOREQUOTE[Math.round(Math.random() * (BEFOREQUOTE.length - 1))],
        endquote: ENDQUOTE[Math.round(Math.random() * (ENDQUOTE.length - 1))],
    };
}

function createNewRow6(id: number): RowData6 {

    return {
        srno: SRNO[Math.round(Math.random() * (SRNO.length - 1))],
        title: TITLE[Math.round(Math.random() * (TITLE.length - 1))],
        filename: FILENAME[Math.round(Math.random() * (FILENAME.length - 1))],
        documentclass: DOCUMENTCLASS[Math.round(Math.random() * (DOCUMENTCLASS.length - 1))],
        reference: REFERENCE[Math.round(Math.random() * (REFERENCE.length - 1))],
        atttype: ATTTYPE[Math.round(Math.random() * (ATTTYPE.length - 1))],
    };
}

function createNewRow622(id: number): RowData622 {

    return {
        srno622: SRNO[Math.round(Math.random() * (SRNO.length - 1))],
        title622: TITLE[Math.round(Math.random() * (TITLE.length - 1))],
        filename622: FILENAME[Math.round(Math.random() * (FILENAME.length - 1))],
        atttype622: ATTTYPE[Math.round(Math.random() * (ATTTYPE.length - 1))],
        tbeweight622: TBEWEIGHT[Math.round(Math.random() * (TBEWEIGHT.length - 1))],
        comment622: TBECOMMENT[Math.round(Math.random() * (TBECOMMENT.length - 1))],
    };
}

function createNewRow7(id: number): RowData7 {

    return {
        docsrno: DOCSRNO[Math.round(Math.random() * (DOCSRNO.length - 1))],
        outputtype: OUTPUTTYPE[Math.round(Math.random() * (OUTPUTTYPE.length - 1))],
        documentext: DOCUMENTTEXT[Math.round(Math.random() * (DOCUMENTTEXT.length - 1))],
    };
}

function createNewRow722(id: number): RowData722 {

    return {
        docsrno722: DOCSRNO[Math.round(Math.random() * (DOCSRNO.length - 1))],
        outputtype722: OUTPUTTYPE[Math.round(Math.random() * (OUTPUTTYPE.length - 1))],
        documentext722: DOCUMENTTEXT[Math.round(Math.random() * (DOCUMENTTEXT.length - 1))],
        documentext2722: DOCUMENTTEXT[Math.round(Math.random() * (DOCUMENTTEXT.length - 1))],
        tbeweight722: TBEWEIGHT[Math.round(Math.random() * (TBEWEIGHT.length - 1))],
        comment722: TBECOMMENT[Math.round(Math.random() * (TBECOMMENT.length - 1))],
    };
}

function createNewRow8(id: number): RowData8 {

    return {
        notesrno: NOTESRNO[Math.round(Math.random() * (NOTESRNO.length - 1))],
        notes: NOTES[Math.round(Math.random() * (NOTES.length - 1))],
    };
}

function createNewRow822(id: number): RowData822 {

    return {
        notes2822: NOTESRNO[Math.round(Math.random() * (NOTESRNO.length - 1))],
        notes822: NOTES[Math.round(Math.random() * (NOTES.length - 1))],
        tbeweight822: TBEWEIGHT[Math.round(Math.random() * (TBEWEIGHT.length - 1))],
        comment822: TBECOMMENT[Math.round(Math.random() * (TBECOMMENT.length - 1))],
    };
}

function createNewRow9(id: number): RowData9 {

    return {
        milestonenumber: MILESTONENUMBER[Math.round(Math.random() * (MILESTONENUMBER.length - 1))],
        milestonename: MILESTONENAME[Math.round(Math.random() * (MILESTONENAME.length - 1))],
        deliverabledescription: DELIVERABLEDESCRIPTION[Math.round(Math.random() * (DELIVERABLEDESCRIPTION.length - 1))],
        prevmilestonenumber: PREVMILESTONENUMBER[Math.round(Math.random() * (PREVMILESTONENUMBER.length - 1))],
        progresspercentage: PROGRESSPERCENTAGE[Math.round(Math.random() * (PROGRESSPERCENTAGE.length - 1))],
        stagepercentage: STAGEPERCENTAGE[Math.round(Math.random() * (STAGEPERCENTAGE.length - 1))],
        begindate: BEGINDATE[Math.round(Math.random() * (BEGINDATE.length - 1))],
    };
}

function createNewRow932(id: number): RowData932 {

    return {
        milestonenumber932: MILESTONENUMBER[Math.round(Math.random() * (MILESTONENUMBER.length - 1))],
        milestonename932: MILESTONENAME[Math.round(Math.random() * (MILESTONENAME.length - 1))],
        deliverabledescription932: DELIVERABLEDESCRIPTION[Math.round(Math.random() * (DELIVERABLEDESCRIPTION.length - 1))],
        prevmilestonenumber932: PREVMILESTONENUMBER[Math.round(Math.random() * (PREVMILESTONENUMBER.length - 1))],
        progresspercentage932: PROGRESSPERCENTAGE[Math.round(Math.random() * (PROGRESSPERCENTAGE.length - 1))],
        stagepercentage932: STAGEPERCENTAGE[Math.round(Math.random() * (STAGEPERCENTAGE.length - 1))],
        begindate932: BEGINDATE[Math.round(Math.random() * (BEGINDATE.length - 1))],
        tbeweight932: TBEWEIGHT[Math.round(Math.random() * (TBEWEIGHT.length - 1))],
        comment932: TBECOMMENT[Math.round(Math.random() * (TBECOMMENT.length - 1))],
    };
}

function createNewRow10(id: number): RowData10 {

    return {
        templatename: TEMPLATENAME[Math.round(Math.random() * (TEMPLATENAME.length - 1))],
        questionname: QUESTIONNAME[Math.round(Math.random() * (QUESTIONNAME.length - 1))],
        surveydescription: SURVEYDESCRIPTION[Math.round(Math.random() * (SURVEYDESCRIPTION.length - 1))],
        questiondetails: QUESTIONDETAILS[Math.round(Math.random() * (QUESTIONDETAILS.length - 1))],
    };
}

function createNewRow11(id: number): RowData11 {

    return {
        linesprno: LINESPRNO[Math.round(Math.random() * (LINESPRNO.length - 1))],
        lineno: LINENO[Math.round(Math.random() * (LINENO.length - 1))],
        partid: PARTID[Math.round(Math.random() * (PARTID.length - 1))],
        partdescription: PARTDESCRIPTION[Math.round(Math.random() * (PARTDESCRIPTION.length - 1))],
        uom: UOM[Math.round(Math.random() * (UOM.length - 1))],
        needbydate: NEEDBYDATE[Math.round(Math.random() * (NEEDBYDATE.length - 1))],
        startprice: STARTPRICE[Math.round(Math.random() * (STARTPRICE.length - 1))],
        targetprice: TARGETPRICE[Math.round(Math.random() * (TARGETPRICE.length - 1))],
    };
}

function createNewRow12(id: number): RowData12 {

    return {
        supplierId: SUPPLIERID[Math.round(Math.random() * (SUPPLIERID.length - 1))],
        supplierName: SUPPLIERNAME[Math.round(Math.random() * (SUPPLIERNAME.length - 1))],
        invitedDateTime: INVITEDDATETIME[Math.round(Math.random() * (INVITEDDATETIME.length - 1))],
        supplierEmail: SUPPLIEREMAIL[Math.round(Math.random() * (SUPPLIEREMAIL.length - 1))],
        altEmail: ALTEMAIL[Math.round(Math.random() * (ALTEMAIL.length - 1))],
        currencies: CURRENCIES[Math.round(Math.random() * (CURRENCIES.length - 1))],
        acknowledged: ACKNOWLEDGED[Math.round(Math.random() * (ACKNOWLEDGED.length - 1))],
        supplierStatus: SUPPLIERSTATUS[Math.round(Math.random() * (SUPPLIERSTATUS.length - 1))],
        responses: RESPONSES[Math.round(Math.random() * (RESPONSES.length - 1))],
    };
}

function createNewRow13(id: number): RowData13 {

    return {
        teamUserName: TEAMUSERNAME[Math.round(Math.random() * (TEAMUSERNAME.length - 1))],
        rfxRole: RFXROLE[Math.round(Math.random() * (RFXROLE.length - 1))],
        substituteName: SUBSTITUTENAME[Math.round(Math.random() * (SUBSTITUTENAME.length - 1))],
        accessLevel: ACCESSLEVEL[Math.round(Math.random() * (ACCESSLEVEL.length - 1))],
        pageAccess: PAGEACCESS[Math.round(Math.random() * (PAGEACCESS.length - 1))],
        teamName: TEAMNAME[Math.round(Math.random() * (TEAMNAME.length - 1))],
        teamDescription: TEAMDESCRIPTION[Math.round(Math.random() * (TEAMDESCRIPTION.length - 1))],
    };
}
function createNewRow14(id: number): RowData14 {

    return {
        headerRules: HEADERRULES[Math.round(Math.random() * (HEADERRULES.length - 1))],
    };
}
function createNewRow15(id: number): RowData15 {

    return {
        group: GROUP[Math.round(Math.random() * (GROUP.length - 1))],
    };
}
function createNewRow16(id: number): RowData16 {

    return {
        resRules: RESRULES[Math.round(Math.random() * (RESRULES.length - 1))],
    };
}

function createNewRow17(): RowData17 {

    return {
        costfactName: COSTFACTNAME[Math.round(Math.random() * (COSTFACTNAME.length - 1))],
        costfactType: COSTFACTTYPE[Math.round(Math.random() * (COSTFACTTYPE.length - 1))],
        costfactDesc: COSTFACTDESC[Math.round(Math.random() * (COSTFACTDESC.length - 1))],
        costfactExpectedValue: COSTFACTEXPECTEDVALUE[Math.round(Math.random() * (COSTFACTEXPECTEDVALUE.length - 1))],
        costfactValue: COSTFACTVALUE[Math.round(Math.random() * (COSTFACTVALUE.length - 1))],
        costfactComments: COSTFACCOMMENTS[Math.round(Math.random() * (COSTFACCOMMENTS.length - 1))],
    };
}

function createNewRow172(): RowData172 {

    return {
        tbecostfactName: COSTFACTNAME[Math.round(Math.random() * (COSTFACTNAME.length - 1))],
        tbecostfactType: COSTFACTTYPE[Math.round(Math.random() * (COSTFACTTYPE.length - 1))],
        tbecostfactDesc: COSTFACTDESC[Math.round(Math.random() * (COSTFACTDESC.length - 1))],
        tbecostfactExpectedValue: COSTFACTEXPECTEDVALUE[Math.round(Math.random() * (COSTFACTEXPECTEDVALUE.length - 1))],
        tbecostfactValue: COSTFACTVALUE[Math.round(Math.random() * (COSTFACTVALUE.length - 1))],
        tbecostfactComments: COSTFACCOMMENTS[Math.round(Math.random() * (COSTFACCOMMENTS.length - 1))],
        tbeweight2: TBEWEIGHT[Math.round(Math.random() * (TBEWEIGHT.length - 1))],
    };
}
function createNewRow182(): RowData182 {

    return {
        tbecostfactName: COSTFACTNAME[Math.round(Math.random() * (COSTFACTNAME.length - 1))],
        tbecostfactType: COSTFACTTYPE[Math.round(Math.random() * (COSTFACTTYPE.length - 1))],
        tbecostfactDesc: COSTFACTDESC[Math.round(Math.random() * (COSTFACTDESC.length - 1))],
        tbecostfactValue: COSTFACTVALUE[Math.round(Math.random() * (COSTFACTVALUE.length - 1))],
        tbecostfactComments: COSTFACCOMMENTS[Math.round(Math.random() * (COSTFACCOMMENTS.length - 1))],
        tbeweight2: TBEWEIGHT[Math.round(Math.random() * (TBEWEIGHT.length - 1))],
    };
}

function createNewRow19(): RowData19 {

    return {
        attributeItem: ATTRIBUTEITEM[Math.round(Math.random() * (ATTRIBUTEITEM.length - 1))],
        description: RESDESCRIPTION[Math.round(Math.random() * (RESDESCRIPTION.length - 1))],
        expectedValue: EXPECTEDVALUE[Math.round(Math.random() * (EXPECTEDVALUE.length - 1))],
        value: RESVALUE[Math.round(Math.random() * (RESVALUE.length - 1))],
        assocCosts: ASSOCCOSTS[Math.round(Math.random() * (ASSOCCOSTS.length - 1))],
        cost: COST[Math.round(Math.random() * (COST.length - 1))],
        comments: COMMENT[Math.round(Math.random() * (COMMENT.length - 1))],
    };
}
function createNewRow20(): RowData20 {

    return {
        tbeattributeItem: TBEATTRIBUTEITEM[Math.round(Math.random() * (TBEATTRIBUTEITEM.length - 1))],
        tbedescription: TBERESDESCRIPTION[Math.round(Math.random() * (TBERESDESCRIPTION.length - 1))],
        tbeexpectedValue: TBEEXPECTEDVALUE[Math.round(Math.random() * (TBEEXPECTEDVALUE.length - 1))],
        tbevalue: TBERESVALUE[Math.round(Math.random() * (TBERESVALUE.length - 1))],
        tbeassocCosts: TBEASSOCCOSTS[Math.round(Math.random() * (TBEASSOCCOSTS.length - 1))],
        tbecost: TBECOST[Math.round(Math.random() * (TBECOST.length - 1))],
        tbecomments: TBECOMMENT[Math.round(Math.random() * (TBECOMMENT.length - 1))],
        tbecomments2: TBECOMMENT[Math.round(Math.random() * (TBECOMMENT.length - 1))],
        tbeweight: TBEWEIGHT[Math.round(Math.random() * (TBEWEIGHT.length - 1))],
    };
}

