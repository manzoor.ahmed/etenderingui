import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {CommonModule} from '@angular/common';
import {DatatableModule} from '../../common/datatable/datatable.module';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import { TagsOverlayModule } from 'app/modules/common/tags-overlay/tags-overlay.module';
import {DrawerMiniModule} from '../../common/drawer-mini/drawer-mini.module';
import {RfqComponent} from './rfq.component';
import {MatDialogModule } from '@angular/material/dialog';
import {MatChipsModule} from '@angular/material/chips';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {MatTabsModule} from '@angular/material/tabs';
import {MatRadioModule} from '@angular/material/radio';
import {MatDividerModule} from '@angular/material/divider';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {AddCurrencyOverlayComponent} from './add-currency-overlay/add-currency-overlay.component';
import {AddNewAttributeListOverlayComponent} from './add-new-attributelist-overlay/add-new-attribute-list-overlay.component';
import {AddReusableAttributeOverlayComponent} from './add-reusable-attribute-overlay/add-reusable-attribute-overlay.component';
import {AddAttributeItemOverlayComponent} from './add-attribute-item-overlay/add-attribute-item-overlay.component';
import {AddNewAttributeOverlayComponent} from './add-new-attribute-overlay/add-new-attribute-overlay.component';
import {AddNewAttributeOverlay2Component} from './add-new-attribute-overlay2/add-new-attribute-overlay2.component';
import {AddNewAttributeOverlay3Component} from './add-new-attribute-overlay3/add-new-attribute-overlay3.component';
import {AddReusableCostfactorOverlayComponent} from './add-reusable-costfactor-overlay/add-reusable-costfactor-overlay.component';
import {AddNewCostfactorlistOverlayComponent} from './add-new-costfactorlist-overlay/add-new-costfactorlist-overlay.component';
import {AddNewCostfactorOverlayComponent} from './add-new-costfactor-overlay/add-new-costfactor-overlay.component';
import {AddCostfactorItemOverlayComponent} from './add-costfactor-item-overlay/add-costfactor-item-overlay.component';
import {AddNewPaymentschedulesOverlayComponent} from './add-new-paymentschedules-overlay/add-new-paymentschedules-overlay.component';
import {AddTermsOverlayComponent} from './add-terms-overlay/add-terms-overlay.component';
import {AddNewAttachmentOverlayComponent} from './add-new-attachment-overlay/add-new-attachment-overlay.component';
import {AddNewDocumenttextOverlayComponent} from './add-new-documenttext-overlay/add-new-documenttext-overlay.component';
import {AddNewNoteOverlayComponent} from './add-new-note-overlay/add-new-note-overlay.component';
import {AddNewDeliverableOverlayComponent} from './add-new-deliverable-overlay/add-new-deliverable-overlay.component';
import {AddSurveyOverlayComponent} from './add-survey-overlay/add-survey-overlay.component';
import {AddSurveyQuestionOverlayComponent} from './add-survey-question-overlay/add-survey-question-overlay.component';
import {CreateSurveyQuestionOverlayComponent} from './create-survey-question-overlay/create-survey-question-overlay.component';
import {AddSupplierOverlayComponent} from './add-supplier-overlay/add-supplier-overlay.component';
import {AddTeamOverlayComponent} from './add-team-overlay/add-team-overlay.component';
import {AddEditShippingOverlayComponent} from './add-edit-shipping-overlay/add-edit-shipping-overlay.component';
import {CopyToLinesOverlayComponent} from './copy-to-lines-overlay/copy-to-lines-overlay.component';
import {NgApexchartsModule} from 'ng-apexcharts';
import {AwardSelectedOverlayComponent} from './award-selected-overlay/award-selected-overlay.component';
import {AwardRecommendOverlayComponent} from './award-recommend-overlay/award-recommend-overlay.component';

const collaborationRoutes: Route[] = [
    {
        path     : '',
        component: RfqComponent
    }
];

@NgModule({
    declarations: [
        RfqComponent,
        AddCurrencyOverlayComponent,
        AddNewAttributeListOverlayComponent,
        AddReusableAttributeOverlayComponent,
        AddAttributeItemOverlayComponent,
        AddNewAttributeOverlayComponent,
        AddNewAttributeOverlay2Component,
        AddNewAttributeOverlay3Component,
        AddReusableCostfactorOverlayComponent,
        AddNewCostfactorlistOverlayComponent,
        AddNewCostfactorOverlayComponent,
        AddCostfactorItemOverlayComponent,
        AddNewPaymentschedulesOverlayComponent,
        AddTermsOverlayComponent,
        AddNewAttachmentOverlayComponent,
        AddNewDocumenttextOverlayComponent,
        AddNewNoteOverlayComponent,
        AddNewDeliverableOverlayComponent,
        AddSurveyOverlayComponent,
        AddSurveyQuestionOverlayComponent,
        CreateSurveyQuestionOverlayComponent,
        AddSupplierOverlayComponent,
        AddTeamOverlayComponent,
        AddEditShippingOverlayComponent,
        CopyToLinesOverlayComponent,
        AwardSelectedOverlayComponent,
        AwardRecommendOverlayComponent,
    ],
    imports: [
        RouterModule.forChild(collaborationRoutes),
        FormsModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        MatExpansionModule,
        DatatableModule,
        MatIconModule,
        CommonModule,
        MatInputModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatSelectModule,
        MatButtonModule,
        MatMenuModule,
        MatCheckboxModule,
        MatCardModule,
        TagsOverlayModule,
        DrawerMiniModule,
        MatDialogModule,
        MatChipsModule,
        MatDatepickerModule,
        MatTabsModule,
        MatRadioModule,
        MatDividerModule,
        MatStepperModule,
        MatSlideToggleModule,
        NgApexchartsModule
    ]
})
export class RfqModule
{
}
