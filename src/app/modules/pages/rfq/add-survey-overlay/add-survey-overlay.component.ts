import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData {
    id: string;
    name: string;
    description: string;
}
/** Constants used to fill up our data base. */
const NAME: string[] = [
    'Feedback', 'Response Time','Company Information'
];
const DESCRIPTION: string[] = [
    'Feedback questions', 'ABCD Name'
];

@Component({
    selector: 'add-survey-overlay',
    templateUrl: './add-survey-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddSurveyOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumns: string[] = ['id', 'name', 'description'];
    dataSource: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    addTeam = new FormGroup({
        teamName: new FormControl('Team Name One'),
        teamDescription: new FormControl('Team Description One'),
    });


    constructor(public dialogRef: MatDialogRef<AddSurveyOverlayComponent>,
                public dialog: MatDialog
    ) {
        const users = Array.from({length: 4}, (_, k) => createNewRow(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        id: id.toString(),
        name: NAME[Math.round(Math.random() * (NAME.length - 1))],
        description: DESCRIPTION[Math.round(Math.random() * (DESCRIPTION.length - 1))],
    };
}
