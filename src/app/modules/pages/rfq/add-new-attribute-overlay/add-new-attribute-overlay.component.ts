import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { AddNewAttributeOverlay2Component } from '../add-new-attribute-overlay2/add-new-attribute-overlay2.component';
import {AddNewAttributeOverlay3Component} from '../add-new-attribute-overlay3/add-new-attribute-overlay3.component';


@Component({
    selector: 'add-att-new-overlay',
    templateUrl: './add-new-attribute-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddNewAttributeOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    addTeam = new FormGroup({
        teamName: new FormControl('Team Name One'),
        teamDescription: new FormControl('Team Description One'),
    });
    formFieldHelpers: string[] = [''];
    registrationDetails2 = new FormGroup({
        expirationdate: new FormControl(new Date())
    });


    constructor(public dialogRef: MatDialogRef<AddNewAttributeOverlayComponent>,
                public dialog: MatDialog
    ) {
    }
    addNewAttributeItem2() {
        const dialogRef = this.dialog.open(AddNewAttributeOverlay2Component);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    addNewAttributeItem3() {
        const dialogRef = this.dialog.open(AddNewAttributeOverlay3Component);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }

    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }


}
