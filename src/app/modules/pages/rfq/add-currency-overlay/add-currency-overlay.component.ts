import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData {
    id: string;
    currencycode: string;
    rate: string;
    conversionfactor: string;
}
/** Constants used to fill up our data base. */
    const CURRENCYCODE: string[] = [
    'EUR', 'OMR', 'USD'
];
const RATE: string[] = [
    '2.15', '1.5', '3.5',
];
const CONVERSIONFACTOR: string[] = [
    '01', '02'
];

@Component({
    selector: 'add-currency-overlay',
    templateUrl: './add-currency-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddCurrencyOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumns: string[] = ['id', 'currencycode', 'rate', 'conversionfactor'];
    dataSource: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;


    constructor(public dialogRef: MatDialogRef<AddCurrencyOverlayComponent>,
                public dialog: MatDialog
    ) {
        const users = Array.from({length: 3}, (_, k) => createNewRow(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        id: id.toString(),
        currencycode: CURRENCYCODE[Math.round(Math.random() * (CURRENCYCODE.length - 1))],
        rate: RATE[Math.round(Math.random() * (RATE.length - 1))],
        conversionfactor: CONVERSIONFACTOR[Math.round(Math.random() * (CONVERSIONFACTOR.length - 1))],
    };
}
