import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData {
    supplierName: string;
    comment: string;

}
/** Constants used to fill up our data base. */
const SUPPLIERNAME: string[] = [
    'Supplier A', 'Supplier B'
];
const COMMENT: string[] = [
    'comment 1', 'comment 2','comment 3'
];

@Component({
    selector: 'award-selected-overlay',
    templateUrl: './award-selected-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AwardSelectedOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumn: string[] = ['id','supplierName', 'comment'];
    dataSource: MatTableDataSource<RowData>;
    dataSource2: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;


    constructor(public dialogRef: MatDialogRef<AwardSelectedOverlayComponent>,
                public dialog: MatDialog
    ) {
        const users = Array.from({length: 1}, (_, k) => createNewRow(k + 1));
        const users2 = Array.from({length: 3}, (_, k) => createNewRow2(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
        this.dataSource2 = new MatTableDataSource(users2);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        supplierName: SUPPLIERNAME[Math.round(Math.random() * (SUPPLIERNAME.length - 1))],
        comment: COMMENT[Math.round(Math.random() * (COMMENT.length - 1))],
    };
}

function createNewRow2(id: number): RowData {

    return {
        supplierName: SUPPLIERNAME[Math.round(Math.random() * (SUPPLIERNAME.length - 1))],
        comment: COMMENT[Math.round(Math.random() * (COMMENT.length - 1))],
    };
}
