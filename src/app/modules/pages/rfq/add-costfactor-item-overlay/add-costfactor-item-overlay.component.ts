import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData {
    id: string;
    name: string;
    type: string;
    description: string;
}
export interface RowData2 {
    id: string;
    name2: string;
}
/** Constants used to fill up our data base. */
const NAME: string[] = [
    'Sea Freight Cost LCL'
];
const TYPE: string[] = [
    'Fixed', 'Variable'
];
const DESCRIPTION: string[] = [
    'Description for ABCDE'
];

/** Constants used to fill up our data base. */
const NAME2: string[] = [
    'Manufacturing Costs', 'Freight Costs', 'Compliance'
];

@Component({
    selector: 'add-costfactor-item-overlay',
    templateUrl: './add-costfactor-item-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddCostfactorItemOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumns: string[] = ['id', 'name', 'description', 'type' ];
    displayedColumns2: string[] = ['id', 'name2'];
    dataSource: MatTableDataSource<RowData>;
    dataSource2: MatTableDataSource<RowData2>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;


    constructor(public dialogRef: MatDialogRef<AddCostfactorItemOverlayComponent>,
                public dialog: MatDialog
    ) {
        const users = Array.from({length: 3}, (_, k) => createNewRow(k + 1));
        const users2 = Array.from({length: 3}, (_, k) => createNewRow2(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
        this.dataSource2 = new MatTableDataSource(users2);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;
    }

    saveTemplate() {
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        id: id.toString(),
        name: NAME[Math.round(Math.random() * (NAME.length - 1))],
        type: TYPE[Math.round(Math.random() * (TYPE.length - 1))],
        description: DESCRIPTION[Math.round(Math.random() * (DESCRIPTION.length - 1))],
    };
}
/** Builds and returns a new createNewRow. */
function createNewRow2(id: number): RowData2 {

    return {
        id: id.toString(),
        name2: NAME2[Math.round(Math.random() * (NAME2.length - 1))],
    };
}
