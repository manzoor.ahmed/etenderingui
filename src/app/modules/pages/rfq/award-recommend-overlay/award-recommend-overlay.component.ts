import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

@Component({
    selector: 'award-recommend-overlay',
    templateUrl: './award-recommend-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AwardRecommendOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;


    constructor(public dialogRef: MatDialogRef<AwardRecommendOverlayComponent>,
                public dialog: MatDialog
    ) {
    }
    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }
}
