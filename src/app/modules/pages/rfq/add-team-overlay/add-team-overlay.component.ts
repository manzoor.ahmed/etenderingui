import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData {
    teamUserName: string;
    teamPosition: string;
    teamEmail: string;
}

export interface RowData2 {
    team: string;
    description: string;
}
/** Constants used to fill up our data base. */
const TEAMUSERNAME: string[] = [
    'Muhammed Munaad', 'Muhammed Rafiq'
];
const TEAMPOSITION: string[] = [
    'Technical Lead'
];
const TEAMEMAIL: string[] = [
    'A@gmail.com', 'B@gmail.com'
];

/** Constants used to fill up our data base. */
const TEAM: string[] = [
    'Quality Engineering', 'Quality Raw Materials'
];
const DESCRIPTION: string[] = [
    'Engineering Quality Team', 'Packaging Quality Team'
];

@Component({
    selector: 'add-team-overlay',
    templateUrl: './add-team-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddTeamOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumn: string[] = ['id', 'teamUserName', 'teamPosition', 'teamEmail'];
    dataSource: MatTableDataSource<RowData>;

    displayedColumn2: string[] = ['id', 'team', 'description'];
    dataSource2: MatTableDataSource<RowData2>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;


    constructor(public dialogRef: MatDialogRef<AddTeamOverlayComponent>,
                public dialog: MatDialog
    ) {
        const users = Array.from({length: 3}, (_, k) => createNewRow(k + 1));
        const users2 = Array.from({length: 3}, (_, k) => createNewRow2(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
        this.dataSource2 = new MatTableDataSource(users2);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        teamUserName: TEAMUSERNAME[Math.round(Math.random() * (TEAMUSERNAME.length - 1))],
        teamPosition: TEAMPOSITION[Math.round(Math.random() * (TEAMPOSITION.length - 1))],
        teamEmail: TEAMEMAIL[Math.round(Math.random() * (TEAMEMAIL.length - 1))],
    };
}

/** Builds and returns a new createNewRow. */
function createNewRow2(id: number): RowData2 {

    return {
        team: TEAM[Math.round(Math.random() * (TEAM.length - 1))],
        description: DESCRIPTION[Math.round(Math.random() * (DESCRIPTION.length - 1))],
    };
}
