import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';


@Component({
    selector: 'att-new-add-overlay2',
    templateUrl: './add-new-attribute-overlay2.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddNewAttributeOverlay2Component {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    addTeam = new FormGroup({
        teamName: new FormControl('Team Name One'),
        teamDescription: new FormControl('Team Description One'),
    });
    formFieldHelpers: string[] = [''];
    registrationDetails2 = new FormGroup({
        expirationdate: new FormControl(new Date())
    });


    constructor(public dialogRef: MatDialogRef<AddNewAttributeOverlay2Component>,
                public dialog: MatDialog
    ) {
    }

    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }


}
