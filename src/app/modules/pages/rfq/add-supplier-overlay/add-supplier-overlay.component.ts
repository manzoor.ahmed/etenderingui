import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData {
    supplierId: string;
    supplierName: string;
    supplierEmail: string;
    supplierStatus: string;
    invited: string;
    awarded: string;
    category: string;
}
/** Constants used to fill up our data base. */
const SUPPLIERID: string[] = [
    '10101', '10101'
];
const SUPPLIERNAME: string[] = [
    'Sup A', 'Sup B'
];
const SUPPLIEREMAIL: string[] = [
    'A@gmail.com', 'B@gmail.com'
];
const SUPPLIERSTATUS: string[] = [
    'Intend to participate', 'Not Interested to participate','Response Submitted', 'Yet to acknowledge'
];
const INVITED: string[] = [
    '13', '45','63'
];
const AWARDED: string[] = [
    '3', '5', '2'
];
const CATEGORY: string[] = [
    'Oil & Gas', 'Office Supplies', 'IT Supplies'
];

@Component({
    selector: 'add-supplier-overlay',
    templateUrl: './add-supplier-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddSupplierOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumn: string[] = ['id', 'supplierId', 'supplierName', 'supplierStatus', 'supplierEmail','category', 'invited', 'awarded'];
    dataSource: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;


    constructor(public dialogRef: MatDialogRef<AddSupplierOverlayComponent>,
                public dialog: MatDialog
    ) {
        const users = Array.from({length: 3}, (_, k) => createNewRow(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        supplierId: SUPPLIERID[Math.round(Math.random() * (SUPPLIERID.length - 1))],
        supplierName: SUPPLIERNAME[Math.round(Math.random() * (SUPPLIERNAME.length - 1))],
        supplierEmail: SUPPLIEREMAIL[Math.round(Math.random() * (SUPPLIEREMAIL.length - 1))],
        supplierStatus: SUPPLIERSTATUS[Math.round(Math.random() * (SUPPLIERSTATUS.length - 1))],
        invited: INVITED[Math.round(Math.random() * (INVITED.length - 1))],
        awarded: AWARDED[Math.round(Math.random() * (AWARDED.length - 1))],
        category: CATEGORY[Math.round(Math.random() * (CATEGORY.length - 1))],
    };
}
