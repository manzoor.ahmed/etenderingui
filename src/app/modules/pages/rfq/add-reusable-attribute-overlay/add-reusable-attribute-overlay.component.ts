import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData {
    id: string;
    title: string;
    name: string;
    group: string;
}
/** Constants used to fill up our data base. */
    const TITLE: string[] = [
    'Company Information'
];
const NAME: string[] = [
    'ABCD Name'
];
const GROUP: string[] = [
    'Group 01'
];

@Component({
    selector: 'add-reusable-attribute-overlay',
    templateUrl: './add-reusable-attribute-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddReusableAttributeOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumns: string[] = ['id', 'title', 'name', 'group'];
    dataSource: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;


    constructor(public dialogRef: MatDialogRef<AddReusableAttributeOverlayComponent>,
                public dialog: MatDialog
    ) {
        const users = Array.from({length: 3}, (_, k) => createNewRow(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }

    saveTemplate() {
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        id: id.toString(),
        title: TITLE[Math.round(Math.random() * (TITLE.length - 1))],
        name: NAME[Math.round(Math.random() * (NAME.length - 1))],
        group: GROUP[Math.round(Math.random() * (GROUP.length - 1))],
    };
}
