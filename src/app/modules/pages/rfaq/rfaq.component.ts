import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData {
    stepNumber: string;
    id: string;
    name: string;
    address: string;
    status: string;
    rules: string;
    template: string;

}

export interface RowData4 {
    payno: string;
    description2: string;
    work: string;
    milestone: string;
    payment: string;
    retention: string;
    release: string;
    releasevalue: string;
}

export interface RowData6 {
    srno: string;
    title: string;
    filename: string;
    documentclass: string;
    reference: string;
    atttype: string;
}

export interface RowData7 {
    docsrno: string;
    outputtype: string;
    documentext: string;
}

export interface RowData8 {
    notesrno: string;
    notes: string;
}

export interface RowData9 {
    milestonenumber: string;
    milestonename: string;
    deliverabledescription: string;
    prevmilestonenumber: string;
    progresspercentage: string;
    stagepercentage: string;
    begindate: string;
}

export interface RowData15 {
    group: string;
}

export interface RowData17 {
    costfactName: string;
    costfactType: string;
    costfactDesc: string;
    costfactExpectedValue: string;
    costfactValue: string;
    costfactComments: string;
}

export interface RowData19 {
    attributeItem: string;
    description: string;
    expectedValue: string;
    value: string;
    assocCosts: string;
    cost: string;
    comments: string;
}

/** Constants used to fill up our data base. */
const PAYNO: string[] = [
    '10101', '10102'
];
const DESCRIPTION2: string[] = [
    'Freight Cost for single shipment'
];
const WORK: string[] = [
    '01%'
];
const MILESTONE: string[] = [
    '01', '02'
];
const PAYMENT: string[] = [
    '20%', '30%'
];
const RETENTION: string[] = [
    '25%', '34%'
];
const RELEASE: string[] = [
    '20%', '30%'
];
const RELEASEVALUE: string[] = [
    '87234 USD'
];


/** Constants used to fill up our data base. */
const SRNO: string[] = [
    '10101', '10102'
];
const TITLE: string[] = [
    'xxxxxxxxxxxx'
];
const FILENAME: string[] = [
    'abcd.xml', 'pqrs.xml'
];
const DOCUMENTCLASS: string[] = [
    'XXX', 'YYY'
];
const REFERENCE: string[] = [
    'PR 001', 'PR 002'
];
const ATTTYPE: string[] = [
    'Technical', 'Non Technical'
];


/** Constants used to fill up our data base. */
const DOCSRNO: string[] = [
    '10101', '10102'
];
const OUTPUTTYPE: string[] = [
    'XXX', 'YYY'
];
const DOCUMENTTEXT: string[] = [
    'xxxxx xxxxxx xxxxx'
];


/** Constants used to fill up our data base. */
const NOTESRNO: string[] = [
    '10101', '10102'
];
const NOTES: string[] = [
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', 'Ipsum is simply dummy text and typesetting industry. '
];

/** Constants used to fill up our data base. */
const MILESTONENUMBER: string[] = [
    '01', '02'
];
const MILESTONENAME: string[] = [
    'Design Approval', 'Design Approval'
];
const DELIVERABLEDESCRIPTION: string[] = [
    'lorem ipsum text'
];
const PREVMILESTONENUMBER: string[] = [
    'PR 001', 'PR 002'
];
const PROGRESSPERCENTAGE: string[] = [
    '25%', '75%'
];
const STAGEPERCENTAGE: string[] = [
    '15%', '35%'
];
const BEGINDATE: string[] = [
    '02 -12-2021', '22 -12-2021'
];


/** Constants used to fill up our data base. */
const COSTFACTNAME: string[] = [
    'Line Price', 'Cost of Start-Up & C/Ammissioning Spares'
];
const COSTFACTTYPE: string[] = [
    'Total', 'Fixed'
];
const COSTFACTDESC: string[] = [
    'Charges applicable for R&', 'cost of administration '
];
const COSTFACTEXPECTEDVALUE: string[] = [
    '$25,000.00', '$35,000.00'
];
const COSTFACTVALUE: string[] = [
    '$5,000.00', '$5,000.00'
];
const COSTFACCOMMENTS: string[] = [
    'comment'
];


/** Constants used to fill up our data base. */
const ATTRIBUTEITEM: string[] = [
    'Minimum Order', 'Packaging'
];
const RESDESCRIPTION: string[] = [
    'Min order quantity', 'When can you ship?'
];
const EXPECTEDVALUE: string[] = [
    '100', 'Purposes/Overlays/Structures'
];
const RESVALUE: string[] = [
    'Number', 'Text'
];
const ASSOCCOSTS: string[] = [
    'Yes', 'No'
];
const COST: string[] = [
    '236.00 USD', '456.00 USD'
];
const COMMENT: string[] = [
    ''
];

/** Constants used to fill up our data base. */
const TBECOMMENT: string[] = [
    'comment description'
];
const TBEWEIGHT: string[] = [
    '05', '02'
];

@Component({
    selector     : 'rfaq',
    templateUrl  : './rfaq.component.html',
    encapsulation: ViewEncapsulation.None,
    styles         : [
        /* language=SCSS */
        `
            .attribute-items-parent-grid {
                grid-template-columns: 40px 20% 20% auto 96px;

                @screen md {
                    grid-template-columns: 40px 10% 10% auto 96px;
                }
            }
            .attribute-items-parent-grid2 {
                grid-template-columns: 20% 20% auto 96px;

                @screen md {
                    grid-template-columns: 10% 10% auto 96px;
                }
            }
            .attribute-items-inner-grid {
                grid-template-columns: 40px 10% 12% auto 10% 10% 10% 10%;
                min-width: 1200px;

                @screen md {
                    grid-template-columns: 40px 10% 12% auto 10% 10% 10% 10%;
                }
            }
            .noneditable-attribute-items-inner-grid {
                grid-template-columns: 35% auto 30%;
                min-width: 500px;

                @screen md {
                    grid-template-columns: 35% auto 30%;
                }
            }
            .noneditable-attribute-items-inner-grid2 {
                grid-template-columns: 25% 25% 20% auto;
                min-width: 500px;

                @screen md {
                    grid-template-columns: 25% 25% 20% auto;
                }
            }
            .costfactor-items-parent-grid {
                grid-template-columns: 40px 20% auto 96px;

                @screen md {
                    grid-template-columns: 40px 10% auto 96px;
                }
            }
            .noneditable-costfactor-items-parent-grid {
                grid-template-columns: 20% auto 96px;

                @screen md {
                    grid-template-columns: 10% auto 96px;
                }
            }
            .costfactor-items-inner-grid {
                grid-template-columns: 40px 10% 12% 12% 15% 10% 10% 10% 14%;
                min-width: 1200px;

                @screen md {
                    grid-template-columns: 40px 10% 12% 12% 15% 10% 10% 10% 14%;
                }
            }
            .survey-parent-grid {
                grid-template-columns: 20% auto 120px 96px;

                @screen md {
                    grid-template-columns: 20% auto 120px 96px;
                }
            }
            .scoring-grid {
                grid-template-columns: auto 260px 260px 96px;
                min-width: 800px;

                @screen md {
                    grid-template-columns: auto 260px 260px 96px;
                    min-width: 800px;
                }
            }
            .scoring-inner-grid {
                grid-template-columns: auto 160px 160px;

                @screen md {
                    grid-template-columns: auto 160px 160px;
                }
            }
            .survey-inner-grid {
                grid-template-columns: 20% auto 14% 12% 20%;
                min-width: 900px;

                @screen md {
                    grid-template-columns: 20% auto 14% 12% 20%;
                }
            }
            .line-items-parent-grid {
                grid-template-columns: 40px 60px 60px 90px auto 80px 80px 110px 80px 100px 100px 96px;

                @screen md {
                    grid-template-columns: 40px 60px 60px 90px auto 80px 80px 110px 80px 100px 100px 96px;
                }
            }
            .noneditable-line-items-parent-grid {
                grid-template-columns: 60px 60px 90px auto 80px 80px 110px 80px 100px 100px 96px;

                @screen md {
                    grid-template-columns: 60px 60px 90px auto 80px 80px 110px 80px 100px 100px 96px;
                }
            }
            .noneditable-line-items-parent-grid2 {
                grid-template-columns: 60px 90px auto 80px 80px 110px 100px 100px 96px;

                @screen md {
                    grid-template-columns: 60px 90px auto 80px 80px 110px 100px 100px 96px;
                }
            }
            .scoring-line-items-grid {
                grid-template-columns: 60px 140px auto 260px 260px;

                @screen md {
                    grid-template-columns: 60px 140px auto 260px 260px;
                }
            }
            .scoring-line-items-criteria-grid {
                grid-template-columns: auto 120px 120px 135px;

                @screen md {
                    grid-template-columns: auto 120px 120px 135px;
                }
            }
            .awardline-grid {
                grid-template-columns: 90px 80px 120px 110px 180px 180px 120px;

                @screen md {
                    grid-template-columns: 90px 80px 120px 110px 180px 180px 120px;
                }
            }
            .deviation-grid {
                grid-template-columns: 40px 150px 200px 150px 150px 150px;

                @screen md {
                    grid-template-columns: 40px 150px 200px 150px 150px 150px;
                }
            }
            .noneditable-deviation-grid {
                grid-template-columns: 150px 250px 150px 150px 150px;

                @screen md {
                    grid-template-columns: 150px 250px 150px 150px 150px;
                }
            }
            .overall-supplier-grid {
                grid-template-columns: 140px 80px 160px 60px 60px 60px 180px;

                @screen md {
                    grid-template-columns: 140px 80px 160px 60px 60px 60px 180px;
                }
            }
            .shipment-grid {
                grid-template-columns: 40px 100px 100px 100px 180px 150px 150px 150px;

                @screen md {
                    grid-template-columns: 40px 100px 100px 100px 180px 150px 150px 150px;
                }
            }
            .noneditable-shipment-grid {
                grid-template-columns: 100px 100px 100px 180px 150px 150px 150px;

                @screen md {
                    grid-template-columns: 100px 100px 100px 180px 150px 150px 150px;
                }
            }
            .tbe-noneditable-line-items-parent-grid {
                grid-template-columns: 80px 80px 100px 200px 80px 80px 130px 100px 120px 120px 100px 100px 140px 180px 96px;

                @screen md {
                    grid-template-columns: 80px 80px 100px 200px 80px 80px 130px 100px 120px 120px 100px 100px 140px 180px 96px;
                }
            }
        `
    ],
})

export class RfaqComponent
{
    displayedColumn4: string[] = ['id', 'payno', 'description2', 'type2', 'work', 'milestone', 'payment','retention', 'release','releasevalue', 'visibility'];
    displayedColumn42: string[] = ['payno', 'description2', 'work', 'milestone', 'payment','retention', 'release','releasevalue'];
    displayedColumn43: string[] = ['payno', 'description2', 'work', 'milestone', 'payment','retention', 'release','releasevalue'];
    displayedColumn6: string[] = ['id', 'srno', 'title', 'filename','attachment', 'documentclass', 'reference','internalrfq','atttype'];
    displayedColumn62: string[] = ['srno', 'title', 'filename','attachment','atttype', 'attachment2', 'comment'];
    displayedColumn7: string[] = ['id', 'docsrno','outputtype','documentext', 'type3', 'visibility3'];
    displayedColumn72: string[] = ['documentext', 'documentext2', 'comment'];
    displayedColumn8: string[] = ['id', 'notesrno', 'notes', 'type4', 'visibility4'];
    displayedColumn82: string[] = ['notes', 'notes2', 'comment'];
    displayedColumn9: string[] = ['id', 'milestonenumber', 'milestonename', 'deliverabledescription','type5','prevmilestonenumber', 'progresspercentage', 'stagepercentage','begindate','visibility5'];
    displayedColumn92: string[] = ['milestonenumber', 'milestonename', 'deliverabledescription','prevmilestonenumber', 'progresspercentage', 'stagepercentage','begindate'];
    displayedColumn93: string[] = ['milestonenumber', 'milestonename', 'deliverabledescription','prevmilestonenumber', 'progresspercentage', 'stagepercentage','begindate','comment'];
    displayedColumn17: string[] = ['costfactName', 'costfactType', 'costfactDesc', 'costfactExpectedValue', 'costfactValue', 'costfactComments'];
    displayedColumn19: string[] = ['attributeItem', 'description', 'expectedValue', 'value', 'assocCosts', 'cost', 'comments'];


    dataSource4: MatTableDataSource<RowData4>;
    dataSource6: MatTableDataSource<RowData6>;
    dataSource7: MatTableDataSource<RowData7>;
    dataSource8: MatTableDataSource<RowData8>;
    dataSource9: MatTableDataSource<RowData9>;
    dataSource17: MatTableDataSource<RowData17>;
    dataSource19: MatTableDataSource<RowData19>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;

    /**
     * Constructor
     */
    constructor(public dialog: MatDialog)
    {
        const users4 = Array.from({length: 6}, (_, k) => createNewRow4(k + 1));
        const users6 = Array.from({length:3}, (_, k) => createNewRow6(k + 1));
        const users7 = Array.from({length:3}, (_, k) => createNewRow7(k + 1));
        const users8 = Array.from({length:3}, (_, k) => createNewRow8(k + 1));
        const users9 = Array.from({length:3}, (_, k) => createNewRow9(k + 1));
        const users17 = Array.from({length:3}, (_, k) => createNewRow17());
        const users19 = Array.from({length:3}, (_, k) => createNewRow19());

        this.dataSource4 = new MatTableDataSource(users4);
        this.dataSource6 = new MatTableDataSource(users6);
        this.dataSource7 = new MatTableDataSource(users7);
        this.dataSource8 = new MatTableDataSource(users8);
        this.dataSource9 = new MatTableDataSource(users9);
        this.dataSource17 = new MatTableDataSource(users17);
        this.dataSource19 = new MatTableDataSource(users19);
    }
    ngAfterViewInit() {
        this.dataSource4.paginator = this.paginator;
        this.dataSource4.sort = this.sort;
        this.dataSource6.paginator = this.paginator;
        this.dataSource6.sort = this.sort;
        this.dataSource7.paginator = this.paginator;
        this.dataSource7.sort = this.sort;
        this.dataSource8.paginator = this.paginator;
        this.dataSource8.sort = this.sort;
        this.dataSource9.paginator = this.paginator;
        this.dataSource9.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource4.filter = filterValue.trim().toLowerCase();
        this.dataSource6.filter = filterValue.trim().toLowerCase();
        this.dataSource7.filter = filterValue.trim().toLowerCase();
        this.dataSource8.filter = filterValue.trim().toLowerCase();
        this.dataSource9.filter = filterValue.trim().toLowerCase();
    }


    isShow = true;
    isShow2 = true;

    toggleDisplay() {
        this.isShow = !this.isShow;
    }
    toggleDisplay2() {
        this.isShow2 = !this.isShow2;
    }

}


function createNewRow4(id: number): RowData4 {

    return {
        payno: PAYNO[Math.round(Math.random() * (PAYNO.length - 1))],
        description2: DESCRIPTION2[Math.round(Math.random() * (DESCRIPTION2.length - 1))],
        work: WORK[Math.round(Math.random() * (WORK.length - 1))],
        milestone: MILESTONE[Math.round(Math.random() * (MILESTONE.length - 1))],
        payment: PAYMENT[Math.round(Math.random() * (PAYMENT.length - 1))],
        retention: RETENTION[Math.round(Math.random() * (RETENTION.length - 1))],
        release: RELEASE[Math.round(Math.random() * (RELEASE.length - 1))],
        releasevalue: RELEASEVALUE[Math.round(Math.random() * (RELEASEVALUE.length - 1))],
    };
}

function createNewRow6(id: number): RowData6 {

    return {
        srno: SRNO[Math.round(Math.random() * (SRNO.length - 1))],
        title: TITLE[Math.round(Math.random() * (TITLE.length - 1))],
        filename: FILENAME[Math.round(Math.random() * (FILENAME.length - 1))],
        documentclass: DOCUMENTCLASS[Math.round(Math.random() * (DOCUMENTCLASS.length - 1))],
        reference: REFERENCE[Math.round(Math.random() * (REFERENCE.length - 1))],
        atttype: ATTTYPE[Math.round(Math.random() * (ATTTYPE.length - 1))],
    };
}

function createNewRow7(id: number): RowData7 {

    return {
        docsrno: DOCSRNO[Math.round(Math.random() * (DOCSRNO.length - 1))],
        outputtype: OUTPUTTYPE[Math.round(Math.random() * (OUTPUTTYPE.length - 1))],
        documentext: DOCUMENTTEXT[Math.round(Math.random() * (DOCUMENTTEXT.length - 1))],
    };
}

function createNewRow8(id: number): RowData8 {

    return {
        notesrno: NOTESRNO[Math.round(Math.random() * (NOTESRNO.length - 1))],
        notes: NOTES[Math.round(Math.random() * (NOTES.length - 1))],
    };
}

function createNewRow9(id: number): RowData9 {

    return {
        milestonenumber: MILESTONENUMBER[Math.round(Math.random() * (MILESTONENUMBER.length - 1))],
        milestonename: MILESTONENAME[Math.round(Math.random() * (MILESTONENAME.length - 1))],
        deliverabledescription: DELIVERABLEDESCRIPTION[Math.round(Math.random() * (DELIVERABLEDESCRIPTION.length - 1))],
        prevmilestonenumber: PREVMILESTONENUMBER[Math.round(Math.random() * (PREVMILESTONENUMBER.length - 1))],
        progresspercentage: PROGRESSPERCENTAGE[Math.round(Math.random() * (PROGRESSPERCENTAGE.length - 1))],
        stagepercentage: STAGEPERCENTAGE[Math.round(Math.random() * (STAGEPERCENTAGE.length - 1))],
        begindate: BEGINDATE[Math.round(Math.random() * (BEGINDATE.length - 1))],
    };
}

function createNewRow17(): RowData17 {

    return {
        costfactName: COSTFACTNAME[Math.round(Math.random() * (COSTFACTNAME.length - 1))],
        costfactType: COSTFACTTYPE[Math.round(Math.random() * (COSTFACTTYPE.length - 1))],
        costfactDesc: COSTFACTDESC[Math.round(Math.random() * (COSTFACTDESC.length - 1))],
        costfactExpectedValue: COSTFACTEXPECTEDVALUE[Math.round(Math.random() * (COSTFACTEXPECTEDVALUE.length - 1))],
        costfactValue: COSTFACTVALUE[Math.round(Math.random() * (COSTFACTVALUE.length - 1))],
        costfactComments: COSTFACCOMMENTS[Math.round(Math.random() * (COSTFACCOMMENTS.length - 1))],
    };
}

function createNewRow19(): RowData19 {

    return {
        attributeItem: ATTRIBUTEITEM[Math.round(Math.random() * (ATTRIBUTEITEM.length - 1))],
        description: RESDESCRIPTION[Math.round(Math.random() * (RESDESCRIPTION.length - 1))],
        expectedValue: EXPECTEDVALUE[Math.round(Math.random() * (EXPECTEDVALUE.length - 1))],
        value: RESVALUE[Math.round(Math.random() * (RESVALUE.length - 1))],
        assocCosts: ASSOCCOSTS[Math.round(Math.random() * (ASSOCCOSTS.length - 1))],
        cost: COST[Math.round(Math.random() * (COST.length - 1))],
        comments: COMMENT[Math.round(Math.random() * (COMMENT.length - 1))],
    };
}


