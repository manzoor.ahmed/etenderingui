import {Component, ViewChild, ViewEncapsulation,ChangeDetectorRef} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {AddOverlayComponent} from './add-overlay/add-overlay.component';
import { Router } from '@angular/router';
import { CostFactorGroupService } from 'app/services/cost-factor-group.service';
import { CostFactorSearchModel } from 'app/models/cost-factor-search-model';
import { CostFactorGroupSearchModel } from 'app/models/cost-factor-group-search-model';
import { FuseConfirmationService } from '@fuse/services/confirmation/confirmation.service';
import { FuseAlertService } from '@fuse/components/alert';
import { CostFactorTextViewModel } from 'app/models/ViewModels/cost-factor-view-model';


@Component({
    selector     : 'rfx',
    templateUrl  : './list-detail.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ListDetailComponent
{
    displayedColumns: string[] = ['id', 'factor', 'description'];
    
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    pageEvent: PageEvent;
    panelOpenState = false;
    costFactorGroupID : any;
     costFactorGroups: any = new CostFactorGroupSearchModel();
     costFactor : any  = new CostFactorSearchModel(); 
     showMsg: boolean = false;
     Message:string="";
     actualCostFactorModels:  any =[];
     cfName : string = "" ;
     cfDescription:string = "" ;
     cfType: string = "";
     cfTypeName: any;
     dataTypeList : any=[] ;
     selectedDataType:any;
    /**
     * Constructor
     */
    constructor(public dialog: MatDialog,
        private costFactorGroupService:CostFactorGroupService,private router: Router
        ,private changeDetectorRef:ChangeDetectorRef
        ,  private _fuseConfirmationService: FuseConfirmationService,
        private _fuseAlertService: FuseAlertService)
    {
       
        this.costFactorGroupID = this.router.getCurrentNavigation().extras.state.Id
        this.costFactorGroups.pageSize = 5;
        this.costFactorGroups.page = 1;
    }
   
    FetchBasicData() {
        
        this.costFactorGroupService.getCostFactorGroupById(this.costFactorGroupID).subscribe(
            result => {
            console.log(result);
            
            this.costFactorGroups = result.data;
          
        if(!this.costFactorGroups.costFactorModels)
        {
            this.costFactorGroups.costFactorModels=[];
        }
            for(var i=0; i<result.data.costFactorModels.length; i++){
                this.actualCostFactorModels.push(result.data.costFactorModels[i]);
            }
            this.dataTypeList =  result.data.dataTypeList;
            }
        );
    }

    ngOnInit() {
        this.dismiss("successerror");
        this.FetchBasicData();
    }
    
        /**
 * Dismiss the alert via the service
 *
 * @param name
 */
dismiss(name: string): void
{
    this._fuseAlertService.dismiss(name);
}


    OnPaginateChange(event:PageEvent)
    {
    let page=event.pageIndex;
    let size=event.pageSize;
    page=page+1;
    this.costFactorGroups.pageSize=event.pageSize;
    this.costFactorGroups.page=page;
    this.FetchBasicData();
//  this.dataSource=   this.CreatePaginationData(page,size);
    }
 
   
    sortData(sort: Sort){
        debugger;
        this.costFactor.direction = sort.direction;
        this.costFactor.column = sort.active;
        this.FetchBasicData();
    }

    saveChanges(){
        debugger;
        for (var i = 0; i < this.actualCostFactorModels.length; i++) {
            this.actualCostFactorModels[i].costFactorGroupId= this.costFactorGroupID;
            
        }
       //this.costFactorGroups.costFactorModels.push({id:"F9737153-CF99-4049-2F59-08D978387FC5"});
        this.costFactorGroupService.SaveCostFactorGroupMapping(this.actualCostFactorModels).subscribe(
            result => {
                
                console.log(result);
                debugger;
                // this.costFactorGroups = result;
                this.Message="Added";
                this.show("successerror");            
            }
        );
    }
    
    openDialog() {
        debugger;
        const  dialogRef = this.dialog.open(AddOverlayComponent,{data:{AddedCostFactor:this.costFactorGroups}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            debugger;
            let data:any=[];
            for (var i = 0; i < this.costFactorGroups.costFactorModels.length; i++) {
                // if(result[i].isChecked==true)
                // {
                    //this.costFactorGroups.costFactorModels.push(result[i]);
                // }
                data.push(this.costFactorGroups.costFactorModels[i]);
            }
            for(var i=0; i<result.length; i++)
            {
                if(result[i].isChecked==true && result[i].isDisabled==false)
                {
                    data.push(result[i]);
                }
               
            }
            this.Message="Added";
            this.show("successerror");  
            //this.show("successerror");
            //this.changeDetectorRef.detectChanges();
            this.costFactorGroups.costFactorModels=[];
            this.costFactorGroups.costFactorModels=data;
            this.actualCostFactorModels=data;
        });
    }
    Delete(row)
    {
        
       row.IsChecked=false;
       const dialogRef = this._fuseConfirmationService.open({
        "title": "Remove contact",
        "message": "Are you sure you want to delete this record?",
        "icon": {
          "show": true,
          "name": "heroicons_outline:exclamation",
          "color": "warn"
        },
        "actions": {
          "confirm": {
            "show": true,
            "label": "Remove",
            "color": "warn"
          },
          "cancel": {
            "show": true,
            "label": "Cancel"
          }
        },
        "dismissible": true
      });

      dialogRef.addPanelClass('confirmation-dialog');
      dialogRef.afterClosed().subscribe((result) => {
        if (result == "confirmed") {
debugger;
            for(var kk=0;kk<this.actualCostFactorModels.length;kk++)

            {

               if(row.id == this.actualCostFactorModels[kk].id){

                   this.actualCostFactorModels[kk].isChecked=false;

               }

            }

            var attributeIndex = -1;

            for(var kk=0;kk<this.costFactorGroups.costFactorModels.length;kk++)

            {

               if(row.id == this.costFactorGroups.costFactorModels[kk].id){

                  attributeIndex = kk;

               }

            }

            this.costFactorGroups.costFactorModels= this.costFactorGroups.costFactorModels.filter(obj => obj.id !== row.id);

         row.IsChecked=false;



          }
      });
   
    }

    show(name: string): void
    {
        this._fuseAlertService.show(name);
    }
    
    searchData() {
     debugger;
        let dataList: CostFactorTextViewModel[] = this.actualCostFactorModels;

        if(this.cfName && this.cfName != ""){

            dataList = dataList.filter((data: CostFactorTextViewModel) => {

                return data.cfName.indexOf(this.cfName) > -1;

            })

        }

        if(this.cfDescription && this.cfDescription != ""){

            dataList = dataList.filter((data: CostFactorTextViewModel) => {

                return data.cfDescription.indexOf(this.cfDescription) > -1;

            })

        }
       
        if(this.cfTypeName && this.cfTypeName != ""){

            dataList = dataList.filter((data: CostFactorTextViewModel) => {

                return data.cfTypeName.indexOf(this.cfTypeName) > -1;

            })

        }
        this.costFactorGroups.costFactorModels = dataList;

    }
}
