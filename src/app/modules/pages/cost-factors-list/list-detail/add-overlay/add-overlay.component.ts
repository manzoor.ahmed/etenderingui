import { Component, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import { CostFactorSearchModel } from 'app/models/cost-factor-search-model';
import { CostFactorGroupService } from 'app/services/cost-factor-group.service';
import { CostFactorService } from 'app/services/cost-factor.service';
import { CostFactorGroupSearchModel } from 'app/models/cost-factor-group-search-model';
import { ListDetailComponent } from '../list-detail.component';
import { CostFactorTextViewModel } from 'app/models/ViewModels/cost-factor-view-model';

@Component({
    selector: 'add-overlay',
    templateUrl: './add-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumns: string[] = ['id', 'factor', 'description'];
  

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    addTeam = new FormGroup({
        teamName: new FormControl('Team Name One'),
        teamDescription: new FormControl('Team Description One'),
    });

    costFactorGroupID : any = "" ;
    costFactor : CostFactorSearchModel  = new CostFactorSearchModel(); 
    costFactorGroup : any = new CostFactorGroupSearchModel();
    AddedCostFactor:any=[];
    actualCostFactorModels:  any =[];
    cfName : string = "" ;
    cfDescription:string = "" ;
    cfType:string = "";


    constructor(public dialogRef: MatDialogRef<AddOverlayComponent>,private fb: FormBuilder,
        public dialog: MatDialog,private costFactorService:CostFactorService,@Inject(MAT_DIALOG_DATA) public data
        , private costFactorGroupservice: CostFactorGroupService) {
            this.costFactorGroupID = this.data.id;
            this.AddedCostFactor = this.data.AddedCostFactor;
    }
    addCostFactor() {
        debugger;
        this.dialogRef.close(this.costFactor.costFactorTextViewModels);
      }

   
    sortData(sort: Sort){
        debugger;
        this.costFactor.direction = sort.direction;
        this.costFactor.column = sort.active;
        this.FetchBasicData();
    }
    FetchBasicData() {
        this.costFactor.page=1;
        this.costFactor.pageSize=10000;
        this.costFactorService.getCFList(this.costFactor).subscribe(
            result => {
            console.log(result);
            debugger;    
            this.costFactor = result;

            for(var i=0;i< this.costFactor.costFactorTextViewModels.length;i++)
            {
                this.costFactor.costFactorTextViewModels[i].isChecked=false;
                this.costFactor.costFactorTextViewModels[i].isDisabled=false;
                for(var k=0;k< this.AddedCostFactor.costFactorModels.length;k++)
                {
                    if(this.costFactor.costFactorTextViewModels[i].id == this.AddedCostFactor.costFactorModels[k].id)
                    {
                        this.costFactor.costFactorTextViewModels[i].isChecked=true;
                        this.costFactor.costFactorTextViewModels[i].isDisabled=true;
                    }
                    
                }
            }
         
            
            }
        );
    }
    ngOnInit() {
        this.FetchBasicData();  
    }

    OnPaginateChange(event: PageEvent) {
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.costFactor.pageSize = event.pageSize;
        this.costFactor.page = page;
        this.FetchBasicData();
        //  this.dataSource=   this.CreatePaginationData(page,size);

    }

    SetIsChecked(row,event)
    {
        debugger;
        row.isChecked=event.checked;
    }

    searchData() {
        debugger;
           let dataList: CostFactorTextViewModel[] = this.actualCostFactorModels;
   
           if(this.cfName && this.cfName != ""){
   
               dataList = dataList.filter((data: CostFactorTextViewModel) => {
   
                   return data.cfName.indexOf(this.cfName) > -1;
   
               })
   
           }
   
           if(this.cfDescription && this.cfDescription != ""){
   
               dataList = dataList.filter((data: CostFactorTextViewModel) => {
   
                   return data.cfDescription.indexOf(this.cfDescription) > -1;
   
               })
   
           }
          
           this.costFactor.costFactorTextViewModels = dataList;
   
       }

}
