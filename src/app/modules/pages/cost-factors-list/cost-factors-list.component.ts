import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator, PageEvent } from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import { CostFactorGroupSearchModel } from 'app/models/cost-factor-group-search-model';
import { CostFactorGroupService } from 'app/services/cost-factor-group.service';
import { Router } from '@angular/router';
import { AddEditOverlayComponent } from './add-edit-overlay/add-edit-overlay.component';
import { CostFactorGroupViewModel } from 'app/models/ViewModels/cost-factor-group-view-model';
import { CostFactorSearchModel } from 'app/models/cost-factor-search-model';
import { DownloadService } from 'app/services/download.service';

@Component({
    selector     : 'cost-factors-list',
    templateUrl  : './cost-factors-list.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CostFactorsListComponent
{
    displayedColumns: string[] = ['id', 'title', 'name'];
     pageEvent: PageEvent;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;
    dataTypeList : any[] ;
    actualCostFactorModels:  any =[];
    costFactorGroups: any = new CostFactorGroupSearchModel();
    title : string;
   name : string;
   type : string = "";
    costFactor : any  = new CostFactorSearchModel(); 
    /**
     * Constructor
     */
    constructor(private downloadService:DownloadService,public dialog: MatDialog, private costFactorGroupService:CostFactorGroupService,private router: Router)
    {
        this.costFactorGroups.pageSize = 5;
        this.costFactorGroups.page = 1;
    }
    OnPaginateChange(event: PageEvent){
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.costFactorGroups.pageSize = event.pageSize;
        this.costFactorGroups.page = page;
        this.FetchBasicData();
    }

    sortData(sort: Sort){
        debugger;
        this.costFactorGroups.direction = sort.direction;
        this.costFactorGroups.column = sort.active;
        this.FetchBasicData();
    }

    FetchBasicData() {
        debugger;
        this.costFactorGroupService.getCFGList(this.costFactorGroups).subscribe(
            result => {
            console.log(result);
            debugger;
            this.costFactorGroups = result;
            for(var i=0; i<result.costFactorGroupModels.length; i++){
                this.actualCostFactorModels.push(result.costFactorGroupModels[i])
            }
            //this.dataTypeList =  result.dataTypeList;
            }
        );
    }

   

    ngOnInit() {
        this.FetchBasicData();
        this.searchData();
    }

    OpenURL(url,row) {
        //this.router.navigate([url]); 
        debugger;
        this.router.navigateByUrl(url, { state: { Id: row.id } });
    }
    DeleteCFLGrp(model: CostFactorGroupViewModel[]) {
        this.costFactorGroups = model;
        this.costFactorGroupService.DeleteItem([this.costFactorGroups]).subscribe(result => {
            this.costFactorGroups = result;
            this.FetchBasicData();
        });
    }
    openDialog() {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000"}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.FetchBasicData();
        });
    }
    searchData(){
        let dataList: CostFactorGroupViewModel[] = this.actualCostFactorModels;
        if(this.title && this.title != ""){

            dataList = dataList.filter((data: CostFactorGroupViewModel) => {

                return data.title.indexOf(this.title) > -1;

            })

        }

        if(this.name && this.name != ""){

            dataList = dataList.filter((data: CostFactorGroupViewModel) => {

                return data.name.indexOf(this.name) > -1;

            })

        }
        if(this.type != ""){
            dataList = dataList.filter((data: CostFactorGroupViewModel) => {
            return data.isPrivate === (this.type === "private" ? true : false);
            })
            }

        this.costFactorGroups.costFactorGroupModels = dataList;
    }
}
