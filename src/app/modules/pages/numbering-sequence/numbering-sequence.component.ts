import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator,PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AddEditOverlayComponent} from './add-edit-overlay/add-edit-overlay.component';

import { NumberingSequenceService } from 'app/services/numbering-sequence.service';
import { NumberingSequenceSearchModel } from 'app/models/numbering-sequence-search-model';
import { NumberingSequenceViewModel } from 'app/models/ViewModels/numbering-sequence-view-model';
import { FuseAlertService } from '@fuse/components/alert';
import { FuseConfirmationService } from '@fuse/services/confirmation/confirmation.service';




@Component({
    selector     : 'numbering-sequence',
    templateUrl  : './numbering-sequence.component.html',
    encapsulation: ViewEncapsulation.None
})
export class NumberingSequenceComponent
{
    displayedColumns: string[] = ['id', 'startNumber', 'exists', 'suffix', 'sequence', 'startDate', 'endDate'];
    numberingSequenceSearchModel: NumberingSequenceSearchModel = new NumberingSequenceSearchModel();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    pageEvent: PageEvent;
    panelOpenState = false;
    rfxType:string="RFQ";
    Message:string="";
    /**
     * Constructor
     */
     constructor(public dialog: MatDialog, private numberingSequenceService: NumberingSequenceService
        ,private _fuseAlertService: FuseAlertService
        ,private _fuseConfirmationService: FuseConfirmationService)
     {
        this.numberingSequenceSearchModel.pageSize = 10;
        this.numberingSequenceSearchModel.page = 1;
     }
 
     numberingSequenceChange($event){
        console.log($event);
        if($event.index === 0){
            this.fetchNumberingSequenceData("RFQ");
        }
        if($event.index === 1){
            this.fetchNumberingSequenceData("RFI");
        }
        if($event.index === 2){
            this.fetchNumberingSequenceData("RFAQ");
        }
    }

    fetchNumberingSequenceData(type: string) {
        this.rfxType=type;
        this.numberingSequenceSearchModel.rFxType = type;
        this.numberingSequenceService.getNumberingSequenceList(this.numberingSequenceSearchModel).subscribe(result => {
          console.log(result);
          this.numberingSequenceSearchModel = result;
        });
    }

    ngOnInit() {
        this.fetchNumberingSequenceData(this.rfxType);
        this.dismiss("successerror");
    }
   
    openDialog() {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000","RFXType":this.rfxType}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.Message="Added";
            this.show("successerror");
            this.fetchNumberingSequenceData(this.rfxType);
        });  
    }

    editNumberingSequenceData(row :any) {   
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":row.id,"RFXType":this.rfxType}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.Message="Updated";
            this.show("successerror");
            this.fetchNumberingSequenceData(this.rfxType);
        });      
    }

    deleteNumberingSequenceData(model: NumberingSequenceViewModel[]) {
        const dialogRef = this._fuseConfirmationService.open({
            "title": "Remove contact",
            "message": "Are you sure you want to delete this record?",
            "icon": {
              "show": true,
              "name": "heroicons_outline:exclamation",
              "color": "warn"
            },
            "actions": {
              "confirm": {
                "show": true,
                "label": "Remove",
                "color": "warn"
              },
              "cancel": {
                "show": true,
                "label": "Cancel"
              }
            },
            "dismissible": true
          });

        // Subscribe to afterClosed from the dialog reference
        dialogRef.afterClosed().subscribe((result) => {
            if(result=="confirmed")
            {
                this.Message="Deleted";
            this.numberingSequenceService.deleteNumberingSequence([model]).subscribe(result => {
                debugger;
                  this.fetchNumberingSequenceData(this.rfxType);
                    this.show("successerror");
                  
               
      
            });
        }
        });
        
    }
    
    /**
 * Dismiss the alert via the service
 *
 * @param name
 */
dismiss(name: string): void
{
    this._fuseAlertService.dismiss(name);
}

/**
 * Show the alert via the service
 *
 * @param name
 */
show(name: string): void
{
    this._fuseAlertService.show(name);
}
}