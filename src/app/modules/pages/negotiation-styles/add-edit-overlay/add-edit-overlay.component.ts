import { Component, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { NegotiationStyleService } from 'app/services/negotiation-style.service';
import { NegotiationStyleSearchModel } from 'app/models/negotiation-style-search-model';
import { NegotiationStyleViewModel } from 'app/models/ViewModels/negotiation-style-view-model';

export interface RowData {
    id: string;
    group: string;
    objKey: string;
    visibility: string;
}
@Component({
    selector: 'add-edit-overlay',
    templateUrl: './add-edit-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddEditOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumns: string[] = ['id', 'group', 'visibility'];

    headerDataSource: MatTableDataSource<RowData>;
    lineDataSource: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
       isDelete = false;
    negotiationList : FormGroup;
    negotiationStyle: NegotiationStyleViewModel[];
    bidingStyles: any[];
    dataId: any = "";

    headerMap = new Map();
    lineMap = new Map();

    constructor(@Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<AddEditOverlayComponent>,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private negotiationStyleService: NegotiationStyleService
    ) {
        this.negotiationList = this.fb.group({  
            negotiationStyleModels: this.fb.array([])
          });  
          this.dataId = this.data.id;
    }

    get negotiationStyleModels(): FormArray {
        return this.negotiationList.get("negotiationStyleModels") as FormArray
    }

    newNegotiationStyle(): FormGroup {
        return this.fb.group({
          'nsName': [null, Validators.required],
          'title': [null, Validators.required],
          'bidingStyleID': [null, Validators.required],
          'isTwoStageRFx': [null, Validators.required],
          'isRFQ': [null, Validators.required],
          'isRFI': [null, Validators.required],
          'isRFAQ': [null, Validators.required],
          'isAllowOverride': [null, Validators.required],
          'isPrivate': [null, Validators.required],
          'isRFxHeaderAttachment': [null, Validators.required],
          'isRFxHeaderAttributeItem': [null, Validators.required],
          'isRFxHeaderCostFactor': [null, Validators.required],
          'isRFxHeaderDocumentText': [null, Validators.required],
          'isRFxHeaderNotes': [null, Validators.required],
          'isRFxHeaderPaymentSchedule': [null, Validators.required],
          'isRFxTermsConditions': [null, Validators.required],
          'isRFxHeaderDeliverables': [null, Validators.required],
          'isRFxScoringCreteria': [null, Validators.required],
          'isRFxResponseRules': [null, Validators.required],
          'isRFxSurveyForms': [null, Validators.required],
          'isRFxLineAttachment': [null, Validators.required],
          'isRFxLineAttributeItem': [null, Validators.required],
          'isRFxLineCostFactor': [null, Validators.required],
          'isRFxLineDocumentText': [null, Validators.required],
          'isRFxLineNotes': [null, Validators.required],
          'isRFxLinePaymentSchedule': [null, Validators.required],
          'isRFxLineDeliverables': [null, Validators.required]
        })
    }

    initHeaderMap() {
        var rfxHeaderMap = new Map();
        rfxHeaderMap.set("isRFxHeaderAttachment", "Attachments");
        rfxHeaderMap.set("isRFxHeaderAttributeItem", "Attribute Items");
        rfxHeaderMap.set("isRFxHeaderCostFactor", "Cost Factors");
        rfxHeaderMap.set("isRFxHeaderDocumentText", "Document Texts");
        rfxHeaderMap.set("isRFxHeaderNotes", "Notes");
        rfxHeaderMap.set("isRFxHeaderPaymentSchedule", "Payment Schedules");
        rfxHeaderMap.set("isRFxTermsConditions", "Terms Conditions");
        rfxHeaderMap.set("isRFxHeaderDeliverables", "Deliverables");
        rfxHeaderMap.set("isRFxScoringCreteria", "Scoring Creteria");
        rfxHeaderMap.set("isRFxResponseRules", "Response Rules");
        rfxHeaderMap.set("isRFxSurveyForms", "Survey Forms");
        return rfxHeaderMap;
    }

    initLineMap() {
        var rfxLineMap = new Map();
        rfxLineMap.set("isRFxLineAttachment", "Attachments");
        rfxLineMap.set("isRFxLineAttributeItem", "Attribute Items");
        rfxLineMap.set("isRFxLineCostFactor", "Cost Factors");
        rfxLineMap.set("isRFxLineDocumentText", "Document Texts");
        rfxLineMap.set("isRFxLineNotes", "Notes");
        rfxLineMap.set("isRFxLinePaymentSchedule", "Payment Schedules");
        rfxLineMap.set("isRFxLineDeliverables", "Deliverables");
        return rfxLineMap;
    }

    addNegotiationStyle() {
        if (this.negotiationList.get('negotiationStyleModels').invalid) {
          this.negotiationList.get('negotiationStyleModels').markAllAsTouched();
          return;
        }
        this.negotiationStyleModels.push(this.newNegotiationStyle());
        this.headerDataSource = this.createHeaderDataSource(null);
        this.lineDataSource = this.createLineDataSource(null);
    }

    ngOnInit() {

        this.headerMap = this.initHeaderMap();
        this.lineMap = this.initLineMap();
        this.getNegotiationStyle();
    }

    getNegotiationStyle() {
        this.negotiationStyleService.getNegotiationStyleById(this.dataId).subscribe(result => {
            this.isDelete = true;
            this.negotiationStyle = [];
            this.negotiationStyle.push(result.data);
            this.bidingStyles = result.data.biddingStyleList;
            const linesFormArray = this.negotiationList.get("negotiationStyleModels") as FormArray;
            linesFormArray.push(this.newNegotiationStyle());
            this.negotiationList.patchValue({ "negotiationStyleModels": this.negotiationStyle });
            this.headerDataSource = this.createHeaderDataSource(result);
            this.lineDataSource = this.createLineDataSource(result);
        });
    }

    createHeaderDataSource(data: any){
        let dataArray : RowData[] = [];
        for (const [key, value] of this.headerMap.entries()) {
            let object = {id: data ? data.data[key] : false, group: value, objKey: key, visibility: data && data.data[key] ? 'Yes' : 'No'}
            dataArray.push(object);
        }
        return new MatTableDataSource(dataArray);
    }

    createLineDataSource(data: any){
        let dataArray : RowData[] = [];
        for (const [key, value] of this.lineMap.entries()) {
            let object = {id: data ? data.data[key] : false, group: value, objKey: key, visibility: data && data.data[key] ? 'Yes' : 'No'}
            dataArray.push(object);
        }
        return new MatTableDataSource(dataArray);
    }

    onFormSubmit(form: NgForm)  
    {  
        let negotiationStyleSearchModel: NegotiationStyleSearchModel = new NegotiationStyleSearchModel();
        negotiationStyleSearchModel = Object.assign(negotiationStyleSearchModel, form);
        if (this.dataId != "00000000-0000-0000-0000-000000000000") {
            negotiationStyleSearchModel.negotiationStyleModels[0].id = this.negotiationStyle[0].id;
        }

        this.negotiationStyleService.createEditNegotiationStyle(negotiationStyleSearchModel.negotiationStyleModels).subscribe(result => {
            negotiationStyleSearchModel.negotiationStyleModels = result;
            this.dialogRef.close();
        });
    }

    doAction() {
        this.dialogRef.close();
    }
}