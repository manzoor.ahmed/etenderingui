import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator,PageEvent} from '@angular/material/paginator';
import {MatSort,Sort} from '@angular/material/sort';
import {AddEditOverlayComponent} from './add-edit-overlay/add-edit-overlay.component';
import { NegotiationStyleSearchModel } from 'app/models/negotiation-style-search-model';
import { NegotiationStyleService } from 'app/services/negotiation-style.service';
import { NegotiationStyleViewModel } from 'app/models/ViewModels/negotiation-style-view-model';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { FuseAlertService } from '@fuse/components/alert';

@Component({
    selector     : 'attribute-items',
    templateUrl  : './negotiation-styles.component.html',
    encapsulation: ViewEncapsulation.None
})
export class NegotiationStylesComponent
{
    displayedColumns: string[] = ['id', 'name', 'description', 'bidding', 'stage'];
    negotiationStyleSearchModel: NegotiationStyleSearchModel = new NegotiationStyleSearchModel();
    negotiationStyleSearchModelFixed: NegotiationStyleSearchModel = new NegotiationStyleSearchModel();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    pageEvent: PageEvent;
    panelOpenState = false;
    message: string = "";
    dataSource: any; 

    styleName: string = "";
    description: string = "";
    type: string = "";

    /**
     * Constructor
     */
    constructor(public dialog: MatDialog,
                private negotiationStyleService: NegotiationStyleService,
                private _fuseAlertService: FuseAlertService,
                private _fuseConfirmationService: FuseConfirmationService)
    {
        this.negotiationStyleSearchModel.pageSize = 10;
        this.negotiationStyleSearchModel.page = 1;
    }
    OnPaginateChange(event:PageEvent)
    {
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.negotiationStyleSearchModel.pageSize = event.pageSize;
        this.negotiationStyleSearchModel.page = page;
        this.fetchNegotiationStyleData(); 
    }

    sortData(sort: Sort) {
        this.negotiationStyleSearchModel.direction = sort.direction;
        this.negotiationStyleSearchModel.column = sort.active;
        this.fetchNegotiationStyleData();
    }

    searchData() {
        let dataList: NegotiationStyleViewModel[] = this.negotiationStyleSearchModelFixed.negotiationStyleModels;
        
        if(this.styleName && this.styleName != ""){
            dataList = dataList.filter((data: NegotiationStyleViewModel) => {
                return data.nsName === this.styleName;
            })
        }
        if(this.description && this.description != ""){
            dataList = dataList.filter((data: NegotiationStyleViewModel) => {
                return data.title === this.description;
            })
        }
        if(this.type != ""){
            dataList = dataList.filter((data: NegotiationStyleViewModel) => {
                return data.isPrivate === (this.type === "private" ? true : false);
            })
        }
        this.negotiationStyleSearchModel.negotiationStyleModels = dataList;
    }


    fetchNegotiationStyleData() {
        this.negotiationStyleService.getNegotiationStyleList(this.negotiationStyleSearchModel).subscribe(result => {
            this.negotiationStyleSearchModel = result;
            this.negotiationStyleSearchModelFixed.negotiationStyleModels = result.negotiationStyleModels;
            this.searchData();
          });
    }

    ngOnInit() {
        this.fetchNegotiationStyleData();
        this.dismiss("successerror");
    }

    openDialog() {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000"}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.message="Added";
            this.show("successerror");
            this.fetchNegotiationStyleData();
        });
    }

    editNegotiationStyleData(row :any) {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":row.id}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
          this.message="Updated";
          this.show("successerror");
          this.fetchNegotiationStyleData();
        });  
    }

    deleteNegotiationStyleData(model: NegotiationStyleViewModel[]) {
        const dialogRef = this._fuseConfirmationService.open({
            "title": "Remove contact",
            "message": "Are you sure you want to delete this record?",
            "icon": {
              "show": true,
              "name": "heroicons_outline:exclamation",
              "color": "warn"
            },
            "actions": {
              "confirm": {
                "show": true,
                "label": "Remove",
                "color": "warn"
              },
              "cancel": {
                "show": true,
                "label": "Cancel"
              }
            },
            "dismissible": true
          });
          dialogRef.addPanelClass('confirmation-dialog');
        // Subscribe to afterClosed from the dialog reference
        dialogRef.afterClosed().subscribe((result) => {
            if(result=="confirmed")
            {
                this.message="Deleted";
                this.negotiationStyleService.deleteNegotiationStyle([model]).subscribe(result => {
                    this.fetchNegotiationStyleData();
                    this.show("successerror"); 
                });
            }
        });
    }
    
    dismiss(name: string): void
    {
        this._fuseAlertService.dismiss(name);
    }

    show(name: string): void
    {
        this._fuseAlertService.show(name);
    }
}

