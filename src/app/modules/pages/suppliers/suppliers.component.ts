import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {ReviewInitiationOverlayComponent} from './review-initiation-overlay/review-initiation-overlay.component';
import {ReviewScheduleOverlayComponent} from './review-schedule-overlay/review-schedule-overlay.component';
import {
    ApexNonAxisChartSeries,
    ApexPlotOptions,
    ApexChart,
    ApexFill,
    ApexDataLabels,
    ApexStroke,
    ApexStates,
    ApexAxisChartSeries, ApexYAxis, ApexXAxis, ApexTooltip, ApexLegend, ApexGrid, ApexResponsive, ApexTitleSubtitle
} from 'ng-apexcharts';

export interface RowData13 {
    evalName: string;
    supName: string;
    reviewTime: string;
    reqBy: string;
    status: string;
    initDate: string;
    pubDate: string;
}
export type ChartOptions7 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    responsive: ApexResponsive[];
    fill: ApexFill;
    plotOptions: ApexPlotOptions;
    legend: ApexLegend;
    tooltip: ApexTooltip;
    states: ApexStates;
    title: ApexTitleSubtitle;
    labels: any;
};
export type ChartOptions6 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    responsive: ApexResponsive[];
    fill: ApexFill;
    plotOptions: ApexPlotOptions;
    legend: ApexLegend;
    tooltip: ApexTooltip;
    states: ApexStates;
    title: ApexTitleSubtitle;
    labels: any;
};
export type ChartOptions5 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    responsive: ApexResponsive[];
    fill: ApexFill;
    plotOptions: ApexPlotOptions;
    legend: ApexLegend;
    tooltip: ApexTooltip;
    states: ApexStates;
    title: ApexTitleSubtitle;
    labels: any;
};
export type ChartOptions3 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    responsive: ApexResponsive[];
    fill: ApexFill;
    plotOptions: ApexPlotOptions;
    legend: ApexLegend;
    tooltip: ApexTooltip;
    states: ApexStates;
    title: ApexTitleSubtitle;
    labels: any;
};
export type ChartOptions4 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    responsive: ApexResponsive[];
    fill: ApexFill;
    plotOptions: ApexPlotOptions;
    legend: ApexLegend;
    tooltip: ApexTooltip;
    states: ApexStates;
    title: ApexTitleSubtitle;
    labels: any;
};
export type ChartOptions2 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    labels: string[];
    plotOptions: ApexPlotOptions;
    fill: ApexFill;
    dataLabels: ApexDataLabels;
    stroke: ApexStroke;
    states: ApexStates;
};
export type ChartOptions22 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    labels: string[];
    plotOptions: ApexPlotOptions;
    fill: ApexFill;
    dataLabels: ApexDataLabels;
    stroke: ApexStroke;
    states: ApexStates;
};
export type ChartOptions = {
    series: ApexAxisChartSeries;
    chart: ApexChart;
    dataLabels: ApexDataLabels;
    plotOptions: ApexPlotOptions;
    yaxis: ApexYAxis;
    xaxis: ApexXAxis;
    fill: ApexFill;
    tooltip: ApexTooltip;
    stroke: ApexStroke;
    legend: ApexLegend;
    grid: ApexGrid;
    colors: string[];
    states: ApexStates;
};
export type ChartOptions8 = {
    series: ApexAxisChartSeries;
    chart: ApexChart;
    dataLabels: ApexDataLabels;
    plotOptions: ApexPlotOptions;
    yaxis: ApexYAxis;
    xaxis: ApexXAxis;
    fill: ApexFill;
    tooltip: ApexTooltip;
    stroke: ApexStroke;
    legend: ApexLegend;
    grid: ApexGrid;
    colors: string[];
    states: ApexStates;
};
/** Constants used to fill up our data base. */
const EVALNAME: string[] = [
    'IT HW Supplier Evaluation', 'NW HW Supplier Evaluation'
];
const SUPNAME: string[] = [
    'Multiple Suppliers', 'DEF Corporation'
];
const REVIEWTIME: string[] = [
    '2021 Second Half ( Jul - Dec)', '2021 Second Half ( Jul - Dec)'
];
const REQBY: string[] = [
    'Person 1', 'Person 2'
];
const STATUS: string[] = [
    'Published', 'Scheduled'
];
const INITDATE: string[] = [
    '25/12/2021', '20/12/2021'
];
const PUBDATE: string[] = [
    '20/12/2021', '10/12/2022'
];


@Component({
    selector     : 'suppliers',
    templateUrl  : './suppliers.component.html',
    encapsulation: ViewEncapsulation.None,
})

export class SuppliersComponent
{
    displayedColumn13: string[] = ['id', 'evalName', 'supName', 'reviewTime','reqBy', 'status', 'initDate','pubDate'];


    dataSource13: MatTableDataSource<RowData13>;
    public chartOptions8: Partial<ChartOptions8>;
    public chartOptions7: Partial<ChartOptions7>;
    public chartOptions6: Partial<ChartOptions6>;
    public chartOptions5: Partial<ChartOptions5>;
    public chartOptions4: Partial<ChartOptions4>;
    public chartOptions3: Partial<ChartOptions3>;
    public chartOptions2: Partial<ChartOptions2>;
    public chartOptions22: Partial<ChartOptions22>;
    public chartOptions: Partial<ChartOptions>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;

    /**
     * Constructor
     */
    constructor(public dialog: MatDialog)
    {
        const users13 = Array.from({length: 6}, (_, k) => createNewRow13(k + 1));

        this.dataSource13 = new MatTableDataSource(users13);

        this.chartOptions8 = {
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            series: [
                {
                    name: 'No of Deliveries',
                    data: [230, 420, 610, 755]
                },
            ],
            chart: {
                height: 200,
                type: 'bar',
                toolbar: {
                    show: false
                },
                animations:{
                    enabled: false
                }
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#7BCB0A',
                    '#1AAFD0',
                    '#4E36E2',
                    '#0192FF',
                ],
            },

            plotOptions: {
                bar: {
                    columnWidth: '70%',
                    distributed: true,
                    dataLabels: {
                        position: 'top' // top, center, bottom
                    },
                }
            },
            dataLabels: {
                enabled: true,
                formatter: function(val) {
                    return val + '';
                },
                offsetY: -20,
                offsetX: 0,
                style: {
                    fontSize: '12px',
                    colors: ['#464A53']
                }
            },
            legend: {
                show: false
            },
            grid: {
                show: false
            },
            yaxis: {
                title: {
                    text: 'No of Orders'
                }
            },
            xaxis: {
                categories: [
                    ['5<'],
                    ['5-10'],
                    ['11-30'],
                    ['30+'],

                ],
                title:{
                    text: '% of Returns'
                },
                labels: {
                    style: {
                        colors: [
                            '#464A53',
                        ],
                        fontSize: '12px'
                    }
                },
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };
        this.chartOptions7 = {
            series: [820, 80],
            chart: {
                type: 'donut'
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#7BCB0A',
                    '#4E36E2',
                ],
            },
            labels: ['On Time', 'Delayed'],
            plotOptions: {
                pie: {
                    donut: {
                        size: '55%'
                    }
                }
            },
            legend: {
                show: false
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            ],
            title: {
                style: {
                    fontSize:  '14px',
                    fontWeight:  'bold',
                    fontFamily:  'Roboto',
                    color:  '#616161'
                },
                align: 'center'
            },
            tooltip: {
                fillSeriesColor: false,
                shared: false,
                marker: {
                    show: false,
                },
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };
        this.chartOptions6 = {
            series: [820, 80],
            chart: {
                type: 'donut'
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#1AAFD0',
                    '#4E36E2',
                ],
            },
            labels: ['On Time', 'Delayed'],
            plotOptions: {
                pie: {
                    donut: {
                        size: '55%'
                    }
                }
            },
            legend: {
                show: false
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            ],
            title: {
                style: {
                    fontSize:  '14px',
                    fontWeight:  'bold',
                    fontFamily:  'Roboto',
                    color:  '#616161'
                },
                align: 'center'
            },
            tooltip: {
                fillSeriesColor: false,
                shared: false,
                marker: {
                    show: false,
                },
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };
        this.chartOptions5 = {
            series: [820, 80],
            chart: {
                type: 'donut'
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#7BCB0A',
                    '#4E36E2',
                ],
            },
            labels: ['On Time', 'Delayed'],
            plotOptions: {
                pie: {
                    donut: {
                        size: '55%'
                    }
                }
            },
            legend: {
                show: false
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            ],
            title: {
                style: {
                    fontSize:  '14px',
                    fontWeight:  'bold',
                    fontFamily:  'Roboto',
                    color:  '#616161'
                },
                align: 'center'
            },
            tooltip: {
                fillSeriesColor: false,
                shared: false,
                marker: {
                    show: false,
                },
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };
        this.chartOptions4 = {
            series: [820, 80],
            chart: {
                type: 'donut'
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#7BCB0A',
                    '#4E36E2',
                ],
            },
            labels: ['On Time', 'Delayed'],
            plotOptions: {
                pie: {
                    donut: {
                        size: '55%'
                    }
                }
            },
            legend: {
                show: false
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            ],
            title: {
                style: {
                    fontSize:  '14px',
                    fontWeight:  'bold',
                    fontFamily:  'Roboto',
                    color:  '#616161'
                },
                align: 'center',
            },
            tooltip: {
                fillSeriesColor: false,
                shared: false,
                marker: {
                    show: false,
                },
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };
        this.chartOptions3 = {
            series: [820, 80],
            chart: {
                type: 'donut'
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#7BCB0A',
                    '#4E36E2',
                ],
            },
            labels: ['On Time', 'Delayed'],
            plotOptions: {
                pie: {
                    donut: {
                        size: '55%'
                    }
                }
            },
            legend: {
                show: false
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            ],
            title: {
                style: {
                    fontSize:  '14px',
                    fontWeight:  'bold',
                    fontFamily:  'Roboto',
                    color:  '#616161'
                },
                align: 'center'
            },
            tooltip: {
                fillSeriesColor: false,
                shared: false,
                marker: {
                    show: false,
                },
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };
        this.chartOptions2 = {
            series: [60],
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            chart: {
                height: 190,
                type: 'radialBar',
            },
            fill: {
                colors: ['#8BC740']
            },
            plotOptions: {
                radialBar: {
                    hollow: {
                        size: '65%',
                        margin: 10,
                    },
                    dataLabels:{
                        name: {
                            fontSize: '14px',
                            color: '#6c6c6c',
                            fontWeight: 600,
                            fontFamily: 'Roboto',
                            offsetY: -10,
                        },
                        value: {
                            fontSize: '26px',
                            color: '#8BC740',
                            offsetY: 5,
                        },
                    }
                }
            },
            stroke: {
                lineCap: 'round'
            },
            labels: ['Issued POs'],

        };
        this.chartOptions22 = {
            series: [60],
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            chart: {
                height: 190,
                type: 'radialBar',
            },
            fill: {
                colors: ['#8BC740']
            },
            plotOptions: {
                radialBar: {
                    hollow: {
                        size: '65%',
                        margin: 10,
                    },
                    dataLabels:{
                        name: {
                            fontSize: '14px',
                            color: '#6c6c6c',
                            fontWeight: 600,
                            fontFamily: 'Roboto',
                            offsetY: -10,
                        },
                        value: {
                            fontSize: '26px',
                            color: '#8BC740',
                            offsetY: 5,
                        },
                    }
                }
            },
            stroke: {
                lineCap: 'round'
            },
            labels: ['Issued Receipts'],

        };
        this.chartOptions = {
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            series: [
                {
                    name: 'No of Deliveries',
                    data: [230, 420, 610, 755]
                },
            ],
            chart: {
                height: 200,
                type: 'bar',
                toolbar: {
                    show: false
                },
                animations:{
                    enabled: false
                }
            },
            fill:{
                type: 'solid',
                opacity: 1,
                colors: [
                    '#7BCB0A',
                    '#1AAFD0',
                    '#4E36E2',
                    '#0192FF',
                ],
            },

            plotOptions: {
                bar: {
                    columnWidth: '70%',
                    distributed: true,
                    dataLabels: {
                        position: 'top' // top, center, bottom
                    },
                }
            },
            dataLabels: {
                enabled: true,
                formatter: function(val) {
                    return val + '';
                },
                offsetY: -20,
                offsetX: 0,
                style: {
                    fontSize: '12px',
                    colors: ['#464A53']
                }
            },
            legend: {
                show: false
            },
            grid: {
                show: false
            },
            yaxis: {
                title: {
                    text: 'No of Deliveries'
                }
            },
            xaxis: {
                categories: [
                    ['5<'],
                    ['5-10'],
                    ['11-30'],
                    ['30+'],

                ],
                title:{
                    text: 'Days Delayed'
                },
                labels: {
                    style: {
                        colors: [
                            '#464A53',
                        ],
                        fontSize: '12px'
                    }
                },
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return val+'';
                    }
                }
            }
        };

    }
    ngAfterViewInit() {
        this.dataSource13.paginator = this.paginator;
        this.dataSource13.sort = this.sort;

    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource13.filter = filterValue.trim().toLowerCase();

    }
    performReview() {
        const dialogRef = this.dialog.open(ReviewInitiationOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
    scheduleReview() {
        const dialogRef = this.dialog.open(ReviewScheduleOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }

    isShow = true;
    isShow2 = true;

    toggleDisplay() {
        this.isShow = !this.isShow;
    }
    toggleDisplay2() {
        this.isShow2 = !this.isShow2;
    }

}

function createNewRow13(id: number): RowData13 {

    return {
        evalName: EVALNAME[Math.round(Math.random() * (EVALNAME.length - 1))],
        supName: SUPNAME[Math.round(Math.random() * (SUPNAME.length - 1))],
        reviewTime: REVIEWTIME[Math.round(Math.random() * (REVIEWTIME.length - 1))],
        reqBy: REQBY[Math.round(Math.random() * (REQBY.length - 1))],
        status: STATUS[Math.round(Math.random() * (STATUS.length - 1))],
        initDate: INITDATE[Math.round(Math.random() * (INITDATE.length - 1))],
        pubDate: PUBDATE[Math.round(Math.random() * (PUBDATE.length - 1))],
    };
}
