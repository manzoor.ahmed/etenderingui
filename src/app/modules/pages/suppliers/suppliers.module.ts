import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {CommonModule} from '@angular/common';
import {DatatableModule} from '../../common/datatable/datatable.module';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import {TagsOverlayModule} from 'app/modules/common/tags-overlay/tags-overlay.module';
import {DrawerMiniModule} from '../../common/drawer-mini/drawer-mini.module';
import {SuppliersComponent} from './suppliers.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatChipsModule} from '@angular/material/chips';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {MatTabsModule} from '@angular/material/tabs';
import {MatRadioModule} from '@angular/material/radio';
import {MatDividerModule} from '@angular/material/divider';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ReviewInitiationOverlayComponent} from './review-initiation-overlay/review-initiation-overlay.component';
import {ReviewInitiationSummaryOverlayComponent} from './review-initiation-summary-overlay/review-initiation-summary-overlay.component';
import {ReviewScheduleSummaryOverlayComponent} from './review-schedule-summary-overlay/review-schedule-summary-overlay.component';
import {ReviewScheduleOverlayComponent} from './review-schedule-overlay/review-schedule-overlay.component';
import {SearchModule} from "../../../layout/common/search/search.module";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {NgApexchartsModule} from "ng-apexcharts";

const suppliersRoutes: Route[] = [
    {
        path     : '',
        component: SuppliersComponent
    }
];

@NgModule({
    declarations: [
        SuppliersComponent,
        ReviewInitiationOverlayComponent,
        ReviewInitiationSummaryOverlayComponent,
        ReviewScheduleSummaryOverlayComponent,
        ReviewScheduleOverlayComponent
    ],
    imports: [
        RouterModule.forChild(suppliersRoutes),
        FormsModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        MatExpansionModule,
        DatatableModule,
        MatIconModule,
        CommonModule,
        MatInputModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatSelectModule,
        MatButtonModule,
        MatMenuModule,
        MatCheckboxModule,
        MatCardModule,
        TagsOverlayModule,
        DrawerMiniModule,
        MatDialogModule,
        MatChipsModule,
        MatDatepickerModule,
        MatTabsModule,
        MatRadioModule,
        MatDividerModule,
        MatStepperModule,
        MatSlideToggleModule,
        MatTooltipModule,
        SearchModule,
        MatAutocompleteModule,
        NgApexchartsModule,
    ]
})
export class SuppliersModule
{
}
