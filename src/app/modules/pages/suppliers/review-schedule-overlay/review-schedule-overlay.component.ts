import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {ReviewScheduleSummaryOverlayComponent} from '../review-schedule-summary-overlay/review-schedule-summary-overlay.component';


export interface RowData2 {
    id: string;
    name: string;
}
export interface RowData3 {
    sid: string;
    sname: string;
}

/** Constants used to fill up our data base. */
const ID: string[] = [
    'EID 4467', 'EID 4462'
];
const NAME: string[] = [
    'M. Ali', 'Ali Asger'
];

/** Constants used to fill up our data base. */
const SID: string[] = [
    'SID 4467', 'SID 4462'
];
const SNAME: string[] = [
    'UOI Corporation', 'ABC Group'
];

@Component({
    selector: 'review-schedule',
    templateUrl: './review-schedule-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ReviewScheduleOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumn2: string[] = ['index', 'id', 'name'];
    displayedColumn3: string[] = ['index', 'sid', 'sname'];
    dataSource2: MatTableDataSource<RowData2>;
    dataSource3: MatTableDataSource<RowData3>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;


    constructor(public dialogRef: MatDialogRef<ReviewScheduleOverlayComponent>,
                public dialog: MatDialog
    ) {
        const users2 = Array.from({length: 3}, (_, k) => createNewRow2(k + 1));
        const users3 = Array.from({length: 3}, (_, k) => createNewRow3(k + 1));
        // Assign the data to the data source for the table to render
        this.dataSource2 = new MatTableDataSource(users2);
        this.dataSource3 = new MatTableDataSource(users3);
    }

    doAction() {
        this.dialogRef.close();
        window.location.reload();

    }
    viewSummary() {
        const dialogRef = this.dialog.open(ReviewScheduleSummaryOverlayComponent);
        dialogRef.addPanelClass('inline-sm-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
}

/** Builds and returns a new createNewRow. */
function createNewRow2(id: number): RowData2 {

    return {
        id: ID[Math.round(Math.random() * (ID.length - 1))],
        name: NAME[Math.round(Math.random() * (NAME.length - 1))],
    };
}

/** Builds and returns a new createNewRow. */
function createNewRow3(id: number): RowData3 {

    return {
        sid: SID[Math.round(Math.random() * (SID.length - 1))],
        sname: SNAME[Math.round(Math.random() * (SNAME.length - 1))],
    };
}
