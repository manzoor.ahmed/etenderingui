import { Component, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators, FormsModule, NgForm, FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { CostFactorSearchModel } from 'app/models/cost-factor-search-model';
import { CostFactorService } from '../../../../services/cost-factor.service';
import { CostFactorTextViewModel } from '../../../../models/ViewModels/cost-factor-view-model';

@Component({
  selector: 'att-add-edit-overlay',
  templateUrl: './add-edit-overlay.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AddEditOverlayComponent {
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

  templateData: any = [];
  useremail: string = '';

  selectedId: any = [];
  errormessage = 'Something went wrong, please try again.';
  successmessage = 'Successfully added the template';
  issuccess = false;
  iserror = false;
  frmCostFactorList: FormGroup;
  isDelete = false;
  dataId: any = "";
  costFactors: CostFactorTextViewModel[];//Your Model 
  costFactorTypes: any[];//Your Model 

  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialogRef: MatDialogRef<AddEditOverlayComponent>,
    public dialog: MatDialog, private fb: FormBuilder
    , private costFactorService: CostFactorService
  ) {
    this.frmCostFactorList = this.fb.group({
      costFactorTextViewModels: this.fb.array([])
    });
    this.dataId = data.id;
  }
  get costFactorTextViewModels(): FormArray {
    return this.frmCostFactorList.get("costFactorTextViewModels") as FormArray
  }
  newCostFactor(): FormGroup {
    debugger;

    return this.fb.group({
      'cfName': [null, Validators.required],
      'cfDescription': [null, Validators.required],
      'cfTypeId': [null, Validators.required]
    })
  }
  addCostFactor() {
    if (this.frmCostFactorList.get('costFactorTextViewModels').invalid) {
      this.frmCostFactorList.get('costFactorTextViewModels').markAllAsTouched();
      return;
    }
    this.costFactorTextViewModels.push(this.newCostFactor());

  }

  ngOnInit() {
    // if (this.dataId == "00000000-0000-0000-0000-000000000000") {
    //   this.addCostFactor();
    // }


    this.costFactorService.getCFById(this.dataId).subscribe(result => {//your service call here
      console.log(result);
      debugger;
      this.isDelete = true;
      this.costFactors = [];
      this.costFactors.push(result.data);
      // this.costFactorTypes.push({"id":"","text":"Select"});
      this.costFactorTypes=result.data.cfTypes;
      const linesFormArray = this.frmCostFactorList.get("costFactorTextViewModels") as FormArray;
      linesFormArray.push(this.newCostFactor());
      this.frmCostFactorList.patchValue({ "costFactorTextViewModels": this.costFactors });
    });


  }

  onFormSubmit(form: NgForm) {
    debugger;
    console.log(form);
    let costFactorSearchModel: CostFactorSearchModel = new CostFactorSearchModel();
    costFactorSearchModel = Object.assign(costFactorSearchModel, form);
    if (this.dataId != "00000000-0000-0000-0000-000000000000") {
      costFactorSearchModel.costFactorTextViewModels[0].id = this.costFactors[0].id;
    }
    this.costFactorService.SaveCF(costFactorSearchModel.costFactorTextViewModels).subscribe(result => {
      console.log(result);
      debugger;

      costFactorSearchModel.costFactorTextViewModels = result;
      this.dialogRef.close();
    });
  }
  doAction() {
    this.dialogRef.close();
    // window.location.reload() ;

  }

}
