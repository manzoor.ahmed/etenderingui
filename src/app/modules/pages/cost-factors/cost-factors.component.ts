import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AddEditOverlayComponent } from './add-edit-overlay/add-edit-overlay.component';
import { CostFactorSearchModel } from '../../../models/cost-factor-search-model';
import { CostFactorService } from '../../../services/cost-factor.service';
import { Sort } from '@angular/material/sort';
import { FuseConfirmationService } from '@fuse/services/confirmation/confirmation.service';
import { FuseAlertService } from '@fuse/components/alert';
import { CostFactorTextViewModel } from 'app/models/ViewModels/cost-factor-view-model';

@Component({
    selector: 'cost-factors',
    templateUrl: './cost-factors.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CostFactorsComponent {
    displayedColumns: string[] = ['id', 'factor', 'description', 'costType'];
Message:any="";
    pageEvent: PageEvent;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;
    /**
     * Constructor
     */
    costFactorSearchModel: CostFactorSearchModel = new CostFactorSearchModel();
    constructor(public dialog: MatDialog, private costFactorService: CostFactorService, 
        private _fuseConfirmationService: FuseConfirmationService,
        private _fuseAlertService: FuseAlertService) {
        this.costFactorSearchModel.pageSize = 10;
        this.costFactorSearchModel.page = 1;
    }
    openDialog() {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000"}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.Message="Added";
            this.FetchBasicData();
        });
    }

    EditCF(row: any) {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":row.id}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.Message="Updated";
            this.FetchBasicData();
        });

    }

    OnPaginateChange(event: PageEvent) {
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.costFactorSearchModel.pageSize = event.pageSize;
        this.costFactorSearchModel.page = page;
        this.FetchBasicData();
        //  this.dataSource=   this.CreatePaginationData(page,size);

    }
    sortData(sort: Sort) {
        debugger;
        this.costFactorSearchModel.direction = sort.direction;
        this.costFactorSearchModel.column = sort.active;
        this.FetchBasicData();
    }
    FetchBasicData() {
        debugger;
        
        this.costFactorService.getCFList(this.costFactorSearchModel).subscribe(result => {
            console.log(result);
            debugger;
            this.costFactorSearchModel = result;
        });
    }

    ngOnInit() {
        this.FetchBasicData();
this.dismiss("successerror");
    }

    DeleteCF(model: CostFactorTextViewModel[]) {

     

      
        const dialogRef = this._fuseConfirmationService.open({
            "title": "Remove contact",
            "message": "Are you sure you want to delete this record?",
            "icon": {
              "show": true,
              "name": "heroicons_outline:exclamation",
              "color": "warn"
            },
            "actions": {
              "confirm": {
                "show": true,
                "label": "Remove",
                "color": "warn"
              },
              "cancel": {
                "show": true,
                "label": "Cancel"
              }
            },
            "dismissible": true
          });
        dialogRef.addPanelClass('confirmation-dialog');

        // Subscribe to afterClosed from the dialog reference
        dialogRef.afterClosed().subscribe((result) => {
            if(result=="confirmed")
            {
               
              this.costFactorService.DeleteItem([model]).subscribe(result => {

          
                this.Message="Deleted";
                this.FetchBasicData();
                  this.show("successerror");    
    
            });
        }
        });  
    }
    
        /**
 * Dismiss the alert via the service
 *
 * @param name
 */
dismiss(name: string): void
{
    this._fuseAlertService.dismiss(name);
}

/**
 * Show the alert via the service
 *
 * @param name
 */
show(name: string): void
{
    this._fuseAlertService.show(name);
}
}

