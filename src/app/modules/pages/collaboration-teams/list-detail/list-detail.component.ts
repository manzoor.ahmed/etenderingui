import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AddOverlayComponent} from './add-overlay/add-overlay.component';

export interface RowData {
    id: string;
    name: string;
    position: string;
}

/** Constants used to fill up our data base. */
const NAME: string[] = [
    'Abdul Bilall', 'Rameez Raja'
];
const POSITION: string[] = [
    'SME - Engineering', 'Category Manager'
];

@Component({
    selector     : 'col-list-detail',
    templateUrl  : './list-detail.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ListDetailComponent
{
    displayedColumns: string[] = ['id', 'name', 'position'];
    dataSource: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;
    /**
     * Constructor
     */
    constructor(public dialog: MatDialog)
    {
        const users = Array.from({length: 100}, (_, k) => createNewRow(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    openDialog() {
        const dialogRef = this.dialog.open(AddOverlayComponent);
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
        });
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        id: id.toString(),
        name: NAME[Math.round(Math.random() * (NAME.length - 1))],
        position: POSITION[Math.round(Math.random() * (POSITION.length - 1))],
    };
}

