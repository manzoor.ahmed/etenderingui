import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddEditOverlayComponent } from './add-edit-overlay/add-edit-overlay.component';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { map,tap } from 'lodash';
import {Sort} from '@angular/material/sort';
import { CollaborationTeamSearchModel } from '../../../models/collaboration-team-search-model';
import { CollaborationTeamService } from '../../../services/collaboration-team.service';

@Component({
    selector     : 'collaboration-teams',
    templateUrl  : './collaboration-teams.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CollaborationTeamsComponent
{
    displayedColumns: string[] = ['teamname', 'teamdescription'];
    dataSource: CollaborationTeamSearchModel=null;
    
pageEvent:PageEvent;
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;
    resultsLength:any;
    /**
     * Constructor
     */
     collaborationTeamSearchModel: CollaborationTeamSearchModel = new CollaborationTeamSearchModel();
    constructor(public dialog: MatDialog,private collaborationTeamService: CollaborationTeamService)
    {
        this.collaborationTeamSearchModel.pageSize=10;
        this.collaborationTeamSearchModel.page=1;
    }
    
    ngAfterViewInit() {
       
       
    }
    EditTeam(row :any) {
     
      const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":row.id}});
      dialogRef.addPanelClass('inline-md-overlay');
      dialogRef.afterClosed().subscribe(result => {
        this.FetchBasicData();
      });
     
  }
    
    OnPaginateChange(event:PageEvent)
    {
        let page=event.pageIndex;
        let size=event.pageSize;
        page=page+1;
        this.collaborationTeamSearchModel.pageSize=event.pageSize;
        this.collaborationTeamSearchModel.page=page;
        this.FetchBasicData();
    //  this.dataSource=   this.CreatePaginationData(page,size);
           
    }
    sortData(sort: Sort) {
        debugger;
        this.collaborationTeamSearchModel.direction=sort.direction;
        this.collaborationTeamSearchModel.column=sort.active;
        this.FetchBasicData();
    }
    FetchBasicData() {
      debugger;
      
        this.collaborationTeamService.getCTList(this.collaborationTeamSearchModel).subscribe(result => {
          console.log(result);
          debugger;
          this.collaborationTeamSearchModel = result;
        });
   
      }
    ngOnInit() {
       this.FetchBasicData();
     
      }
   
    openDialog() {
      const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000"}});
      dialogRef.addPanelClass('inline-md-overlay');
      dialogRef.afterClosed().subscribe(result => {
        this.FetchBasicData();
      });
  }
}
