import { Component, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators ,FormsModule,NgForm,FormArray } from '@angular/forms';  
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { CollaborationTeamSearchModel } from '../../../../models/collaboration-team-search-model';
import { CollaborationTeamService } from '../../../../services/collaboration-team.service';
import { CollaborationTeamTextViewModel } from '../../../../models/ViewModels/collaboration-team-view-model';

@Component({
  selector: 'add-edit-overlay',
  templateUrl: '../add-edit-overlay/add-edit-overlay.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AddEditOverlayComponent {
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    teamList :  FormGroup;
    isDelete = false;
    dataId: any = "";
    collaborationTeam: CollaborationTeamTextViewModel[];

  constructor(@Inject(MAT_DIALOG_DATA) public data,public dialogRef: MatDialogRef<AddEditOverlayComponent>,
    public dialog: MatDialog,private fb: FormBuilder,private collaborationTeamService: CollaborationTeamService) {
      this.teamList = this.fb.group({  
        collaborationTeamTextViewModels: this.fb.array([])
      });  
      this.dataId=this.data.id;
  }
  get collaborationTeamTextViewModels(): FormArray {
    return this.teamList.get("collaborationTeamTextViewModels") as FormArray
  }
  newTeam(): FormGroup {
    debugger;

    return this.fb.group({
      'teamName': [null, Validators.required],
      'teamDescription': [null, Validators.required]
    })
  }
  addTeam() {
    if (this.teamList.get('collaborationTeamTextViewModels').invalid) {
      this.teamList.get('collaborationTeamTextViewModels').markAllAsTouched();
      return;
    }
    this.collaborationTeamTextViewModels.push(this.newTeam());

  }
  ngOnInit() {
    if (this.dataId == "00000000-0000-0000-0000-000000000000") {
      this.addTeam();
    }
    else {

      this.collaborationTeamService.getCTById(this.dataId).subscribe(result => {
        console.log(result);
        debugger;
        this.isDelete = true;
        this.collaborationTeam = [];
        this.collaborationTeam.push(result.data);
        const linesFormArray = this.teamList.get("collaborationTeamTextViewModels") as FormArray;
        linesFormArray.push(this.newTeam());
        this.teamList.patchValue({ "collaborationTeamTextViewModels": this.collaborationTeam });
      });

    }
   }
  onFormSubmit(form:NgForm)  
  {  
    debugger;
    console.log(form);
    let collaborationTeamSearchModel: CollaborationTeamSearchModel = new CollaborationTeamSearchModel();
    collaborationTeamSearchModel = Object.assign(collaborationTeamSearchModel, form);
    if (this.dataId != "00000000-0000-0000-0000-000000000000") {
      collaborationTeamSearchModel.collaborationTeamTextViewModels[0].id = this.collaborationTeam[0].id;
    }
    this.collaborationTeamService.createEditTeam(collaborationTeamSearchModel.collaborationTeamTextViewModels).subscribe(result => {
      console.log(result);
      debugger;

      collaborationTeamSearchModel.collaborationTeamTextViewModels = result;
      this.dialogRef.close();
    });
  }  

    addTemplate(item, event) {
    }

    doAction() {
        this.dialogRef.close();
        // window.location.reload() ;

    }
    DeleteCT() {
      this.collaborationTeam
      this.collaborationTeamService.DeleteItem(this.collaborationTeam).subscribe(result => {
        console.log(result);
        debugger;
  
        this.dialogRef.close();
      });
    }


}
