import { Component, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators ,FormsModule,NgForm,FormArray } from '@angular/forms';  
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { AttributeGroupSearchModel } from '../../../../models/attribute-group-search-model';
 import { AttributeGroupService } from '../../../../services/attribute-group.service';
import { AttributeGroupViewModel } from '../../../../models/ViewModels/attribute-group-view-model';

@Component({
    selector: 'att-list-add-edit-overlay',
    templateUrl: './add-edit-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddEditOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    frmAttributeGroup :  FormGroup;
    isDelete = false;
    dataId: any = "";
    attributeGroupViewModel: AttributeGroupViewModel[];


    constructor(@Inject(MAT_DIALOG_DATA) public data,public dialogRef: MatDialogRef<AddEditOverlayComponent>,
    public dialog: MatDialog,private fb: FormBuilder
    ,private attributeGroupService: AttributeGroupService
    ) {
        this.frmAttributeGroup = this.fb.group({
            'title': [null, Validators.required],
            'name': [null, Validators.required],
            'groupName': [null, Validators.required],
            'isPrivate': [null, Validators.required]
          })
          this.frmAttributeGroup = this.fb.group({  
            attributeGroupViewModels: this.fb.array([])
          });  
        this.dataId=data.id;
    }
    get attributeGroupViewModels(): FormArray {
      return this.frmAttributeGroup.get("attributeGroupViewModels") as FormArray
    }
    newAttributeList(): FormGroup {
      debugger;
  
      return this.fb.group({
        'title': [null, Validators.required],
            'name': [null, Validators.required],
            'groupName': [null, Validators.required],
            'isPrivate': [null, Validators.required]
      })
    }
    addAttributeList() {
      debugger;
      if (this.frmAttributeGroup.get('attributeGroupViewModels').invalid) {
        this.frmAttributeGroup.get('attributeGroupViewModels').markAllAsTouched();
        return;
      }
      this.attributeGroupViewModels.push(this.newAttributeList());
  
    }
    ngOnInit() {
      if (this.dataId == "00000000-0000-0000-0000-000000000000") {
        this.addAttributeList();
      }
      else {
  
        this.attributeGroupService.getAttributeGroupById(this.dataId).subscribe(result => {
          console.log(result);
          debugger;
          this.isDelete = true;
          this.attributeGroupViewModel = [];
          this.attributeGroupViewModel.push(result.data);
          const linesFormArray = this.frmAttributeGroup.get("attributeGroupViewModels") as FormArray;
          linesFormArray.push(this.newAttributeList());
          this.frmAttributeGroup.patchValue({ "attributeGroupViewModels": this.attributeGroupViewModel });
        });
  
      }
       }
      onFormSubmit(form:NgForm)  
      {  
        debugger;
        console.log(form);
        let attributeGroupSearchModel: AttributeGroupSearchModel = new AttributeGroupSearchModel();
        attributeGroupSearchModel = Object.assign(attributeGroupSearchModel, form);
        if (this.dataId != "00000000-0000-0000-0000-000000000000") {
          attributeGroupSearchModel.attributeGroupViewModels[0].id = this.attributeGroupViewModel[0].id;
        }
        this.attributeGroupService.SaveAttributeGroup(attributeGroupSearchModel.attributeGroupViewModels).subscribe(result => {
          console.log(result);
          debugger;
    
          attributeGroupSearchModel.attributeGroupViewModels = result;
          this.dialogRef.close();
        });
      }  
    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }



}
