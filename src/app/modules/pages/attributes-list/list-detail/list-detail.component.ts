import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {AddOverlayComponent} from './add-overlay/add-overlay.component';
import { AttributeGroupSearchModel } from 'app/models/attribute-group-search-model';
import { AttributeGroupService } from 'app/services/attribute-group.service';
import { Router } from '@angular/router';
import { FuseAlertService } from '@fuse/components/alert';
import { FuseConfirmationService } from '@fuse/services/confirmation/confirmation.service';
import { AttributeViewModel } from 'app/models/ViewModels/attribute-view-model';

@Component({
    selector     : 'rfx',
    templateUrl  : './list-detail.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ListDetailComponent
{
    displayedColumns: string[] = ['id','attItem', 'category', 'description', 'dataType'];

    attributeGroupID : any;
    attributeGroup: any = new AttributeGroupSearchModel();
  
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    pageEvent:PageEvent;
    panelOpenState = false;
    message:string="";
   
    attributeItems: string = "";
    categories: string = "";
    descriptions: string = "";
    dataTypeList: any=[];
    dTypes: any;
    dataTypeName:string;
    actualAttributeModels:any=[];
    /**
     * Constructor
     */
     constructor(public dialog: MatDialog,private attributeGroupService: AttributeGroupService,private router: Router
        ,private _fuseAlertService: FuseAlertService
        ,private _fuseConfirmationService: FuseConfirmationService)
    {
        this.attributeGroupID = this.router.getCurrentNavigation().extras.state.Id;
        this.attributeGroup.pageSize=10;
        this.attributeGroup.page=1;
        
  
    }
    OnPaginateChange(event:PageEvent)
    {
        let page=event.pageIndex;
        let size=event.pageSize;
        page=page+1;
        this.attributeGroup.pageSize=event.pageSize;
        this.attributeGroup.page=page;
        this.FetchBasicData();
    //  this.dataSource=   this.CreatePaginationData(page,size);
           
    }

    sortData(sort: Sort) {
        debugger;
        this.attributeGroup.direction=sort.direction;
        this.attributeGroup.column=sort.active;
        this.FetchBasicData();
    }

    searchData() {

        debugger;
        let dataList: AttributeViewModel[] = this.actualAttributeModels;
        
        if(this.attributeItems && this.attributeItems != ""){
            dataList = dataList.filter((data: AttributeViewModel) => {
                return data.attributeName.indexOf(this.attributeItems) > -1;
            })
        }
        if(this.categories && this.categories != ""){
            dataList = dataList.filter((data: AttributeViewModel) => {
                return data.categoryName.indexOf(this.categories) > -1;
            })
        }
        if(this.descriptions && this.descriptions != ""){
            dataList = dataList.filter((data: AttributeViewModel) => {
                return data.description.indexOf(this.descriptions) > -1;
            })
        }
        if(this.dataTypeName && this.dataTypeName != ""){
            dataList = dataList.filter((data: AttributeViewModel) => {
                return data.dataTypeName.indexOf(this.dataTypeName) > -1;

            })
        }

        this.attributeGroup.attributeModels = dataList;
    }

    FetchBasicData(){

        this.attributeGroupService.getAttributeGroupById(this.attributeGroupID).subscribe(result => {
          console.log(result);
          debugger;
          this.attributeGroup = result.data;
          for(var kk=0;kk<result.data.attributeModels.length;kk++)
          {
              this.actualAttributeModels.push(result.data.attributeModels[kk]);
          }
        
        this.dataTypeList = result.data.dataTypeList;
        
        });
   
    }

    ngOnInit() {
        this.dismiss("successerror");
        this.FetchBasicData();
       
      
    }

    saveChanges(){

        for (var i = 0; i < this.actualAttributeModels.length; i++) {

            this.actualAttributeModels[i].attributeGroupId= this.attributeGroupID;

        }

        this.attributeGroupService.SaveAttributeGroupMapping(this.actualAttributeModels).subscribe(

            result => {

                console.log(result);
                this.message="Saved";
                this.show("successerror");

                }
        );
     
    }

    openDialog() {

        const  dialogRef = this.dialog.open(AddOverlayComponent,{data:{AddedAttribute:this.attributeGroup}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            let data:any=[];
            for (var i = 0; i < this.attributeGroup.attributeModels.length; i++) {
                data.push(this.attributeGroup.attributeModels[i]);
            }
            for(var i=0; i<result.length; i++)
            {
                if(result[i].isChecked==true && result[i].isDisabled==false)
                {
                    data.push(result[i]);
                }
            }
            this.attributeGroup.attributeModels=[];
            this.attributeGroup.attributeModels=data;
            this.actualAttributeModels=[];
            for(var kk=0;kk<this.attributeGroup.attributeModels.length;kk++)
            {
                this.actualAttributeModels.push(this.attributeGroup.attributeModels[kk]);
            }
            this.message="Added";
            this.show("successerror");

        });

    }

    Delete(row)
    {
        debugger;
        const dialogRef = this._fuseConfirmationService.open({
            "title": "Remove contact",
            "message": "Are you sure you want to delete this record?",
            "icon": {
              "show": true,
              "name": "heroicons_outline:exclamation",
              "color": "warn"
            },
            "actions": {
              "confirm": {
                "show": true,
                "label": "Remove",
                "color": "warn"
              },
              "cancel": {
                "show": true,
                "label": "Cancel"
              }
            },
            "dismissible": true
          });
          dialogRef.addPanelClass('confirmation-dialog');

          // Subscribe to afterClosed from the dialog reference
  
          dialogRef.afterClosed().subscribe((result) => {  
              if (result == "confirmed") {
                for(var kk=0;kk<this.actualAttributeModels.length;kk++)
                {
                   if(row.id == this.actualAttributeModels[kk].id){
                       this.actualAttributeModels[kk].isChecked=false;
                   }      
                }      
                var attributeIndex = -1;      
                for(var kk=0;kk<this.attributeGroup.attributeModels.length;kk++)
                {
                   if(row.id == this.attributeGroup.attributeModels[kk].id){
                      attributeIndex = kk;
                   }      
                }
                this.attributeGroup.attributeModels= this.attributeGroup.attributeModels.filter(obj => obj.id !== row.id);      
             row.IsChecked=false;
  
  
              }
  
          });
          

    }

          /**
 * Dismiss the alert via the service
 *
 * @param name
 */
dismiss(name: string): void
{
    this._fuseAlertService.dismiss(name);
}

/**
 * Show the alert via the service
 *
 * @param name
 */
show(name: string): void
{
    this._fuseAlertService.show(name);
}

   
}

