import { Component, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import { AttributeGroupSearchModel } from 'app/models/attribute-group-search-model';
import { AttributeSearchModel } from 'app/models/attribute-search-model';
import { AttributeService } from 'app/services/attribute.service';


@Component({
    selector: 'add-overlay',
    templateUrl: './add-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddOverlayComponent {
    displayedColumns: string[] = ['id', 'category', 'attItem', 'description'];

    attribute : AttributeSearchModel = new AttributeSearchModel();
    attributeGroup: any = new AttributeGroupSearchModel();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    pageEvent:PageEvent;
    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    AddedAttribute:any=[];
    actualAttributeModels:any=[];

    constructor(public dialog: MatDialog,private attributeService: AttributeService,public dialogRef: MatDialogRef<AddOverlayComponent>,@Inject(MAT_DIALOG_DATA) public data)
    {
       
        this.attribute.pageSize=10;
        this.attribute.page=1;
        this.AddedAttribute = this.data.AddedAttribute;
  
    }

    addAttribute() {
        this.dialogRef.close(this.attribute.attributeModels);
    }

    
    sortData(sort: Sort) {
        debugger;
        this.attribute.direction=sort.direction;
        this.attribute.column=sort.active;
        this.FetchBasicData();
    }

    FetchBasicData() {

        this.attribute.page=1;
        this.attribute.pageSize=10000;
        this.attributeService.getAttributeList(this.attribute).subscribe(result => {
          console.log(result);
          debugger;
          this.attribute = result;
          for(var i=0;i< this.attribute.attributeModels.length;i++)
          {
            this.attribute.attributeModels[i].isChecked=false;
            this.attribute.attributeModels[i].isDisabled=false;
            for(var k=0;k< this.AddedAttribute.attributeModels.length;k++)
              {
                  if(this.attribute.attributeModels[i].id==this.AddedAttribute.attributeModels[k].id)
                  {
                      this.attribute.attributeModels[i].isChecked=true;
                      this.attribute.attributeModels[i].isDisabled=true;
                  }
              }
              for(var kk=0;kk<result.attributeModels.length;kk++)
              {
                  this.actualAttributeModels.push(result.attributeModels[kk]);
              }
          }

        });
   
    }

    SetIsChecked(row,event)

    {

        debugger;

        row.isChecked=event.checked;

    }

    ngOnInit() {
        this.FetchBasicData();  
    }

    OnPaginateChange(event:PageEvent)
    {
        let page=event.pageIndex;
        let size=event.pageSize;
        page=page+1;
        this.attribute.pageSize=event.pageSize;
        this.attribute.page=page;
        this.FetchBasicData();       
    }
}
