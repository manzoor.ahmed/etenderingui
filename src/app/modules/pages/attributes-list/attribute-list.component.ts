import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AddEditOverlayComponent} from './add-edit-overlay/add-edit-overlay.component';
import { map,tap } from 'lodash';
import { AttributeGroupSearchModel } from '../../../models/attribute-group-search-model';
import { AttributeGroupService } from '../../../services/attribute-group.service';
import {Sort} from '@angular/material/sort';
import { AttributeGroupViewModel } from 'app/models/ViewModels/attribute-group-view-model';
import { Router } from '@angular/router';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { FuseAlertService } from '@fuse/components/alert';

@Component({
    selector     : 'attribute-items',
    templateUrl  : './attribute-list.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AttributeListComponent
{
    displayedColumns: string[] = ['id', 'attrilisttitle', 'attlistname', 'attlistgroup'];

    attributeGroups: AttributeGroupViewModel[];
    attributeGroupSearchModel: AttributeGroupSearchModel = new AttributeGroupSearchModel();
   
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    pageEvent:PageEvent;
    panelOpenState = false;
    message: string = "";
    actualAttributeModels:any=[];

    title: string = "";
    name: string = "";
    groupName: string = "";
    type: string = "";
    /**
     * Constructor
     */
     constructor(public dialog: MatDialog,private attributeGroupService: AttributeGroupService,private router: Router
        ,private _fuseAlertService: FuseAlertService,private _fuseConfirmationService: FuseConfirmationService)
    {

        this.attributeGroupSearchModel.pageSize=10;
        this.attributeGroupSearchModel.page=1;
        
    }
    OpenURL(url,row) {

        this.router.navigateByUrl(url, { state: { Id: row.id} });

    }


    EditAttGrp(row :any) {
     debugger;
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":row.id}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.message="Updated";
            this.show("successerror");
            this.FetchBasicData();
        });   
    }

    DeleteAttGrp(model: AttributeGroupViewModel[]) {

       // this.attributeGroups = model;

       const dialogRef = this._fuseConfirmationService.open({
        "title": "Remove contact",
        "message": "Are you sure you want to delete this record?",
        "icon": {
          "show": true,
          "name": "heroicons_outline:exclamation",
          "color": "warn"
        },
        "actions": {
          "confirm": {
            "show": true,
            "label": "Remove",
            "color": "warn"
          },
          "cancel": {
            "show": true,
            "label": "Cancel"
          }
        },
        "dismissible": true
      });
    dialogRef.addPanelClass('confirmation-dialog');

    // Subscribe to afterClosed from the dialog reference
      dialogRef.afterClosed().subscribe((result) => {
          if(result=="confirmed")
          {
              
              this.attributeGroupService.DeleteItem([model]).subscribe(result => {
            //   this.attributeGroupService.DeleteItem([this.attributeGroups]).subscribe(result => {
              this.FetchBasicData();
              this.message="Deleted";
              this.show("successerror");
              });
          }
      });

    }

    OnPaginateChange(event:PageEvent)
    {
        let page=event.pageIndex;
        let size=event.pageSize;
        page=page+1;
        this.attributeGroupSearchModel.pageSize=event.pageSize;
        this.attributeGroupSearchModel.page=page;
        this.FetchBasicData();
        
    }

    sortData(sort: Sort) {
        debugger;
        this.attributeGroupSearchModel.direction=sort.direction;
        this.attributeGroupSearchModel.column=sort.active;
        this.FetchBasicData();
    }

    searchData() {

        debugger;
        let dataList: AttributeGroupViewModel[] = this.actualAttributeModels;
        
        if(this.title && this.title != ""){
            dataList = dataList.filter((data: AttributeGroupViewModel) => {
                return data.title.indexOf(this.title) > -1;
            })
        }
        if(this.name && this.name != ""){
            dataList = dataList.filter((data: AttributeGroupViewModel) => {
                return data.name.indexOf(this.name) > -1;
            })
        }
        if(this.groupName && this.groupName != ""){
            dataList = dataList.filter((data: AttributeGroupViewModel) => {
                return data.groupName.indexOf(this.groupName) > -1;
            })
        }
        if(this.type != ""){
            dataList = dataList.filter((data: AttributeGroupViewModel) => {
                return data.isPrivate === (this.type === "private" ? true : false);
            })
        }
        
        this.attributeGroupSearchModel.attributeGroupViewModels = dataList;
    }

    FetchBasicData() {
      debugger;
      
        this.attributeGroupService.getAttributeGroupList(this.attributeGroupSearchModel).subscribe(result => {
          console.log(result);
          debugger;
          this.attributeGroupSearchModel = result;

          for(var kk=0;kk<result.attributeGroupViewModels.length;kk++)
          {
              this.actualAttributeModels.push(result.attributeGroupViewModels[kk]);
          }
        });
   
    }

    ngOnInit() {
        this.FetchBasicData();
        this.dismiss("successerror");
      
    }

    openDialog() {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000"}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.message="Added";
            this.show("successerror");
            this.FetchBasicData();
        });
        
    }

     /**
     * Dismiss the alert via the service
     *
     * @param name
    */
      dismiss(name: string): void
      {
          this._fuseAlertService.dismiss(name);
      }
  
      /**
       * Show the alert via the service
       *
       * @param name
       */
      show(name: string): void
      {
          this._fuseAlertService.show(name);
      }
}

