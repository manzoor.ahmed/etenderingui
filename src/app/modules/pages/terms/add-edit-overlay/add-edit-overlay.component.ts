import { Component, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import { MatDialog, MatDialogRef ,MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { HttpEventType, HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { TermsService } from 'app/services/terms.service';
import { TermsConditionViewModel } from 'app/models/ViewModels/terms-condition-view-model';
import { TermsConditionSearchModel } from 'app/models/ViewModels/terms-condition-search-model';

@Component({
    selector: 'att-add-edit-overlay',
    templateUrl: './add-edit-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddEditOverlayComponent {
    quillModules: any = {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'align': [] }],
            [{ 'size': ['small', false, 'large', 'huge'] }],
            [{ 'font': [] }],
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            [{ 'direction': 'rtl' }],                         // text direction


            ['link', 'image', 'video']                         // link and image, video
        ]
    };
    
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    displayedColumns: string[] = ['id', 'currency', 'country'];
   

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    etmedia:any={id:"00000000-0000-0000-0000-000000000000",fileName:""};
    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    termsConditionList: FormGroup;
    dataId: any = "";
    termsCondition: TermsConditionViewModel;//Your Model 
    termsConditionTypes: any[];//Your Model 
    isDelete = false;



    constructor(@Inject(MAT_DIALOG_DATA) public data, public dialogRef: MatDialogRef<AddEditOverlayComponent>,
    public dialog: MatDialog, private fb: FormBuilder
    , private termsService: TermsService) {
        this.termsConditionList = this.fb.group({
            termsConditions: [null, Validators.required],
            isEditable: [null, Validators.required],
            inputType: [null, Validators.required],
            beforeQuoteId: [null, Validators.required],          
            submitQuoteId: [null, Validators.required],  
            isRFQ: [false, Validators.required],
            isRFI: [false, Validators.required],
            isRFAQ: [false, Validators.required],
            isPO: [false, Validators.required],
            termsConditionsAttachmentPath: ['', Validators.required],
            etMediaId: ['00000000-0000-0000-0000-000000000000', Validators.required],
            fileName: ['', Validators.required],
            wordData: ['', Validators.required],
        });
        this.dataId = data.id;
    }
    // get termsConditionModels(): FormArray {
    //     return this.termsConditionList.get("termsConditionModels") as FormArray
    // }

    // newTermsCondition(): FormGroup {
    //     debugger;
    
    //     return this.fb.group({
    //       'termsConditions': [null, Validators.required],
    //       'isEditable': [null, Validators.required],
    //       'inputType': [null, Validators.required],
    //       'beforeQuoteId': [null, Validators.required],          
    //     //   'beforeQuoteStatus': ["9C41C756-85B5-43C2-A47D-6503E39B543C", Validators.required],
    //       'submitQuoteId': [null, Validators.required],  
    //     //   'submitQuoteStatus': ["9C41C756-85B5-43C2-A47D-6503E39B543C", Validators.required],
    //       'isRFQ': [false, Validators.required],
    //       'isRFI': [false, Validators.required],
    //       'isRFAQ': [false, Validators.required],
    //       'isPO': [false, Validators.required],
    //       'termsConditionsAttachmentPath': ['', Validators.required],
    //       'etMediaId': ['', Validators.required],

    //     })
    //   }

    // addTermsCondition() {
    //     if (this.termsConditionList.get('termsConditionModels').invalid) {
    //     this.termsConditionList.get('termsConditionModels').markAllAsTouched();
    //     return;
    //     }
    //    this.termsConditionModels.push(this.newTermsCondition());

    // }
    ngOnInit() {

        // if (this.dataId == "00000000-0000-0000-0000-000000000000") {
        //   this.addTermsCondition();
        // }
            this.termsService.GetTermsConditionByID(this.dataId).subscribe(result => {//your service call here
            console.log(result);
            debugger;
            this.isDelete = true;
            // this.termsCondition = [];
            this.EInputType=result.data.inputType;
            // result.data.WordData='<p><strong>asasas<u>asdasasa</u><s><u>asasasd</u></s><em><s><u>sdsdsdsd</u></s></em></strong><strong style="color: rgb(102, 163, 224);"><em><s><u>sdsdsdsd</u></s></em></strong></p>';
            this.termsCondition=result.data;
           
            console.log(result.data);
            // this.costFactorTypes.push({"id":"","text":"Select"});
            this.termsConditionTypes=result.data.consents;
            console.log(this.termsConditionTypes)
            // const linesFormArray = this.termsConditionList.get("termsConditionModels") as FormArray;
            // linesFormArray.push(this.newTermsCondition());
            // console.log(this.termsCondition[0].beforeQuoteId)
            // console.log(this.termsCondition[0].beforeQuoteStatus)
            this.termsConditionList.patchValue(this.termsCondition);
            if (this.termsCondition.etMediaId != null) {
            this.isDeleteandDownloadEnabledVisible=true;
            }
        }); 
          
    }
    doAction() {
        this.dialogRef.close();
        window.location.reload() ;

    }
    isDeleteandDownloadEnabledVisible=false;
    onFormSubmit(form:NgForm)  
    {  
        debugger;
        console.log(form);
        let termsConditionSearchModel: TermsConditionViewModel = new TermsConditionViewModel();
        termsConditionSearchModel = Object.assign(termsConditionSearchModel, form);
        if (this.dataId != "00000000-0000-0000-0000-000000000000") {
            termsConditionSearchModel.id = this.termsCondition.id;
        }
        let terms:TermsConditionViewModel[];
        terms=[];
        terms.push(termsConditionSearchModel);
        this.termsService.SaveTermsCondition(terms).subscribe(result => {
       
        console.log(result);
        // debugger;
        this.dialogRef.close();
        });
    } 
    EInputType:any="Word";
    OnInputTypeChange(event)  
    {  
        debugger;
      this.EInputType=event.value;
    } 
    public uploadFile = (files) => {
        if (files.length === 0) {
            return;
        }
        let fileToUpload = <File>files[0];
        const formData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);

        this.termsService.uploadFile(formData).subscribe(result => {
       debugger;
           
                if(result["body"]!=undefined)
                {
                    debugger;
                    // this.termsConditionList.get("etMediaId").setValue(result["body"].object[0].id);
                    // this.etmedia.fileName=result["body"].object[0].fileName
                    // this.termsConditionList.get("etMediaId").setValue(result["body"].object[0].fileName);
this.isDeleteandDownloadEnabledVisible=true;
                    this.termsConditionList.controls['etMediaId'].setValue(result["body"].object[0].id);
                    this.termsConditionList.controls['fileName'].setValue(result["body"].object[0].fileName);
                }
              
        });
        
    }

    DownloadMedia() {
        this.etmedia.id=this.termsConditionList.controls['etMediaId'].value;
        this.termsService.DownloadMedia(this.etmedia).subscribe(blob => {
       
            debugger;
            saveAs(blob, this.termsConditionList.controls['fileName'].value, {
                type: 'application/pdf;charset=utf-8' // --> or whatever you need here
            });
              
        });
        
    }

    DeleteFile() {
        this.etmedia.id=this.termsConditionList.controls['etMediaId'].value;
        // this.termsService.DeleteFile(this.etmedia).subscribe(result => {
       
        //     console.log(result);

            this.isDeleteandDownloadEnabledVisible=false;
        this.termsConditionList.controls['etMediaId'].setValue(null);
        this.termsConditionList.controls['fileName'].setValue('');
              
        // });
        
    }
}

