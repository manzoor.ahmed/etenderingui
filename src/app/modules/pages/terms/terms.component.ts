import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator,PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AddEditOverlayComponent} from './add-edit-overlay/add-edit-overlay.component';
import { TermsService } from 'app/services/terms.service';
import { TermsConditionSearchModel } from 'app/models/ViewModels/terms-condition-search-model';
import { Sort } from '@angular/material/sort';
import { TermsConditionViewModel } from 'app/models/ViewModels/terms-condition-view-model';
import { FuseConfirmationService } from '@fuse/services/confirmation/confirmation.service';
import { FuseAlertService } from '@fuse/components/alert';

@Component({
    selector     : 'terms',
    templateUrl  : './terms.component.html',
    encapsulation: ViewEncapsulation.None
})
export class TermsComponent
{
    displayedColumns: string[] = ['id', 'name', 'inputType', 'editable', 'beforeQuote','submitQuote', 'rfq', 'rfi', 'rfaq', 'po'];
  

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    terms: TermsConditionViewModel[];
    Message:string="";
    pageEvent: PageEvent;
    panelOpenState = false;
    /**
     * Constructor
     */
     termsConditionSearchModel: TermsConditionSearchModel = new TermsConditionSearchModel();
    constructor(public dialog: MatDialog, 
        private _fuseConfirmationService: FuseConfirmationService,
        private _fuseAlertService: FuseAlertService,
        private costFactorService: TermsService)
    {
        this.termsConditionSearchModel.pageSize = 10;
        this.termsConditionSearchModel.page = 1;
    }


    ngOnInit() {
        this.FetchBasicData();
        this.dismiss("successerror")
    }
    

    EditTerms(row: any) {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":row.id}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.Message="Updated";
            this.show("successerror");
            this.FetchBasicData();
        });
    }
    
    OnPaginateChange(event: PageEvent) {
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.termsConditionSearchModel.pageSize = event.pageSize;
        this.termsConditionSearchModel.page = page;
        this.FetchBasicData();
        //this.dataSource=   this.CreatePaginationData(page,size);

    }

    sortData(sort: Sort) {
        debugger;
        this.termsConditionSearchModel.direction = sort.direction;
        this.termsConditionSearchModel.column = sort.active;
        this.FetchBasicData();
    }

    FetchBasicData() {
        debugger;
        
        this.costFactorService.GetTermsConditionList(this.termsConditionSearchModel).subscribe(result => {
            console.log(result);
            // debugger;
            this.termsConditionSearchModel = result;
            
        });
    }
    openDialog() {
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000"}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            this.Message="Added";
            this.show("successerror");
          this.FetchBasicData();
        });
    }
    
    DeleteTerms(model: TermsConditionViewModel[])
    {
        // this.dismiss("successerror");
        // this.show("successerror");
        // this.dismiss("successerror");

        const dialogRef = this._fuseConfirmationService.open({
            "title": "Remove contact",
            "message": "Are you sure you want to delete this record?",
            "icon": {
              "show": true,
              "name": "heroicons_outline:exclamation",
              "color": "warn"
            },
            "actions": {
              "confirm": {
                "show": true,
                "label": "Remove",
                "color": "warn"
              },
              "cancel": {
                "show": true,
                "label": "Cancel"
              }
            },
            "dismissible": true
          });
        dialogRef.addPanelClass('confirmation-dialog');

        // Subscribe to afterClosed from the dialog reference
        dialogRef.afterClosed().subscribe((result) => {
            if(result=="confirmed")
            {
                this.Message="Deleted";
                this.costFactorService.DeleteTermsCondition([model]).subscribe(result => {
                    this.Message="Deleted";
                    this.FetchBasicData();
                      this.show("successerror");              
                      
              });
        }
        });     
    }

        /**
 * Dismiss the alert via the service
 *
 * @param name
 */
dismiss(name: string): void
{
    this._fuseAlertService.dismiss(name);
}

/**
 * Show the alert via the service
 *
 * @param name
 */
show(name: string): void
{
    this._fuseAlertService.show(name);
}


}