import { Component, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators ,FormsModule,NgForm,FormArray } from '@angular/forms';  
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { SupplierGroupSearchModel } from '../../../../models/supplier-group-search-model';
 import { SupplierInvitationListService } from '../../../../services/supplier-invitation-list.service';
import { SupplierGroupViewModel } from '../../../../models/ViewModels/supplier-group-view-model';


@Component({
    selector: 'att-list-add-edit-overlay',
    templateUrl: './add-edit-overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AddEditOverlayComponent {
    @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

    templateData: any = [];
    useremail: string = '';

    selectedId: any = [];
    errormessage = 'Something went wrong, please try again.';
    successmessage = 'Successfully added the template';
    issuccess = false;
    iserror = false;
    frmSupplierGroup :  FormGroup;
    isDelete = false;
    dataId: any = "";
    supplierGroupViewModel: SupplierGroupViewModel[];


    constructor(@Inject(MAT_DIALOG_DATA) public data,public dialogRef: MatDialogRef<AddEditOverlayComponent>,
    public dialog: MatDialog,private fb: FormBuilder
    ,private supplierInvitationListService: SupplierInvitationListService
    ) {
        this.frmSupplierGroup = this.fb.group({
            'title': [null, Validators.required],
            'name': [null, Validators.required],
            'isPrivate': [null, Validators.required]
          })
          this.frmSupplierGroup = this.fb.group({  
            supplierGroupViewModels: this.fb.array([])
          });  
        this.dataId=data.id;
    }
    get supplierGroupViewModels(): FormArray {
      return this.frmSupplierGroup.get("supplierGroupViewModels") as FormArray
    }
    newSupplierList(): FormGroup {
      debugger;
  
      return this.fb.group({
            'title': [null, Validators.required],
            'name': [null, Validators.required],
            'isPrivate': [null, Validators.required]
      })
    }
    addSupplierList() {
      debugger;
      if (this.frmSupplierGroup.get('supplierGroupViewModels').invalid) {
        this.frmSupplierGroup.get('supplierGroupViewModels').markAllAsTouched();
        return;
      }
      this.supplierGroupViewModels.push(this.newSupplierList());
  
    }
    ngOnInit() {
      if (this.dataId == "00000000-0000-0000-0000-000000000000") {
        this.addSupplierList();
      }
      else {
  
        this.supplierInvitationListService.GetSupplierGroupByID(this.dataId).subscribe(result => {
          console.log(result);
          debugger;
          this.isDelete = true;
          this.supplierGroupViewModel = [];
          this.supplierGroupViewModel.push(result.data);
          const linesFormArray = this.frmSupplierGroup.get("supplierGroupViewModels") as FormArray;
          linesFormArray.push(this.newSupplierList());
          this.frmSupplierGroup.patchValue({ "supplierGroupViewModels": this.supplierGroupViewModel });
        });
  
      }
       }
      onFormSubmit(form:NgForm)  
      {  
        debugger;
        console.log(form);
        let supplierGroupSearchModel: SupplierGroupSearchModel = new SupplierGroupSearchModel();
        supplierGroupSearchModel = Object.assign(supplierGroupSearchModel, form);
        if (this.dataId != "00000000-0000-0000-0000-000000000000") {
          supplierGroupSearchModel.supplierGroupViewModels[0].id = this.supplierGroupViewModel[0].id;
        }
        this.supplierInvitationListService.SaveSupplierGroup(supplierGroupSearchModel.supplierGroupViewModels).subscribe(result => {
          console.log(result);
          debugger;
    
          supplierGroupSearchModel.supplierGroupViewModels = result;
          this.dialogRef.close("Saved");
        });
      }  
    doAction() {
        this.dialogRef.close("Cancelled");
     

    }


}
