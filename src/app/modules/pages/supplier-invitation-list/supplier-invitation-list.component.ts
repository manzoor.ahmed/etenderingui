import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {AddEditOverlayComponent} from './add-edit-overlay/add-edit-overlay.component';
import { SupplierGroupSearchModel } from 'app/models/supplier-group-search-model';
import { SupplierSearchModel } from 'app/models/supplier-search-model';
import { SupplierInvitationListService } from 'app/services/supplier-invitation-list.service';
import { Router } from '@angular/router';
import { FuseAlertService } from '@fuse/components/alert';
import { FuseConfirmationService } from '@fuse/services/confirmation/confirmation.service';

@Component({
    selector     : 'attribute-items',
    templateUrl  : './supplier-invitation-list.component.html',
    encapsulation: ViewEncapsulation.None
})
export class SupplierInvitationListComponent
{
    displayedColumns: string[] = ['id', 'suplisttitle', 'suplistname'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;
    supplierGroups : any = new SupplierGroupSearchModel();
    supplier : any = new SupplierSearchModel();

    SupplierTitle : string="";

    SupplierName : string = "";

    actualSuppplierModels:  any =[];
    Message:string="";
    type : string = "";

    /**
     * Constructor
     */
     constructor(public dialog: MatDialog
        ,  private supplierInvitationListService: SupplierInvitationListService
        ,private router: Router
        ,private _fuseAlertService: FuseAlertService
        ,private _fuseConfirmationService: FuseConfirmationService)
     {
         debugger;
         this.supplierGroups.pageSize=10;
         this.supplierGroups.page=1;
       
     }
 
     OnPaginateChange(event: PageEvent){
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.supplierGroups.pageSize = event.pageSize;
        this.supplierGroups.page = page;
        this.FetchBasicData();
    }

    sortData(sort: Sort){
        debugger;
        this.supplierGroups.direction = sort.direction;
        this.supplierGroups.column = sort.active;
        this.FetchBasicData();
    }

    FetchBasicData() {
        debugger;
        this.supplierInvitationListService.getSGList(this.supplierGroups).subscribe(
            result => {
            console.log(result);
            debugger;
            this.supplierGroups = result;

            for(var i=0; i<result.supplierGroupModels.length; i++){

                this.actualSuppplierModels.push(result.supplierGroupModels[i]);

            }
            }
        );
    }

    ngOnInit() {
        this.dismiss("successerror");
        this.FetchBasicData();

    }

    OpenURL(url,row) {
        //this.router.navigate([url]); 
        debugger;
        this.router.navigateByUrl(url, { state: { Id: row.id } });
    }

    openDialog() {
     
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000"}});
        dialogRef.addPanelClass('inline-md-overlay');
       
        dialogRef.afterClosed().subscribe((result) => {
           if(result=="Saved")
           {
            this.Message="Added";
            this.show("successerror");
            this.FetchBasicData();
           }
            
        });
    }
    editList(row :any) {   
        const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":row.id}});
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            if(result=="Saved")
            {
            this.Message="Updated";
            this.show("successerror");
            this.FetchBasicData();
            }
        });      
    }
        /**
 * Dismiss the alert via the service
 *
 * @param name
 */
dismiss(name: string): void
{
    this._fuseAlertService.dismiss(name);
}

/**
 * Show the alert via the service
 *
 * @param name
 */
show(name: string): void
{
    this._fuseAlertService.show(name);
}
    
}

