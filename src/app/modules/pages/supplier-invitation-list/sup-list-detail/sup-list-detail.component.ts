import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { AddOverlayComponent } from './add-overlay/add-overlay.component';
import { SupplierGroupSearchModel } from 'app/models/supplier-group-search-model';
import { SupplierSearchModel } from 'app/models/supplier-search-model';
import { SupplierInvitationListService } from 'app/services/supplier-invitation-list.service';
import { Router } from '@angular/router';
import { SupplierViewModel } from 'app/models/ViewModels/supplier-view-model';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { FuseAlertService } from '@fuse/components/alert';

@Component({
    selector: 'rfx',
    templateUrl: './sup-list-detail.component.html',
    encapsulation: ViewEncapsulation.None
})
export class SupListDetailComponent {
    displayedColumns: string[] = ['id', 'supId', 'name', 'status', 'address', 'contact', 'email'];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;
    supplierGroupID: any;
pageEvent:PageEvent;
    supplierGroups: any = new SupplierGroupSearchModel();
    supplier: any = new SupplierSearchModel();
    supplierName: string = "";
    ifsSupplierId: string = "";
    supplierStatus: string = "";
    contactTitle: string = "";
    message: string = "";
    actualSuppplierModels: any = [];
    /**
     * Constructor
     */
    constructor(public dialog: MatDialog, private supplierInvitationListService: SupplierInvitationListService, private router: Router,
        private _fuseConfirmationService: FuseConfirmationService,
        private _fuseAlertService: FuseAlertService) {
        this.supplierGroupID = this.router.getCurrentNavigation().extras.state.Id;
        this.supplierGroups.pageSize = 5;
        this.supplierGroups.page = 1;
     
    }


    OnPaginateChange(event: PageEvent) {
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.supplierGroups.pageSize = event.pageSize;
        this.supplierGroups.page = page;
        this.FetchBasicData();
        //  this.dataSource=   this.CreatePaginationData(page,size);
    }

    FetchBasicData() {

        this.supplierInvitationListService.GetSupplierGroupByID(this.supplierGroupID).subscribe(result => {
                console.log(result);
                debugger;
                this.supplierGroups = result.data;
                if(this.supplierGroups)
                {
                    if(!this.supplierGroups.supplierModels)
                    {
                        this.supplierGroups.supplierModels=[];
                    }
                }

                for (var i = 0; i < result.data.supplierModels.length; i++) {

                    this.actualSuppplierModels.push(result.data.supplierModels[i]);

                }

            }
        );
    }

    ngOnInit() {
        this.FetchBasicData();
        // this.saveChanges();
    }
    sortData(sort: Sort) {
        debugger;
        this.supplier.direction = sort.direction;
        this.supplier.column = sort.active;
        this.FetchBasicData();
    }

    saveChanges() {
        debugger;
        for (var i = 0; i < this.actualSuppplierModels.length; i++) {
            this.actualSuppplierModels[i].supplierGroupId = this.supplierGroupID;

        }
        //this.costFactorGroups.costFactorModels.push({id:"F9737153-CF99-4049-2F59-08D978387FC5"});
        this.supplierInvitationListService.SaveSupplierGroupMapping(this.actualSuppplierModels).subscribe(
            result => {

                console.log(result);
                debugger;
                this.message = "Saved";

                this.show("successerror");
                // this.costFactorGroups = result;
            }
        );
    }
    openDialog() {
        debugger;
        const dialogRef = this.dialog.open(AddOverlayComponent, { data: { AddedSupplierInvitationList: this.supplierGroups } });
        dialogRef.addPanelClass('inline-md-overlay');
        dialogRef.afterClosed().subscribe(result => {
            debugger;
            let data: any = [];
            if(this.supplierGroups)
                {
                    if(!this.supplierGroups.supplierModels)
                    {
                        this.supplierGroups.supplierModels=[];
                    }
                }
            for (var i = 0; i < this.supplierGroups.supplierModels.length; i++) {                
                data.push(this.supplierGroups.supplierModels[i]);
            }
            for (var i = 0; i < result.length; i++) {
                if (result[i].isChecked == true && result[i].isDisabled == false) {
                    data.push(result[i]);
                }

            }
            //this.changeDetectorRef.detectChanges();
            this.supplierGroups.supplierModels = [];
            this.supplierGroups.supplierModels = data;
            this.actualSuppplierModels=[];
            for(var kk=0;kk<this.supplierGroups.supplierModels.length;kk++)
            {
                this.actualSuppplierModels.push(this.supplierGroups.supplierModels[kk]);
            }
        });
    }


    searchData() {
        debugger;

        let dataList: SupplierViewModel[] = this.actualSuppplierModels;

        if (this.supplierName && this.supplierName != "") {

            dataList = dataList.filter((data: SupplierViewModel) => {

                return data.supplierName.indexOf(this.supplierName) > -1;

            })

        }

        if (this.ifsSupplierId && this.ifsSupplierId != "") {

            dataList = dataList.filter((data: SupplierViewModel) => {

                return data.ifsSupplierId === this.ifsSupplierId;

            })

        }

        if (this.supplierStatus && this.supplierStatus != "") {

            dataList = dataList.filter((data: SupplierViewModel) => {

                return data.supplierStatus === this.supplierStatus;

            })

        }


        if (this.contactTitle && this.contactTitle != "") {

            dataList = dataList.filter((data: SupplierViewModel) => {

                return data.contactTitle === this.contactTitle;

            })

        }


        this.supplierGroups.supplierModels = dataList;
        console.log(this.supplier, this.supplierName, this.supplierStatus, this.ifsSupplierId);

    }
    Delete(row) {
        debugger;

        const dialogRef = this._fuseConfirmationService.open({
            "title": "Remove contact",
            "message": "Are you sure you want to delete this record?",
            "icon": {
                "show": true,
                "name": "heroicons_outline:exclamation",
                "color": "warn"
                        },
            "actions": {
                "confirm": {
                    "show": true,
                    "label": "Remove",
                    "color": "warn"
                },
                "cancel": {
                    "show": true,
                    "label": "Cancel"
                }
                        },
            "dismissible": true
        });

        dialogRef.addPanelClass('confirmation-dialog');

        // Subscribe to afterClosed from the dialog reference

        dialogRef.afterClosed().subscribe((result) => {
            if (result == "confirmed") {
                for (var kk = 0; kk < this.actualSuppplierModels.length; kk++) {
                    if (row.id == this.actualSuppplierModels[kk].id) {
                        this.actualSuppplierModels[kk].IsChecked = false;
                    }
                }
                var attributeIndex = -1;
                for (var kk = 0; kk < this.supplierGroups.supplierModels.length; kk++) {
                    if (row.id == this.supplierGroups.supplierModels[kk].id) {
                        attributeIndex = kk;
                    }
                }
                this.supplierGroups.supplierModels = this.supplierGroups.supplierModels.filter(obj => obj.id !== row.id);
                row.IsChecked = false;
            }
        });
    }

    dismiss(name: string): void {
        this._fuseAlertService.dismiss(name);
    }

    show(name: string): void {
        this._fuseAlertService.show(name);
    }
}

