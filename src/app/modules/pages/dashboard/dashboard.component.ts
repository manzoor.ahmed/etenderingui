import { Component, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TagsOverlayComponent } from 'app/modules/common/tags-overlay/tags-overlay.component';
import {MatTableDataSource} from '@angular/material/table';
import {ApexNonAxisChartSeries, ApexPlotOptions, ApexChart, ApexFill, ApexDataLabels, ApexStroke, ApexStates} from 'ng-apexcharts';

export type ChartOptions = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    labels: string[];
    plotOptions: ApexPlotOptions;
    fill: ApexFill;
    dataLabels: ApexDataLabels;
    stroke: ApexStroke;
    states: ApexStates;
};
export type ChartOptions2 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    labels: string[];
    plotOptions: ApexPlotOptions;
    fill: ApexFill;
    dataLabels: ApexDataLabels;
    stroke: ApexStroke;
    states: ApexStates;
};
export type ChartOptions3 = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    labels: string[];
    plotOptions: ApexPlotOptions;
    fill: ApexFill;
    dataLabels: ApexDataLabels;
    stroke: ApexStroke;
    states: ApexStates;
};

export interface RowData {
    value: string;
    status: string;
    title: string;
}
/** Constants used to fill up our data base. */
const VALUE: string[] = [
    'USD 9.00', 'USD 12.00'
];
const STATUS: string[] = [
    'Approved', 'Rejected'
];
const TITLE: string[] = [
    'Title One', 'Title Two'
];

@Component({
    selector     : 'dashboard',
    templateUrl  : './dashboard.component.html',
    encapsulation: ViewEncapsulation.None
})

export class DashboardComponent
{
    displayedColumns: string[] = ['title', 'status', 'value'];
    dataSource: MatTableDataSource<RowData>;
    panelOpenState = false;
    public chartOptions: Partial<ChartOptions>;
    public chartOptions2: Partial<ChartOptions2>;
    public chartOptions3: Partial<ChartOptions3>;

    /**
     * Constructor
     */
    constructor(public dialog: MatDialog)
    {
        const users = Array.from({length: 3}, () => createNewRow());

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);

        this.chartOptions = {
            series: [80],
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            chart: {
                height: 190,
                type: 'radialBar',
            },
            fill: {
                colors: ['#4E36E2']
            },
            plotOptions: {
                radialBar: {
                    hollow: {
                        size: '65%',
                        margin: 10,
                    },
                    dataLabels:{
                        name: {
                            fontSize: '14px',
                            color: '#6c6c6c',
                            fontWeight: 600,
                            fontFamily: 'Roboto',
                            offsetY: -10,
                        },
                        value: {
                            fontSize: '26px',
                            color: '#4E36E2',
                            offsetY: 5,
                        },
                    }
                }
            },
            stroke: {
                lineCap: 'round'
            },
            labels: ['Total Orders'],

        };
        this.chartOptions2 = {
            series: [60],
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            chart: {
                height: 190,
                type: 'radialBar',
            },
            fill: {
                colors: ['#8BC740']
            },
            plotOptions: {
                radialBar: {
                    hollow: {
                        size: '65%',
                        margin: 10,
                    },
                    dataLabels:{
                        name: {
                            fontSize: '14px',
                            color: '#6c6c6c',
                            fontWeight: 600,
                            fontFamily: 'Roboto',
                            offsetY: -10,
                        },
                        value: {
                            fontSize: '26px',
                            color: '#8BC740',
                            offsetY: 5,
                        },
                    }
                }
            },
            stroke: {
                lineCap: 'round'
            },
            labels: ['Total Invoices'],

        };
        this.chartOptions3 = {
            series: [45],
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.2,
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0,
                    }
                },
            },
            chart: {
                height: 190,
                type: 'radialBar',
            },
            fill: {
                colors: ['#FF6967']
            },
            plotOptions: {
                radialBar: {
                    hollow: {
                        size: '65%',
                        margin: 10,
                    },
                    dataLabels:{
                        name: {
                            fontSize: '14px',
                            color: '#6c6c6c',
                            fontWeight: 600,
                            fontFamily: 'Roboto',
                            offsetY: -10,
                        },
                        value: {
                            fontSize: '26px',
                            color: '#FF6967',
                            offsetY: 5,
                        },
                    }
                }
            },
            stroke: {
                lineCap: 'round'
            },
            labels: ['Total Sourcing'],

        };
    }

    openDialog() {
      const dialogRef = this.dialog.open(TagsOverlayComponent);
      dialogRef.addPanelClass('inline-md-overlay');
      dialogRef.afterClosed().subscribe(result => {
      });
  }
}

/** Builds and returns a new createNewRow. */
function createNewRow(): RowData {

    return {
        title: TITLE[Math.round(Math.random() * (TITLE.length - 1))],
        status: STATUS[Math.round(Math.random() * (STATUS.length - 1))],
        value: VALUE[Math.round(Math.random() * (VALUE.length - 1))],

    };
}
