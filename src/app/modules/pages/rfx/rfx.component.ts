import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator,PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { RFXSearchModel } from '../../../models/rfx-search-model';
import { RFXViewModel } from '../../../models/ViewModels/rfx-view-model';
import {Sort} from '@angular/material/sort';
import { RFXService } from '../../../services/rfx.service';

import { FuseConfirmationService } from '@fuse/services/confirmation';
import { FuseAlertService } from '@fuse/components/alert';

@Component({
    selector     : 'rfx',
    templateUrl  : './rfx.component.html',
    encapsulation: ViewEncapsulation.None
})
export class RfxComponent
{
    displayedColumns: string[] = ['id', 'number', 'revision', 'type', 'name', 'created', 'startDate', 'endDate', 'status'];
    dataSource: RFXSearchModel=null;
    rfxType:string="RFQ";
    Message: string;
    selected: string;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
 RFXNo : string = "";
    revision : string = "";
    type : string = "";
    panelOpenState = false;
    resultsLength:any;
    pageEvent:PageEvent;
    dataId: any = "";
    issuccess = false;
    iserror = false;
    rfx : RFXViewModel[];
    /**
     * Constructor
     */
    rfxSearchModel: RFXSearchModel = new RFXSearchModel();
  numberingSequenceService: any;
    /**
     * Constructor
     */
    constructor(public dialog: MatDialog,
        private rfxService: RFXService,
        private _fuseConfirmationService: FuseConfirmationService,
        private _fuseAlertService: FuseAlertService)
    {
        this.rfxSearchModel.pageSize=10;
        this.rfxSearchModel.page=1;
    }


    SearchData(fielsName,fieldData){


      if(fielsName=="number")
      {
this.rfxSearchModel.RFXNumber=fieldData.value;
      }
      else if(fielsName=="revision")
      {
        this.rfxSearchModel.Revision=fieldData.value;
      }
      else if(fielsName=="rfxType")
      {
        this.rfxSearchModel.rFxType=fieldData.value;
      }
      else if(fielsName=="rfxName")
      {
        this.rfxSearchModel.RFXName=fieldData.value;
      }
      else if(fielsName=="created")
      {
        this.rfxSearchModel.RFXNumber=fieldData.value;
      }
      else if(fielsName=="startDate")
      {
        this.rfxSearchModel.RFXNumber=fieldData.value;
      }
      else if(fielsName=="endDate")
      {
        this.rfxSearchModel.RFXNumber=fieldData.value;
      }
      else if(fielsName=="status")
      {
        this.rfxSearchModel.statusName=fieldData.value;
      }

      this.FetchBasicData();
    
    }

    OnPaginateChange(event:PageEvent)
    {
      debugger;
        let page=event.pageIndex;
        let size=event.pageSize;
        page=page+1;
        this.rfxSearchModel.pageSize=event.pageSize;
        this.rfxSearchModel.page=page;
        this.fetchRFXData("RFQ");

           
    }
    sortData(sort: Sort) {
      debugger;
      this.rfxSearchModel.direction = sort.direction;
      this.rfxSearchModel.column = sort.active;
      this.FetchBasicData();
    }
    FetchBasicData() {
        debugger;
         this.rfxService.getRFQList(this.rfxSearchModel).subscribe(result => {
           debugger;
           this.rfxSearchModel = result;
           console.log(this.rfxSearchModel)
         })  
        }
  
        fetchRFXData(type: string) {
          //this.rfxType=type;
          this.rfxSearchModel.rFxType = type;
          this.FetchBasicData();
        }
        
      ngOnInit() {
         //this.FetchBasicData();
         this.fetchRFXData(this.rfxType);
  
        }
        rfxChange($event){
            console.log($event);
            if($event.index === 0){
                this.fetchRFXData("RFQ");
            }
            if($event.index === 1){
                this.fetchRFXData("RFI");
            }
            if($event.index === 2){
                this.fetchRFXData("RFAQ");
            }
            if($event.index === 3){
                this.fetchRFXData("Draft");
            }
            if($event.index === 4){
              this.fetchRFXData("Published");
            }
            if($event.index === 5){
              this.fetchRFXData("Awarded");
            }
            if($event.index === 6){
              this.fetchRFXData("Revised");
            }
        }
      
          DeleteRFX(model: RFXViewModel[]) {
              
              this.rfx = model;
              this.rfxService.DeleteItem([this.rfx]).subscribe(result => {
      
                  this.rfx = result;
                  this.fetchRFXData("RFQ");
              });
              const dialogRef = this._fuseConfirmationService.open({
                "title": "Remove contact",
                "message": "Are you sure you want to delete this record?",
                "icon": {
                  "show": true,
                  "name": "heroicons_outline:exclamation",
                  "color": "warn"
                },
                "actions": {
                  "confirm": {
                    "show": true,
                    "label": "Remove",
                    "color": "warn"
                  },
                  "cancel": {
                    "show": true,
                    "label": "Cancel"
                  }
                },
                "dismissible": true
          });
          
          dialogRef.afterClosed().subscribe((result) => {
            if(result=="confirmed")
            {
                this.Message="Deleted";
            this.rfxService.DeleteItem([model]).subscribe(result => {
                debugger;
                  this.fetchRFXData(this.rfxType);
                    this.show("successerror");
                  
               
      
            });
        }
        });
        
      }
      show(name: string): void
      {
          this._fuseAlertService.show(name);
      }
      
      dismiss(name: string): void
      {
          this._fuseAlertService.dismiss(name);
      }
      
      
      
      changeSelected(e) {
        console.log(e);
        this.selected = e.value;
      }
            
}

