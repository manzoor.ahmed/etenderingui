import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { Router } from '@angular/router';

export interface RowData {
    id: string;
    number: string;
    description: string;
    type: string;
    name: string;
    createdDate: string;
    status: string;
}

/** Constants used to fill up our data base. */
const NUMBER: string[] = [
    '1231', '1232', '1233', '1234', '1235', '1236',
];
const DESCRIPTION: string[] = [
    'Laptop template 01', 'Laptop template 02', 'Laptop template 03', 'Laptop template 04', 'Laptop template 05', 'Laptop template 06',
];
const TYPE: string[] = [
    'RFI', 'RFAQ', 'RFI', 'RFAQ', 'RFI', 'RFAQ',
];
const NAME: string[] = [
    'Office Supplies for 3rd floor', 'Port Dubai warehouse  01', 'Port Dubai warehouse  02', 'Port Dubai warehouse  03', 'Port Dubai warehouse  04', 'Port Dubai warehouse  05',
];
const CREATEDDATE: string[] = [
    '21-Jan-2020', '20-Jan-2020', '22-Jan-2020', '24-Jan-2020', '22-Jan-2020', '21-Jan-2020',
];
const STATUS: string[] = [
    'Awarded', 'Awarded', 'Revised', 'Revised', 'Awarded', 'Revised',
];



@Component({
    selector     : 'rfx',
    templateUrl  : './rfx-new-from-existing.component.html',
    encapsulation: ViewEncapsulation.None
})
export class RfxNewFromExistingComponent
{
    displayedColumns: string[] = ['id', 'number', 'createdDate', 'name', 'description', 'type', 'status'];
    dataSource: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;
    /**
     * Constructor
     */
    constructor(public dialog: MatDialog,private router: Router)
    {
        const users = Array.from({length: 100}, (_, k) => createNewRow(k + 1));
console.log(this.router.getCurrentNavigation().extras.state);
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    OpenURL(url) {
        this.router.navigate([url]);
    }
}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        id: id.toString(),
        number: NUMBER[Math.round(Math.random() * (NUMBER.length - 1))],
        description: DESCRIPTION[Math.round(Math.random() * (DESCRIPTION.length - 1))],
        type: TYPE[Math.round(Math.random() * (TYPE.length - 1))],
        name: NAME[Math.round(Math.random() * (NAME.length - 1))],
        createdDate: CREATEDDATE[Math.round(Math.random() * (CREATEDDATE.length - 1))],
        status: STATUS[Math.round(Math.random() * (STATUS.length - 1))],
    };
}

