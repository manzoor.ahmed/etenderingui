import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map, tap } from 'lodash';
import { Sort } from '@angular/material/sort';
import { RFXTemplateService } from 'app/services/rfx-template.service';
import { RFXTemplateSearchModel } from "app/models/rfx-template-search-model";
import { Router } from '@angular/router';



@Component({
    selector: 'rfx',
    templateUrl: './rfx-new-from-template.component.html',
    encapsulation: ViewEncapsulation.None
})
export class RfxNewFromTemplateComponent {
    displayedColumns: string[] = ['id', 'name', 'description', 'type'];

    pageEvent: PageEvent;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;
    /**
     * Constructor
     */
    rfxTemplates: any = new RFXTemplateSearchModel();
    constructor(public dialog: MatDialog, private rfxTemplateService: RFXTemplateService,private router: Router) {
        this.rfxTemplates.pageSize = 10;
        this.rfxTemplates.page = 1;
    }
    OpenURL(url) {
        this.router.navigate([url]);
    }
    //  EditTeam(row :any) {

    //     const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":row.id}});
    //     dialogRef.addPanelClass('inline-md-overlay');
    //     dialogRef.afterClosed().subscribe(result => {
    //       this.FetchBasicData();
    //     });

    // }

    OnPaginateChange(event: PageEvent) {
        let page = event.pageIndex;
        let size = event.pageSize;
        page = page + 1;
        this.rfxTemplates.pageSize = event.pageSize;
        this.rfxTemplates.page = page;
        this.FetchBasicData();
        //  this.dataSource=   this.CreatePaginationData(page,size);

    }
    sortData(sort: Sort) {
        debugger;
        this.rfxTemplates.direction = sort.direction;
        this.rfxTemplates.column = sort.active;
        this.FetchBasicData();
    }
    FetchBasicData() {
        debugger;

        this.rfxTemplateService.getRFXTemplateList(this.rfxTemplates).subscribe(result => {
            console.log(result);
            debugger;
            this.rfxTemplates = result;
        });

    }
    ngOnInit() {
        this.FetchBasicData();

    }

    //   openDialog() {
    //     const dialogRef = this.dialog.open(AddEditOverlayComponent,{data:{"id":"00000000-0000-0000-0000-000000000000"}});
    //     dialogRef.addPanelClass('inline-md-overlay');
    //     dialogRef.afterClosed().subscribe(result => {
    //       this.FetchBasicData();
    //     });
    // }
}

