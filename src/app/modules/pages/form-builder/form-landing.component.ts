import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface RowData13 {
    formId: string;
    formName: string;
    status: string;
    createdBy: string;
    publishDate: string;
}

/** Constants used to fill up our data base. */
const FORMID: string[] = [
    'FID 12674', 'FID 12673'
];
const FORMNAME: string[] = [
    'Quarterly review Form Network Vendors', 'Annual review Form for IT Vendors', 'Annual review form for Financial service providers'
];
const STATUS: string[] = [
    'Approved'
];
const CREATEDBY: string[] = [
    'Person One', 'Person Two'
];
const PUBLISHDATE: string[] = [
    '28/12/2021', '18/12/2021'
];

@Component({
    selector     : 'form-landing',
    templateUrl  : './form-landing.component.html',
    encapsulation: ViewEncapsulation.None,
})

export class FormLandingComponent
{
    displayedColumn13: string[] = ['index','formId', 'formName', 'status', 'createdBy','publishDate'];
    dataSource13: MatTableDataSource<RowData13>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    panelOpenState = false;

    /**
     * Constructor
     */
    constructor(public dialog: MatDialog)
    {
        const users13 = Array.from({length:3}, (_, k) => createNewRow13());

        // Assign the data to the data source for the table to render
        this.dataSource13 = new MatTableDataSource(users13);
    }


    isShow = true;
    isShow2 = true;

    toggleDisplay() {
        this.isShow = !this.isShow;
    }
    toggleDisplay2() {
        this.isShow2 = !this.isShow2;
    }

}
function createNewRow13(): RowData13 {

    return {
        formId: FORMID[Math.round(Math.random() * (FORMID.length - 1))],
        formName: FORMNAME[Math.round(Math.random() * (FORMNAME.length - 1))],
        status: STATUS[Math.round(Math.random() * (STATUS.length - 1))],
        createdBy: CREATEDBY[Math.round(Math.random() * (CREATEDBY.length - 1))],
        publishDate: PUBLISHDATE[Math.round(Math.random() * (PUBLISHDATE.length - 1))],
    };
}

