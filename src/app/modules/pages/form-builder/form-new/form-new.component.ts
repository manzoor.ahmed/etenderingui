import {ChangeDetectionStrategy, Component, ViewChild, ViewEncapsulation, OnInit, ChangeDetectorRef, Inject, ElementRef, TemplateRef, Renderer2} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {ActivatedRoute, Router} from '@angular/router';
import {PreviewFormOverlayComponent} from '../preview-form-overlay/preview-form-overlay.component';
import { Contact, Country } from './contacts.types';
import { FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ContactsService} from './contacts.service';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import {DatePipe} from '@angular/common';


export interface RowData {
    id: string;
    number: string;
    revision: string;
    type: string;
    name: string;
    created: string;
    startDate: string;
    endDate: string;
    status: string;
}

/** Constants used to fill up our data base. */
const NUMBER: string[] = [
    '1231', '1232', '1233', '1234', '1235', '1236',
];
const REVISION: string[] = [
    '00', '01', '11', '00', '01', '10',
];
const TYPE: string[] = [
    'RFI', 'RFAQ', 'RFI', 'RFAQ', 'RFI', 'RFAQ',
];
const NAME: string[] = [
    'Office Supplies for 3rd floor', 'Port Dubai warehouse  01', 'Port Dubai warehouse  02', 'Port Dubai warehouse  03', 'Port Dubai warehouse  04', 'Port Dubai warehouse  05',
];
const CREATED: string[] = [
    'Andy Robert', 'Jason Mac', 'Jacob Gray', 'Chris Potter', 'Mohammed Kafil', 'Jason Potter',
];
const STARTDATE: string[] = [
    '21-Jan-2020', '20-Jan-2020', '22-Jan-2020', '24-Jan-2020', '22-Jan-2020', '21-Jan-2020',
];
const ENDDATE: string[] = [
    '21-Jan-2021', '21-Jan-2021', '21-Jan-2021', '21-Jan-2021', '21-Jan-20210', '21-Jan-2021',
];
const STATUS: string[] = [
    'Awarded', 'Awarded', 'Revised', 'Revised', 'Awarded', 'Revised',
];



@Component({
    selector     : 'form-new',
    templateUrl  : './form-new.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush

})
export class FormNewComponent
{
    displayedColumns: string[] = ['id', 'number', 'revision', 'type', 'name', 'created', 'startDate', 'endDate', 'status'];
    dataSource: MatTableDataSource<RowData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @ViewChild('imageFileInput') private _imageFileInput: ElementRef;
    @ViewChild('tagsPanel') private _tagsPanel: TemplateRef<any>;
    @ViewChild('tagsPanelOrigin') private _tagsPanelOrigin: ElementRef;

    editMode: boolean = false;
    contact: Contact;
    contactForm: FormGroup;
    contacts: Contact[];
    countries: Country[];
    date = new FormControl(new Date());

    panelOpenState = false;
    hasSubQuestion = false;
    /**
     * Constructor
     */
    constructor(
        public dialog: MatDialog,
        private router: Router,
        private _activatedRoute: ActivatedRoute,
        private _changeDetectorRef: ChangeDetectorRef,
        private _contactsService: ContactsService,
        private _formBuilder: FormBuilder,
        private _fuseConfirmationService: FuseConfirmationService,
        private _renderer2: Renderer2,
        private datePipe: DatePipe

    )
    {
        const users = Array.from({length: 100}, (_, k) => createNewRow(k + 1));

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);
    }
    getDate() {
        let dateObject = new Date();
        dateObject.setDate(dateObject.getDate());
        return this.datePipe.transform(dateObject, 'yyyy-MM-dd');
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }


    /**
     * Upload images
     *
     * @param fileList
     */
    uploadImage(fileList: FileList): void
    {
        console.log('Hellow at..........//////////////'+ fileList);

        // Return if canceled
        if ( !fileList.length )
        {
            return;
        }

        const allowedTypes = ['image/jpeg', 'image/png', 'image/jpg'];
        const file = fileList[0];

        // Return if the file is not allowed
        if ( !allowedTypes.includes(file.type) )
        {
            return;
        }
        // Upload the Image
        this._contactsService.uploadImage(this.contact.id, file).subscribe();
    }

    /**
     * Remove the images
     */
    removeImage(): void
    {
        // Get the form control for 'Image'
        const imageFormControl = this.contactForm.get('image');

        // Set the image as null
        imageFormControl.setValue(null);

        // Set the file input value as null
        this._imageFileInput.nativeElement.value = null;

        // Update the contact
        this.contact.image = null;
    }

    trackByFn(index: number, item: any): any
    {
        return item.id || index;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    previewForm() {
        const dialogRef = this.dialog.open(PreviewFormOverlayComponent, {autoFocus: false});
        dialogRef.addPanelClass('full-height');
        dialogRef.afterClosed().subscribe(result => {
        });
    }

}
/** Builds and returns a new createNewRow. */
function createNewRow(id: number): RowData {

    return {
        id: id.toString(),
        number: NUMBER[Math.round(Math.random() * (NUMBER.length - 1))],
        revision: REVISION[Math.round(Math.random() * (REVISION.length - 1))],
        type: TYPE[Math.round(Math.random() * (TYPE.length - 1))],
        name: NAME[Math.round(Math.random() * (NAME.length - 1))],
        created: CREATED[Math.round(Math.random() * (CREATED.length - 1))],
        startDate: STARTDATE[Math.round(Math.random() * (STARTDATE.length - 1))],
        endDate: ENDDATE[Math.round(Math.random() * (ENDDATE.length - 1))],
        status: STATUS[Math.round(Math.random() * (STATUS.length - 1))],
    };
}

