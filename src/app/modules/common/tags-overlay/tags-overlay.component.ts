import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';


@Component({
  selector: 'tags-overlay',
  templateUrl: '../tags-overlay/tags-overlay.component.html',
  encapsulation: ViewEncapsulation.None
})
export class TagsOverlayComponent {
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

  templateData: any = [];
  useremail: string = '';

  selectedId: any = [];
  errormessage = 'Something went wrong, please try again.';
  successmessage = 'Successfully added the template';
  issuccess = false;
  iserror = false;

  constructor(public dialogRef: MatDialogRef<TagsOverlayComponent>,
    public dialog: MatDialog
  ) {
  }

  addTemplate(item, event) {
  }

  doAction() {
    this.dialogRef.close();
    window.location.reload() ;

  }

  saveTemplate() {
  }


}
