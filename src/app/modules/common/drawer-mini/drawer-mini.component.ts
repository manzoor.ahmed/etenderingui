import { Component, ViewEncapsulation } from '@angular/core';
import { FuseNavigationItem } from '@fuse/components/navigation/navigation.types';

@Component({
    selector     : 'drawer-mini',
    templateUrl  : '../drawer-mini/drawer-mini.component.html',
    encapsulation: ViewEncapsulation.None
})
export class DrawerMiniComponent
{
    menuData: FuseNavigationItem[];

    /**
     * Constructor
     */
    constructor()
    {
        this.menuData = [
            {
                title: 'Dashboard',
                type : 'basic',
                icon : 'iconsmind:home',
                link : '/dashboard',
                active: true,
            },
            {
                id :'sourcing',
                title: 'Sourcing',
                icon : 'iconsmind:handshake',
                type    : 'collapsable',
                children: [
                    {
                        id   : 'rfx',
                        title: 'Manage RFx',
                        type : 'basic',
                        link : '/rfx'
                    }
                ]
            },
            {
                id :'basic',
                title: 'Basic Data',
                icon : 'iconsmind:big_data',
                type    : 'collapsable',
                children: [
                    {
                        id   : 'rfxtemp',
                        title: 'RFx Templates',
                        type : 'basic',
                        link : '/rfxtemp'
                    },
                    {
                        id   : 'supinlist',
                        title: 'Supplier Invitation Lists',
                        type : 'basic',
                        link : '/supplier-invitation-list'
                    },
                    {
                        id   : 'reattlist',
                        title: 'Reusable Attribute Lists',
                        type : 'basic',
                        link : '/reattlist'
                    },
                    {
                        id   : 'costfactors',
                        title: 'Cost Factor',
                        type : 'basic',
                        link : '/cost-factors'
                      },
                    {
                        id   : 'cost-factors-list',
                        title: 'Cost factors list',
                        type : 'basic',
                        link : 'cost-factors/cost-factors-list'
                    },
                    {
                        id   : 'negotiation-styles',
                        title: 'Negotiation Styles',
                        type : 'basic',
                        link : '/negotiation-styles'
                      },
                    {
                        id   : 'colteams',
                        title: 'Collaboration Teams',
                        type : 'basic',
                        link : '/colteams'
                    },
                    {
                        id   : 'doctexts',
                        title: 'Document Texts',
                        type : 'basic',
                        link : '/doctexts'
                    },
                    {
                        id   : 'attach',
                        title: 'Attachments',
                        type : 'basic',
                        link : '/attach'
                    },
                    {
                     id   : 'sequences',
                     title: 'Numbering Sequences',
                     type : 'basic',
                     link : '/numbering-sequence'
                    },
                    {
                     id   : 'currency',
                     title: 'Currency',
                     type : 'basic',
                     link : '/currency'
                    },
                    {
                        id   : 'terms',
                        title: 'Terms',
                        type : 'basic',
                        link : '/terms'
                     },
                ]
            },
            {
                id :'analytics',
                title: 'Analytics',
                icon : 'iconsmind:monitor_analytics',
                type    : 'collapsable',
                children: [
                    {
                        id   : 'reports',
                        title: 'Reports',
                        type : 'basic',
                        link : '/reports'
                    },
                    {
                        id   : 'dashboards',
                        title: 'Dashboards',
                        type : 'basic',
                        link : '/dashboards'
                    }
                ]
            }
        ];
    }
}
