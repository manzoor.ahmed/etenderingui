import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector     : 'overlay',
    templateUrl  : '../overlay/overlay.component.html',
    encapsulation: ViewEncapsulation.None
})
export class OverlayComponent
{

    /**
     * Constructor
     */
    constructor(@Inject(MAT_DIALOG_DATA) public data,public dialogRef: MatDialogRef<OverlayComponent>)
    {
    }
    criticalitysummary = new FormGroup({
        numbers: new FormControl('1653'),
        name: new FormControl('Design supplier 01-06'),
        code: new FormControl('IMI-2021-0161'),
        score: new FormControl('0'),
    });
}
