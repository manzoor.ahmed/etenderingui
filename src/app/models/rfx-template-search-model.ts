import { ListModel } from "app/models/list-model";
import { RFXTemplateViewModel } from "app/models/ViewModels/rfx-template-view-model";

export class RFXTemplateSearchModel  extends ListModel {
    rfxTempName : string;
    rfxTempDescription : string;
    rfxTempType:string;
    rfxTemplateModelViewModels : RFXTemplateViewModel[];
    
    constructor() {
        super();
        this.column = "title";
    }
}
