// public Guid BaseCurrency { get; set; }
// public List<CurrencyRateModel> CurrencyRates { get; set; }

import { ListModel } from "./list-model";
import { CurrencyRateViewModel } from "./ViewModels/currency-rate-view-model";

export class CurrencyRateSearchModel  extends ListModel {
    baseCurrency : string;
    currencyName: string;
    currencyRates : CurrencyRateViewModel[];
    
    constructor() {
        super();
        this.column = "currencyName";
    }
}
