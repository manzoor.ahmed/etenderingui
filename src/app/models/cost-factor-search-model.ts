import { ListModel } from "./list-model";
import { CostFactorTextViewModel } from "./ViewModels/cost-factor-view-model";

export class CostFactorSearchModel  extends ListModel {
    cfName : string;
    cfDescription:string;
    cfType:string;
    cfTypeId:string;
    costFactorTextViewModels : CostFactorTextViewModel[];
    cfTypes:any[];
    
    isPrivate:boolean;
    constructor() {
        super();
        this.column = "cfName";
    }
}
