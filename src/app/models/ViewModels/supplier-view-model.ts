export class SupplierViewModel {
    id: string;
    supplierName: string;
    supplierType: string;
    contactEmail : string; 
    supplierStatus: string;
    ifsSupplierId : string;
    title: string;
    isChecked:boolean;
    supplierGroupId:string;
    contactTitle: string;
 }