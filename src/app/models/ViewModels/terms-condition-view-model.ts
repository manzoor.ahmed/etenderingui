
export class TermsConditionViewModel {
    id: string;
    inputType: string;
    termsConditionsAttachmentPath: string;
    isEditable: boolean;
    beforeQuoteId: boolean;
    submitQuoteId: string;
    isRFQ: boolean;
    isRFI: boolean;
    isRFAQ: boolean;
    isPO: boolean;
    consents: any[];
    beforeQuoteStatus:string;
    submitQuoteStatus:string;
    termsConditions:string;
    etMediaId: string;
    fileName: string;
    wordData: string;
}
