import { AttributeViewModel } from "./attribute-view-model";

export class AttributeGroupViewModel {
    id : string;
    title : string;
    name : string;
    groupName : string;
    isPrivate : boolean;
    attributeModels:AttributeViewModel[];
    dataTypeList:any[];
}
