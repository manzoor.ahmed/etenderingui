

export class CurrencyRateViewModel {
    id : string;
    currencyName : string;
    rate : number;
    conversionFactor : number;
    isRFQ:boolean;
    isRFAQ:boolean;
    isRFI:boolean;
    validFrom:Date;
    isBaseCurrency:boolean;

}
