
export class CostFactorTextViewModel {
    id : string;
    cfName : string;
    cfDescription : string;
    cfTypeName:string;
    cfType : string;
    cfTypeId: string;
    isChecked:boolean;
    costFactorGroupId:string;
    isDisabled:boolean;
}
