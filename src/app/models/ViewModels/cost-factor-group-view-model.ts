import { CostFactorTextViewModel } from "../ViewModels/cost-factor-view-model";

export class CostFactorGroupViewModel {
    id : string;
    title : string;
    name : string;
    costFactorModels:CostFactorTextViewModel[];
    dataTypeList : any[];
    isPrivate: boolean;

}
