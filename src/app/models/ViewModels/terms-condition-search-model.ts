import { ListModel } from "../list-model";
import { TermsConditionViewModel } from "../ViewModels/terms-condition-view-model";

export class TermsConditionSearchModel  extends ListModel {
    id : string;
    tCs : string;
  
    termsConditionModels : TermsConditionViewModel[];
    
    constructor() {
        super();
        this.column = "tCs";
    }
}
