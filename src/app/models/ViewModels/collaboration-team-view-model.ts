export class CollaborationTeamTextViewModel {
    id : string;
    teamName : string;
    teamDescription : string;
}
