export class RFXViewModel {

    id : string ;
    RFXNumber : string;
    revision : string;
    RFXType : string;
    RFXName : string;
    CreatedBy : string;
    StartDate : Date;
    EndDate : Date;
    StatusName : string;


}
