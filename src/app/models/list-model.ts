import { environment } from "../../environments/environment";

export class ListModel {
    pageSize : number;
    page : number;
    totalPages: number;
    totalItems: number;
    column:string;
    direction:string = "Asc";
}
