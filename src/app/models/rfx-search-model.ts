import { ListModel } from "./list-model";
import { RFXViewModel } from "./ViewModels/rfx-view-model";

export class RFXSearchModel  extends ListModel {
    RFXNumber : string;
    Revision : string;
    RFXName : string;
    rfqModels : RFXViewModel[];
    rFxType : string;
    statusName : string;
    
    constructor() {
        super();
        this.column = "bidOpeningDate";
    }
}
