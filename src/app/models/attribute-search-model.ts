import { ListModel } from "./list-model";
import { AttributeViewModel } from "app/models/ViewModels/attribute-view-model";

export class AttributeSearchModel  extends ListModel {
    name : string;
    DataType: string;
    category : string;
    description : string;
    attributeModels : AttributeViewModel[];
    AttributeDataTypeList:any[];
    attributeCategotyList:any[];
    
    constructor() {
        super();
        this.column = "name";
    }
}
