import { ListModel } from "./list-model";
import { CollaborationTeamTextViewModel } from "./ViewModels/collaboration-team-view-model";

export class CollaborationTeamSearchModel  extends ListModel {
    teamName : string;
    teamDescription:string;
    token:{result:string};
    collaborationTeamTextViewModels : CollaborationTeamTextViewModel[];
    
    constructor() {
        super();
        this.column = "teamName";
    }
}
