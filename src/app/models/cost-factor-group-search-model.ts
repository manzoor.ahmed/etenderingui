import { ListModel } from "./list-model";
import { CostFactorGroupViewModel } from "./ViewModels/cost-factor-group-view-model";
import { CostFactorTextViewModel } from 'app/models/ViewModels/cost-factor-view-model';

export class CostFactorGroupSearchModel  extends ListModel {
    id : string;
    cFGTitle : string;
    cFGName:string;
    isPrivate: boolean;
    costFactorModels: CostFactorTextViewModel[];
    costFactorGroupModels : CostFactorGroupViewModel[];
    
    //dataTypeList : any[];
    constructor() {
        super();
        this.column = "cFGTitle";
    }
}
