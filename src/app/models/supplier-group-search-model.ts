import { ListModel } from "./list-model";
import { SupplierGroupViewModel } from "./ViewModels/supplier-group-view-model";
import { SupplierViewModel } from "./ViewModels/supplier-view-model";

export class SupplierGroupSearchModel  extends ListModel {
    supplierGroupTitle : string;
    supplierGroupName:string;
    supplierModels : SupplierViewModel[];
    supplierGroupModels : SupplierGroupViewModel[];
    supplierGroupViewModels: SupplierGroupViewModel[];
    isPrivate : boolean;
    paginator: any;
    sort: any;
    filter: string;
    
    constructor() {
        super();
        this.column = "supplierGroupTitle";
    }
}
