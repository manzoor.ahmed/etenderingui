import { ListModel } from "./list-model";
import { SupplierViewModel } from "./ViewModels/supplier-view-model";

export class SupplierSearchModel  extends ListModel {
    id: string;
    supplierName : string;
    supplierType : string;
    title : string;
    contactEmail : string;
    supplierStatus: string;
    data:any;
    supplierModel : SupplierViewModel[];
    paginator: any;
    sort: any;
    filter: string;
    
    constructor() {
        super();
        this.column = "supplierTitle";
    }
}