import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from 'app/core/auth/auth.service';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { environment } from 'environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor
{
    /**
     * Constructor
     */
    constructor(private _authService: AuthService)
    {
    }

    /**
     * Intercept
     *
     * @param req
     * @param next
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        // Clone the request object
        let newReq = req.clone();
       
// this._authService.accessToken="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoibWFuem9vciIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6ImFkbWluIiwiZXhwIjoxNjMzMzQwOTAxLCJpc3MiOiJ0ZWNod2l0aG1hbnpvb3IiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjM0MjE2In0.z5528PT8m4dprz2aiMw0l1fOdIWvd72gN5GB4cuSaHg";
        // Request
        //
        // If the access token didn't expire, add the Authorization header.
        // We won't add the Authorization header if the access token expired.
        // This will force the server to return a "401 Unauthorized" response
        // for the protected API routes which our response interceptor will
        // catch and delete the access token from the local storage while logging
        // the user out from the app.
        if ((this._authService.accessToken!=undefined || this._authService.accessToken!="underfined") && this._authService.accessToken && !AuthUtils.isTokenExpired(this._authService.accessToken) )
        {
            if(req.url != environment.apiUrl +"/api/auth/login")
            {
                // req = req.clone({
                //     headers: req.headers.set('Authorization', 'Bearer ' + this._authService.accessToken)
                // });
                // req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + this._authService.accessToken) });
                req = req.clone({
                    setHeaders: {
                      'Content-Type' : 'application/json; charset=utf-8',
                      'Accept'       : 'application/json',
                      'Authorization': `Bearer ${this._authService.accessToken}`,
                    },
                  });
            }
            
        }

        // Response
        return next.handle(req).pipe(
            catchError((error) => {

                // Catch "401 Unauthorized" responses
                if ( error instanceof HttpErrorResponse && error.status === 401 )
                {
                    // Sign out
                    this._authService.signOut();

                    // Reload the app
                    location.reload();
                }

                return throwError(error);
            })
        );
    }
}
