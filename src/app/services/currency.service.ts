import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { CostFactorSearchModel } from '../models/cost-factor-search-model';
import { Observable, ReplaySubject } from 'rxjs';
import {CurrencySearchModel} from 'app/models/currency-search-model';
import {CurrencyRateSearchModel} from 'app/models/currency-rate-search-model';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  
  constructor(private http: HttpClient) { }
  url = environment.apiUrl + 'api/cur/';
  
  getCurrencyList(searchModel : CurrencySearchModel){
    let params = new HttpParams();      
    params = params.append('tabIndex', 2);   
    var endpointUrl = this.url;
    endpointUrl = endpointUrl + "/GetCurrencies";
    const httpOptions = { headers: new HttpHeaders(),params:params };
    return this.http.post<CurrencySearchModel>(endpointUrl, searchModel,httpOptions);
  }
  
  getCurrencyRateList(){

    let params = new HttpParams();
    var endpointUrl = this.url;
    endpointUrl = endpointUrl + "GetCurrencyRates";

    return this.http.get<any>(endpointUrl, {params});

  }
  public UpdateCurrencies(models: any[], index?: any): Observable<any[]> {

    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: null };


    return this.http.post<any[]>(this.url + "UpdateCurrencies", models, httpOptions);
  }
  public SaveCurrencyRates(models: CurrencyRateSearchModel, index?: any): Observable<CurrencyRateSearchModel> {

    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: null };


    return this.http.post<CurrencyRateSearchModel>(this.url + "SaveCurrencyRate", models, httpOptions);
  }
  public DeleteItem(models: any[], index?: any): Observable<any> {

    const httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: models};


    return this.http.delete<any>(this.url + "DeleteCollaborationTeam",  httpOptions);
  }
}
