import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable, ReplaySubject } from 'rxjs';
import { CostFactorGroupSearchModel } from 'app/models/cost-factor-group-search-model';
import { CostFactorSearchModel } from 'app/models/cost-factor-search-model';


@Injectable({
    providedIn: 'root'
  })
  export class CostFactorGroupService{

    constructor(private http: HttpClient) { }
    url = environment.apiUrl + 'api/CF/';

    getCFGList(searchModel: CostFactorGroupSearchModel){
        let params = new HttpParams();
    params = params.append('tabIndex', 2);
    var endpointUrl = this.url;
    endpointUrl = endpointUrl + "GetCFGList";
    const httpOptions = { headers: new HttpHeaders(), params: params };
    return this.http.post<CostFactorGroupSearchModel>(endpointUrl, searchModel, httpOptions);
    }

    getCostFactorGroupById(id: any) {
      //debugger;
        let params = new HttpParams();
         params = params.append('tabIndex', 2);
        var endpointUrl = this.url;
        endpointUrl = endpointUrl + "GetCFGByID";
        params = params.append('id', id);  
        return this.http.get<any>(endpointUrl, {params});
    
    }

    getCostFactor(cfgid: any){
      debugger;
      let params = new HttpParams();
         params = params.append('tabIndex', 2);
        var endpointUrl = this.url;
        endpointUrl = endpointUrl + "GetCFs";
        params = params.append('id', cfgid);  
        return this.http.get<any>(endpointUrl, {params});
    }

    
  public login(userName, password): Observable<any> {

    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: null };


    return this.http.post<any>(environment.apiUrl + "/api/auth/login", { "UserName": userName, "Password": password }, httpOptions);
  }

  public SaveCostFactorGroupMapping(models: any[], index?: any): Observable<any[]> {

    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: null };

    return this.http.post<any[]>(this.url + "SaveCostFactorGroupMapping", models, httpOptions);
  }
  
public DeleteItem(models: any[], index?: any): Observable<any> {



  const httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};



  return this.http.post<any>(this.url + "DeleteCostFactorGroup", models, httpOptions);



}
  }