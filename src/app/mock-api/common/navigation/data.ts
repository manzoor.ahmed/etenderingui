/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id   : 'dashboard',
        title: 'Dashboards',
        type : 'basic',
        link : '/dashboard',
        icon : 'feather:home',
    },
    {
        id   : 'sourcing',
        title: 'Sourcing',
        type : 'collapsable',
        link : '/sourcing',
        icon : 'feather:users',
        active: true,
        children: [
            {
                id   : 'sourcing.rfx',
                title: 'Create RFX',
                type : 'basic',
                link : 'rfx-new'
            },
            {
                id   : 'sourcing.rfx.new',
                title: 'Manage RFX',
                type : 'basic',
                link : '/rfx'
            },
        ]
    },
    {
        id   : 'basicData',
        title: 'Basic Data',
        type : 'collapsable',
        link : '/basicData',
        icon : 'feather:database',
        children: [
            {
                id   : 'basicData.rfx-templates',
                title: 'RFX Templates',
                type : 'basic',
                link : '/rfx-templates'
            },
            {
                id   : 'basicData.supplier',
                title: 'Supplier Invitation Lists',
                type : 'basic',
                link : '/supplier-invitation-list'
            },
            {
                id   : 'basicData.reusableattributelists',
                title: 'Reusable Attribute Lists',
                type : 'collapsable',
                link : '/reusable-attribute-lists',
                children: [
                    {
                        id   : 'basicData.attributeitems',
                        title: 'Attribute Items',
                        type : 'basic',
                        link : '/attribute-items'
                    },
                    {
                        id   : 'basicData.attributelists',
                        title: 'Attribute Lists',
                        type : 'basic',
                        link : '/attribute-list'
                    },
                ]
            },
            {
                id   : 'basicData.reusableattributelists',
                title: 'Cost Factor Lists',
                type : 'collapsable',
                link : '/reusable-attribute-lists',
                children: [
                    {
                        id   : 'basicData.costfactors',
                        title: 'Cost Factors',
                        type : 'basic',
                        link : '/cost-factors'
                    },
                    {
                        id   : 'basicData.costfactorlists',
                        title: 'Cost Factor Lists',
                        type : 'basic',
                        link : '/cost-factors-list'
                    },
                ]
            },
            {
                id   : 'basicData.negotiation-styles',
                title: 'Negotiation Styles',
                type : 'basic',
                link : '/negotiation-styles'
            },
            {
                id   : 'basicData.collaborationteams',
                title: 'Collaboration Teams',
                type : 'basic',
                link : '/colteams'
            },
            {
                id   : 'basicData.documenttexts',
                title: 'Document Texts',
                type : 'basic',
                link : '/document-texts'
            },
            {
                id   : 'basicData.attachments',
                title: 'Attachments',
                type : 'basic',
                link : '/attachments'
            },
        ]
    },
    {
        id   : 'analytics',
        title: 'Analytics',
        type : 'collapsable',
        link : '/analytics',
        icon : 'feather:trending-up',
        children: [
            {
                id   : 'analytics.reports',
                title: 'Reports',
                type : 'basic',
                link : '/reports'
            },
            {
                id   : 'analytics.dashboards',
                title: 'Dashboards',
                type : 'basic',
                link : '/dashboards'
            },
        ]
    },
    {
        id   : 'suppliers',
        title: 'Suppliers',
        type : 'basic',
        link : '/suppliers',
        icon : 'feather:truck',
    },
    {
        id   : 'setup',
        title: 'Setup',
        type : 'basic',
        link : '/setup',
        icon : 'feather:settings',
    },
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id   : 'dashboard',
        title: 'Dashboard',
        type : 'basic',
        link : '/dashboard'
    },
    {
        id   : 'help',
        title: 'Help',
        type : 'basic',
        link : '/help'
    },
    {
        id   : 'contact',
        title: 'Contact',
        type : 'basic',
        link : '/contact'
    },
    {
        id   : 'profile',
        title: 'IMI Admin',
        type : 'group',
        link : '/profile'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id   : 'dashboard',
        title: 'Dashboard',
        type : 'basic',
        link : '/dashboard'
    },
    {
        id   : 'help',
        title: 'Help',
        type : 'basic',
        link : '/help'
    },
    {
        id   : 'contact',
        title: 'Contact',
        type : 'basic',
        link : '/contact'
    },
    {
        id   : 'profile',
        title: 'IMI Admin',
        type : 'group',
        link : '/profile'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id   : 'dashboard',
        title: 'Dashboards',
        type : 'basic',
        link : '/dashboard',
        icon : 'feather:home',
    },
    {
        id   : 'sourcing',
        title: 'Sourcing',
        type : 'collapsable',
        link : '/sourcing',
        icon : 'feather:users',
        active: true,
        children: [
            {
                id   : 'sourcing.rfx',
                title: 'Create RFX',
                type : 'basic',
                link : 'rfx-new'
            },
            {
                id   : 'sourcing.rfx.new',
                title: 'Manage RFX',
                type : 'basic',
                link : '/rfx'
            },
        ]
    },
    {
        id   : 'basicData',
        title: 'Basic Data',
        type : 'collapsable',
        link : '/basicData',
        icon : 'feather:database',
        children: [
            {
                id   : 'basicData.rfx-templates',
                title: 'RFX Templates',
                type : 'basic',
                link : '/rfx-templates'
            },
            {
                id   : 'basicData.supplier',
                title: 'Supplier Invitation Lists',
                type : 'basic',
                link : '/supplier-invitation-list'
            },
            {
                id   : 'basicData.reusableattributelists',
                title: 'Reusable Attribute Lists',
                type : 'collapsable',
                link : '/reusable-attribute-lists',
                children: [
                    {
                        id   : 'basicData.attributeitems',
                        title: 'Attribute Items',
                        type : 'basic',
                        link : '/attribute-items'
                    },
                    {
                        id   : 'basicData.attributelists',
                        title: 'Attribute Lists',
                        type : 'basic',
                        link : '/attribute-list'
                    },
                ]
            },
            {
                id   : 'basicData.reusableattributelists',
                title: 'Cost Factor Lists',
                type : 'collapsable',
                link : '/reusable-attribute-lists',
                children: [
                    {
                        id   : 'basicData.costfactors',
                        title: 'Cost Factors',
                        type : 'basic',
                        link : '/cost-factors'
                    },
                    {
                        id   : 'basicData.costfactorlists',
                        title: 'Cost Factor Lists',
                        type : 'basic',
                        link : '/cost-factors-list'
                    },
                ]
            },
            {
                id   : 'basicData.negotiation-styles',
                title: 'Negotiation Styles',
                type : 'basic',
                link : '/negotiation-styles'
            },
            {
                id   : 'basicData.collaborationteams',
                title: 'Collaboration Teams',
                type : 'basic',
                link : '/colteams'
            },
            {
                id   : 'basicData.documenttexts',
                title: 'Document Texts',
                type : 'basic',
                link : '/document-texts'
            },
            {
                id   : 'basicData.attachments',
                title: 'Attachments',
                type : 'basic',
                link : '/attachments'
            },
        ]
    },
    {
        id   : 'analytics',
        title: 'Analytics',
        type : 'collapsable',
        link : '/analytics',
        icon : 'feather:trending-up',
        children: [
            {
                id   : 'analytics.reports',
                title: 'Reports',
                type : 'basic',
                link : '/reports'
            },
            {
                id   : 'analytics.dashboards',
                title: 'Dashboards',
                type : 'basic',
                link : '/dashboards'
            },
        ]
    },
    {
        id   : 'suppliers',
        title: 'Suppliers',
        type : 'basic',
        link : '/suppliers',
        icon : 'feather:truck',
    },
    {
        id   : 'setup',
        title: 'Setup',
        type : 'basic',
        link : '/setup',
        icon : 'feather:settings',
    },
];
